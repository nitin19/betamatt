<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('username')->unique();
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('hd_password', 191)->nullable();
            $table->string('firstname', 191)->nullable();
            $table->string('lastname', 191)->nullable();
            $table->string('gender', 191)->nullable();
            $table->date('dob', 191)->nullable();
            $table->string('phone', 191)->nullable();
            $table->text('address')->nullable();
            $table->string('city', 191)->nullable();
            $table->string('state', 191)->nullable();
            $table->string('country', 191)->nullable();
            $table->string('zipcode', 191)->nullable();
            $table->string('lat', 191)->nullable();
            $table->string('lng', 191)->nullable();
            $table->text('profilepic')->nullable();
            $table->text('description')->nullable();
            $table->string('role', 191)->nullable();
            $table->text('device')->nullable();
            $table->text('browser')->nullable();
            $table->string('ipaddress', 191)->nullable();
            $table->string('active_code', 191)->nullable();
            $table->enum('is_approved',['0', '1', '2'])->default('1')->comment="1=Approved,0=Not Approved,2=Declined";
            $table->enum('is_active',['0', '1'])->default('1')->comment="1=Active,0=Inactive";
            $table->enum('is_deleted',['0', '1'])->default('0')->comment="1=Deleted,0=Not Deleted";
            $table->enum('is_online',['0', '1'])->default('0')->comment="1=Online,0=Offline";
            $table->string('llhid', 191)->nullable();
            $table->integer('logins')->default('0');
            $table->timestamp('last_login')->nullable();
            $table->string('fp_token', 191)->nullable();
            $table->rememberToken();
            $table->string('created_by', 191)->nullable();
            $table->string('updated_by', 191)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
