<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->nullable();
            $table->string('post_type', 191)->nullable();
            $table->text('post_description')->nullable();
            $table->enum('post_media_choice',['0', '1'])->default('0')->comment="1=Live,0=Local";
            $table->enum('post_privacy',['0', '1'])->default('0')->comment="1=All,0=Me";
            $table->enum('is_like_post',['0', '1'])->default('0')->comment="1=Yes,0=No";
            $table->enum('is_pray_post',['0', '1'])->default('0')->comment="1=Yes,0=No";
            $table->enum('is_active',['0', '1'])->default('1')->comment="1=Yes,0=No";
            $table->enum('is_deleted',['0', '1'])->default('0')->comment="1=Yes,0=No";
            $table->integer('post_views');
            $table->string('created_by', 191)->nullable();
            $table->string('updated_by', 191)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
