<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
  if (Auth::check()) {
      if(Auth::user()->role == "1"){
        return redirect()->intended('admin/dashboard');
      } else {
        return redirect()->intended('/home');
      }
	} else {
	   return view('loginregister.loginregister');	
	}
});



  Route::group(['middleware' => ['guest']], function ($router) {
  Route::post ( '/userlogin', 'AuthController@login' );
  Route::post ( '/userregister', 'AuthController@register' );
  /*Route::post('register', 'Api\AuthController@register');
  Route::post('login', 'Api\AuthController@login');
  Route::post('logout', 'Api\AuthController@logout');
  Route::post('forgetpassword', 'Api\AuthController@forgetpassword');
  Route::post('resetpassword', 'Api\AuthController@Resetpassword');
  Route::get('reset/password/{email}', 'Api\AuthController@passwordReset');
  Route::post('/account/activation/{active_code}', 'Api\AuthController@userActivation');*/
});
Route::get ( '/logout', 'AuthController@logout' );

Auth::routes();

Route::get('/home', 'Frontend\HomeController@index')->name('home');
Route::get('/feed', 'Frontend\FeedController@index')->name('feed');
Route::post('/posts/addpost', 'Frontend\PostsController@addPost');
Route::post('/posts/editdefaultpost', 'Frontend\PostsController@editdefaultPost');
Route::post('/posts/editlikepost', 'Frontend\PostsController@editlikePost');
Route::post('/posts/editpraypost', 'Frontend\PostsController@editprayPost');
Route::get('/posts/deletepost/{id}', 'Frontend\PostsController@deletePost');
Route::post('/feed/postfeed', 'Frontend\FeedController@addPostFeed');
Route::post('/feed/postfeed', 'Frontend\FeedController@addPostFeed');
Route::post('/home/hidelike', 'Frontend\HideController@hidelikeFeed');
Route::post('/home/hidepray', 'Frontend\HideController@hidePrayFeed');

Route::get('/user/profile/{id}','Frontend\ProfileController@index');
Route::post('/user/updateprofile/{id}', 'Frontend\ProfileController@updateprofile');

Route::get('/user/changepasssword/{id}','Frontend\PasswordController@index');
Route::post('/user/updatepasssword/{id}','Frontend\PasswordController@updatepassword');

Route::group(['middleware' => ['auth'], 'prefix' => 'admin'], function ($router) {
  Route::get('dashboard','Admin\AdmindashboardController@index');
  Route::post('accountenabledisable','Admin\AdmindashboardController@account_enable_disable');
  Route::post('updateuserrole','Admin\AdmindashboardController@update_user_role');
  Route::post('updateposthidestatus','Admin\AdmindashboardController@update_post_hide_status');
  Route::post('adminresetpassword','Admin\AdmindashboardController@admin_reset_password');
  Route::post('registerenabledisable','Admin\AdmindashboardController@register_enable_disable');
  
});



/*Route::group(['middleware' => ['auth'], 'prefix' => 'posts'], function ($router) {
  Route::post('addpost', 'Api\UserController@checkemail');
});*/