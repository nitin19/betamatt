-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 26, 2020 at 09:46 PM
-- Server version: 5.6.46-cll-lve
-- PHP Version: 7.2.7

SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `betamattblog`
--

-- --------------------------------------------------------

--
-- Table structure for table `errorlog`
--

CREATE TABLE `errorlog` (
  `id` int(10) UNSIGNED NOT NULL,
  `error_message` longtext,
  `line_number` varchar(191) DEFAULT NULL,
  `file_name` varchar(191) DEFAULT NULL,
  `browser` varchar(191) DEFAULT NULL,
  `operating_system` varchar(191) DEFAULT NULL,
  `loggedin_id` int(11) DEFAULT NULL,
  `ip_address` varchar(191) DEFAULT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '1' COMMENT '1=Active,0=Deactive',
  `deleted` enum('1','0') NOT NULL DEFAULT '0' COMMENT '1=Deleted,0=Not Deleted',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ;

--
-- Dumping data for table `errorlog`
--

INSERT INTO `errorlog` (`id`, `error_message`, `line_number`, `file_name`, `browser`, `operating_system`, `loggedin_id`, `ip_address`, `status`, `deleted`, `created_at`, `updated_at`) VALUES
(1, 'SQLSTATE[22001]: String data, right truncated: 1406 Data too long for column \'post_description\' at row 1 (SQL: update `posts` set `post_type` = text, `post_privacy` = 0, `is_like_post` = 0, `is_pray_post` = 0, `updated_by` = 2, `post_description` = Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\n\r\nWhy do we use it2020-02-04 07:57:21\r\nIt is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like)., `updated_at` = 7 where `id` = ?)', '664', '/home/vyp81upvi58i/public_html/betamattblog/vendor/laravel/framework/src/Illuminate/Database/Connection.php', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36', 'Apache', 2, '1860289601', '1', '0', '2020-02-04 14:57:21', NULL),
(2, 'SQLSTATE[22001]: String data, right truncated: 1406 Data too long for column \'post_description\' at row 1 (SQL: update `posts` set `post_type` = text, `post_privacy` = 0, `is_like_post` = 0, `is_pray_post` = 0, `updated_by` = 2, `post_description` = Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\n\r\nWhy do we use it2020-02-04 07:58:06\r\nIt is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like)., `updated_at` = 7 where `id` = ?)', '664', '/home/vyp81upvi58i/public_html/betamattblog/vendor/laravel/framework/src/Illuminate/Database/Connection.php', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36', 'Apache', 2, '1860289601', '1', '0', '2020-02-04 14:58:06', NULL),
(3, 'Undefined variable: 1580881583_Screenshot from 2019-08-02 16-46-12.png', '61', '/home/vyp81upvi58i/public_html/betamattblog/app/Http/Controllers/Frontend/ProfileController.php', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 'Apache', 2, '2058170655', '1', '0', '2020-02-05 12:46:27', NULL),
(4, 'Undefined variable: 1580882077_Screenshot from 2019-08-02 16-46-12.png', '60', '/home/vyp81upvi58i/public_html/betamattblog/app/Http/Controllers/Frontend/ProfileController.php', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 'Apache', 2, '2058170655', '1', '0', '2020-02-05 12:54:38', NULL),
(5, 'Undefined variable: 1580882105_Screenshot from 2019-08-02 16-46-12.png', '60', '/home/vyp81upvi58i/public_html/betamattblog/app/Http/Controllers/Frontend/ProfileController.php', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 'Apache', 2, '2058170655', '1', '0', '2020-02-05 12:55:05', NULL),
(6, 'Undefined variable: postImgUrl', '62', '/home/vyp81upvi58i/public_html/betamattblog/app/Http/Controllers/Frontend/ProfileController.php', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 'Apache', 2, '2058170655', '1', '0', '2020-02-05 13:08:20', NULL),
(7, 'Undefined variable: postImgUrl', '62', '/home/vyp81upvi58i/public_html/betamattblog/app/Http/Controllers/Frontend/ProfileController.php', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 'Apache', 2, '2058170655', '1', '0', '2020-02-05 13:09:26', NULL),
(8, 'Route [/user/profile/2] not defined.', '389', '/home/vyp81upvi58i/public_html/betamattblog/vendor/laravel/framework/src/Illuminate/Routing/UrlGenerator.php', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 'Apache', 2, '2058170655', '1', '0', '2020-02-05 13:14:42', NULL),
(9, 'Route [/user/profile/2] not defined.', '389', '/home/vyp81upvi58i/public_html/betamattblog/vendor/laravel/framework/src/Illuminate/Routing/UrlGenerator.php', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 'Apache', 2, '2058170655', '1', '0', '2020-02-05 13:14:48', NULL),
(10, 'Route [/user/profile/2] not defined.', '389', '/home/vyp81upvi58i/public_html/betamattblog/vendor/laravel/framework/src/Illuminate/Routing/UrlGenerator.php', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 'Apache', 2, '2058170655', '1', '0', '2020-02-05 13:14:57', NULL),
(11, 'Route [/user/profile/2] not defined.', '389', '/home/vyp81upvi58i/public_html/betamattblog/vendor/laravel/framework/src/Illuminate/Routing/UrlGenerator.php', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 'Apache', 2, '2058170655', '1', '0', '2020-02-05 13:17:33', NULL),
(12, 'Route [/user/profile/2] not defined.', '389', '/home/vyp81upvi58i/public_html/betamattblog/vendor/laravel/framework/src/Illuminate/Routing/UrlGenerator.php', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 'Apache', 2, '2058170655', '1', '0', '2020-02-05 13:18:36', NULL),
(13, 'Route [/user/profile/2] not defined.', '389', '/home/vyp81upvi58i/public_html/betamattblog/vendor/laravel/framework/src/Illuminate/Routing/UrlGenerator.php', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 'Apache', 2, '2058170655', '1', '0', '2020-02-05 13:50:42', NULL),
(14, 'Route [/user/profile/2] not defined.', '389', '/home/vyp81upvi58i/public_html/betamattblog/vendor/laravel/framework/src/Illuminate/Routing/UrlGenerator.php', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 'Apache', 2, '2058170655', '1', '0', '2020-02-05 13:50:50', NULL),
(15, 'Route [/user/profile/.2] not defined.', '389', '/home/vyp81upvi58i/public_html/betamattblog/vendor/laravel/framework/src/Illuminate/Routing/UrlGenerator.php', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 'Apache', 2, '2058170655', '1', '0', '2020-02-05 13:52:42', NULL),
(16, 'Can\'t write image data to path (/home/vyp81upvi58i/public_html/betamattblog/public/uploads/profileimages/profilethumbs/1581558039_nes tetris.jpg)', '150', '/home/vyp81upvi58i/public_html/betamattblog/vendor/intervention/image/src/Intervention/Image/Image.php', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36', 'Apache', 4, '-1899649618', '1', '0', '2020-02-13 08:40:39', NULL),
(17, 'SQLSTATE[01000]: Warning: 1265 Data truncated for column \'is_deleted\' at row 1 (SQL: update `users` set `is_active` = 1, `is_deleted` = 0, `updated_at` = 2020-02-13 07:06:54 where `id` = 4)', '664', '/home/vyp81upvi58i/public_html/betamattblog/vendor/laravel/framework/src/Illuminate/Database/Connection.php', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 'Apache', 1, '2058164933', '1', '0', '2020-02-13 14:06:54', NULL),
(18, 'SQLSTATE[01000]: Warning: 1265 Data truncated for column \'is_deleted\' at row 1 (SQL: update `users` set `is_active` = 1, `is_deleted` = 0, `updated_at` = 2020-02-13 07:07:10 where `id` = 4)', '664', '/home/vyp81upvi58i/public_html/betamattblog/vendor/laravel/framework/src/Illuminate/Database/Connection.php', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 'Apache', 1, '2058164933', '1', '0', '2020-02-13 14:07:10', NULL),
(19, 'SQLSTATE[01000]: Warning: 1265 Data truncated for column \'is_deleted\' at row 1 (SQL: update `users` set `is_active` = 1, `is_deleted` = 0, `updated_at` = 2020-02-13 07:10:30 where `id` = 4)', '664', '/home/vyp81upvi58i/public_html/betamattblog/vendor/laravel/framework/src/Illuminate/Database/Connection.php', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 'Apache', 1, '2058164933', '1', '0', '2020-02-13 14:10:30', NULL),
(20, 'SQLSTATE[01000]: Warning: 1265 Data truncated for column \'is_deleted\' at row 1 (SQL: update `users` set `is_active` = 1, `is_deleted` = 0, `updated_at` = 2020-02-13 07:24:14 where `id` = 4)', '664', '/home/vyp81upvi58i/public_html/betamattblog/vendor/laravel/framework/src/Illuminate/Database/Connection.php', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 'Apache', 1, '2058164933', '1', '0', '2020-02-13 14:24:14', NULL),
(21, 'SQLSTATE[01000]: Warning: 1265 Data truncated for column \'is_deleted\' at row 1 (SQL: update `users` set `is_active` = 1, `is_deleted` = 0, `updated_at` = 2020-02-13 07:25:41 where `id` = 4)', '664', '/home/vyp81upvi58i/public_html/betamattblog/vendor/laravel/framework/src/Illuminate/Database/Connection.php', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 'Apache', 1, '2058164933', '1', '0', '2020-02-13 14:25:41', NULL),
(22, 'SQLSTATE[01000]: Warning: 1265 Data truncated for column \'is_deleted\' at row 1 (SQL: update `users` set `is_active` = 1, `is_deleted` = 0, `updated_at` = 2020-02-13 07:27:16 where `id` = 4)', '664', '/home/vyp81upvi58i/public_html/betamattblog/vendor/laravel/framework/src/Illuminate/Database/Connection.php', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 'Apache', 1, '2058164933', '1', '0', '2020-02-13 14:27:16', NULL),
(23, 'SQLSTATE[01000]: Warning: 1265 Data truncated for column \'is_deleted\' at row 1 (SQL: update `users` set `is_active` = 1, `is_deleted` = 0, `updated_at` = 2020-02-13 07:28:09 where `id` = 4)', '664', '/home/vyp81upvi58i/public_html/betamattblog/vendor/laravel/framework/src/Illuminate/Database/Connection.php', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 'Apache', 1, '2058164933', '1', '0', '2020-02-13 14:28:09', NULL),
(24, 'SQLSTATE[01000]: Warning: 1265 Data truncated for column \'is_deleted\' at row 1 (SQL: update `users` set `is_active` = 1, `is_deleted` = 0, `updated_at` = 2020-02-13 07:28:16 where `id` = 4)', '664', '/home/vyp81upvi58i/public_html/betamattblog/vendor/laravel/framework/src/Illuminate/Database/Connection.php', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 'Apache', 1, '2058164933', '1', '0', '2020-02-13 14:28:16', NULL),
(25, 'SQLSTATE[01000]: Warning: 1265 Data truncated for column \'is_deleted\' at row 1 (SQL: update `users` set `is_active` = 1, `is_deleted` = 0, `updated_at` = 2020-02-13 07:28:56 where `id` = 4)', '664', '/home/vyp81upvi58i/public_html/betamattblog/vendor/laravel/framework/src/Illuminate/Database/Connection.php', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 'Apache', 1, '2058164933', '1', '0', '2020-02-13 14:28:56', NULL),
(26, 'SQLSTATE[01000]: Warning: 1265 Data truncated for column \'is_active\' at row 1 (SQL: update `users` set `is_active` = 0, `is_deleted` = 1, `updated_at` = 2020-02-13 07:29:00 where `id` = 4)', '664', '/home/vyp81upvi58i/public_html/betamattblog/vendor/laravel/framework/src/Illuminate/Database/Connection.php', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 'Apache', 1, '2058164933', '1', '0', '2020-02-13 14:29:00', NULL),
(27, 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'user_id\' in \'field list\' (SQL: insert into `users` (`user_id`, `post_id`, `post_user_id`, `feed_hide`) values (1, 75, 6, 1))', '664', '/home/vyp81upvi58i/public_html/betamattblog/vendor/laravel/framework/src/Illuminate/Database/Connection.php', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 'Apache', 1, '1860294264', '1', '0', '2020-02-14 14:33:32', NULL),
(28, 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'user_id\' in \'field list\' (SQL: insert into `users` (`user_id`, `post_id`, `post_user_id`, `feed_hide`) values (1, 75, 6, 1))', '664', '/home/vyp81upvi58i/public_html/betamattblog/vendor/laravel/framework/src/Illuminate/Database/Connection.php', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 'Apache', 1, '1860294264', '1', '0', '2020-02-14 14:36:31', NULL),
(29, 'SQLSTATE[23000]: Integrity constraint violation: 1048 Column \'feed_hide\' cannot be null (SQL: insert into `feedposts` (`user_id`, `post_id`, `post_user_id`, `feed_hide`) values (, , 6, ))', '664', '/home/vyp81upvi58i/public_html/betamattblog/vendor/laravel/framework/src/Illuminate/Database/Connection.php', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36', 'Apache', 1, '1860294264', '1', '0', '2020-02-14 17:52:53', NULL),
(30, 'Can\'t write image data to path (/home/vyp81upvi58i/public_html/betamattblog/public/uploads/profileimages/profilethumbs/1581911331_Lumi-Black_1.jpg)', '150', '/home/vyp81upvi58i/public_html/betamattblog/vendor/intervention/image/src/Intervention/Image/Image.php', 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Mobile Safari/537.36', 'Apache', 1, '2058215600', '1', '0', '2020-02-17 10:48:51', NULL),
(31, 'Can\'t write image data to path (/home/vyp81upvi58i/public_html/betamattblog/public/uploads/profileimages/profilethumbs/1581911343_Lumi-Black_1.jpg)', '150', '/home/vyp81upvi58i/public_html/betamattblog/vendor/intervention/image/src/Intervention/Image/Image.php', 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Mobile Safari/537.36', 'Apache', 1, '2058215600', '1', '0', '2020-02-17 10:49:03', NULL),
(32, 'Can\'t write image data to path (/home/vyp81upvi58i/public_html/betamattblog/public/uploads/profileimages/profilethumbs/1581911512_Helio-Mirror-Faceplate-10.5-Silver_1-1.jpg)', '150', '/home/vyp81upvi58i/public_html/betamattblog/vendor/intervention/image/src/Intervention/Image/Image.php', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36', 'Apache', 7, '2058215600', '1', '0', '2020-02-17 10:51:52', NULL),
(33, 'SQLSTATE[23000]: Integrity constraint violation: 1048 Column \'feed_hide\' cannot be null (SQL: insert into `feedposts` (`user_id`, `post_id`, `post_user_id`, `feed_hide`) values (, , , ))', '664', '/home/vyp81upvi58i/public_html/betamattblog/vendor/laravel/framework/src/Illuminate/Database/Connection.php', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36', 'Apache', 9, '2058215600', '1', '0', '2020-02-18 12:16:50', NULL),
(34, 'SQLSTATE[23000]: Integrity constraint violation: 1048 Column \'feed_hide\' cannot be null (SQL: insert into `feedposts` (`user_id`, `post_id`, `post_user_id`, `feed_hide`) values (, , , ))', '664', '/home/vyp81upvi58i/public_html/betamattblog/vendor/laravel/framework/src/Illuminate/Database/Connection.php', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36', 'Apache', 9, '2058215600', '1', '0', '2020-02-18 12:18:02', NULL),
(35, 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'register\' in \'where clause\' (SQL: select * from `users` where `role` = 1 and `register` = 0 limit 1)', '664', '/home/vyp81upvi58i/public_html/betamattblog/vendor/laravel/framework/src/Illuminate/Database/Connection.php', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36', 'Apache', 1, '2058215600', '1', '0', '2020-02-18 19:15:04', NULL),
(36, 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'register\' in \'where clause\' (SQL: select * from `users` where `role` = 1 and `register` = 0 limit 1)', '664', '/home/vyp81upvi58i/public_html/betamattblog/vendor/laravel/framework/src/Illuminate/Database/Connection.php', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36', 'Apache', 1, '2058215600', '1', '0', '2020-02-18 19:19:03', NULL),
(37, 'compact(): Undefined variable: Check_user_enable', '45', '/home/vyp81upvi58i/public_html/betamattblog/app/Http/Controllers/Admin/AdmindashboardController.php', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36', 'Apache', 1, '2058215600', '1', '0', '2020-02-18 19:19:19', NULL),
(38, 'compact(): Undefined variable: Check_user_enable', '45', '/home/vyp81upvi58i/public_html/betamattblog/app/Http/Controllers/Admin/AdmindashboardController.php', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36', 'Apache', 1, '2058215600', '1', '0', '2020-02-18 19:19:39', NULL),
(39, 'compact(): Undefined variable: Check_user_enable', '45', '/home/vyp81upvi58i/public_html/betamattblog/app/Http/Controllers/Admin/AdmindashboardController.php', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36', 'Apache', 1, '2058215600', '1', '0', '2020-02-18 19:19:47', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `feedposts`
--

CREATE TABLE `feedposts` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `post_id` int(11) DEFAULT NULL,
  `post_user_id` int(11) DEFAULT NULL,
  `feed_like` enum('0','1') NOT NULL DEFAULT '0' COMMENT '1=Yes,0=No',
  `feed_pray` enum('0','1') NOT NULL DEFAULT '0' COMMENT '1=Yes,0=No',
  `feed_hide` enum('0','1') NOT NULL DEFAULT '0' COMMENT '1=Yes,0=No',
  `is_active` enum('0','1') NOT NULL DEFAULT '1' COMMENT '1=Yes,0=No',
  `is_deleted` enum('0','1') NOT NULL DEFAULT '0' COMMENT '1=Yes,0=No',
  `created_by` varchar(191) DEFAULT NULL,
  `updated_by` varchar(191) DEFAULT NULL,
  `created_at` timestamp NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ;

--
-- Dumping data for table `feedposts`
--

INSERT INTO `feedposts` (`id`, `user_id`, `post_id`, `post_user_id`, `feed_like`, `feed_pray`, `feed_hide`, `is_active`, `is_deleted`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(214, 15, 259, 16, '0', '0', '0', '1', '0', NULL, NULL, '2020-02-25 10:09:00', '2020-02-25 19:28:52'),
(215, 15, 263, 16, '0', '0', '0', '1', '0', NULL, '15', '2020-02-25 10:10:22', '2020-02-26 11:12:53'),
(216, 18, 304, 17, '0', '0', '1', '1', '0', '18', NULL, '2020-02-26 11:43:14', '2020-02-26 19:13:34'),
(213, 15, 262, 16, '0', '0', '1', '1', '0', NULL, NULL, '2020-02-25 10:08:22', '2020-02-25 18:46:49'),
(212, 16, 259, 16, '1', '1', '0', '1', '0', '16', NULL, '2020-02-25 08:47:16', '2020-02-25 19:28:52'),
(211, 16, 262, 16, '1', '1', '1', '1', '0', '16', '16', '2020-02-25 07:52:56', '2020-02-25 18:46:49'),
(210, 16, 263, 16, '0', '0', '0', '1', '0', '16', NULL, '2020-02-25 07:51:33', '2020-02-26 11:12:53'),
(209, 1, 244, 4, '0', '0', '1', '1', '0', NULL, NULL, '2020-02-25 04:42:10', NULL),
(208, 1, 251, 16, '0', '0', '0', '1', '0', NULL, NULL, '2020-02-25 04:41:42', '2020-02-25 11:41:52'),
(207, 1, 252, 16, '0', '0', '1', '1', '0', NULL, NULL, '2020-02-25 04:41:38', '2020-02-25 11:41:39');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) NOT NULL,
  `batch` int(11) NOT NULL
) ;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2020_01_31_064907_create_posts_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) NOT NULL,
  `token` varchar(191) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('johnpascalnew@mailinator.com', '$2y$10$ipHmEC7kGeYDvLz166wxAOSPafluKoSHra.tzDkeMMK0rEbU5d3Wm', '2020-02-14 14:39:22');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `post_type` varchar(191) DEFAULT NULL,
  `post_description` longtext,
  `post_media_choice` enum('0','1') NOT NULL DEFAULT '0' COMMENT '1=Live,0=Local',
  `post_privacy` enum('0','1') NOT NULL DEFAULT '0' COMMENT '1=All,0=Me',
  `is_like_post` enum('0','1') NOT NULL DEFAULT '0' COMMENT '1=Yes,0=No',
  `is_pray_post` enum('0','1') NOT NULL DEFAULT '0' COMMENT '1=Yes,0=No',
  `is_active` enum('0','1') NOT NULL DEFAULT '1' COMMENT '1=Yes,0=No',
  `is_deleted` enum('0','1') NOT NULL DEFAULT '0' COMMENT '1=Yes,0=No',
  `post_views` int(11) NOT NULL,
  `created_by` varchar(191) DEFAULT NULL,
  `updated_by` varchar(191) DEFAULT NULL,
  `created_at` timestamp NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `user_id`, `post_type`, `post_description`, `post_media_choice`, `post_privacy`, `is_like_post`, `is_pray_post`, `is_active`, `is_deleted`, `post_views`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(277, 17, 'text', 'ASFDFDSFadsfsdaf adgsdfgsdfgsfdgfsdg', '0', '1', '0', '0', '0', '1', 0, '17', '17', '2020-02-26 05:05:27', '2020-02-26 12:12:11'),
(276, 17, 'text', 'efasdfasdf\r\nsdafasdffasd\r\nasdfadsasfdasdfdsfa\r\n\r\n\r\n\r\n\r\n\r\nasdfdasfasd', '0', '0', '0', '0', '0', '1', 0, '17', '17', '2020-02-26 05:00:20', '2020-02-26 12:12:15'),
(274, 17, 'text', 'adasdsa', '0', '0', '0', '0', '0', '1', 0, '17', '17', '2020-02-26 04:54:50', '2020-02-26 11:56:27'),
(275, 17, 'text', 'aaa', '0', '0', '1', '1', '0', '1', 0, '17', '17', '2020-02-26 04:56:41', '2020-02-26 11:59:48'),
(273, 17, 'text', 'amit', '0', '0', '0', '0', '0', '1', 0, '17', '17', '2020-02-26 04:53:41', '2020-02-26 11:54:14'),
(272, 17, 'text', 'MIT', '0', '0', '0', '0', '0', '1', 0, '17', '17', '2020-02-26 04:51:29', '2020-02-26 11:54:25'),
(269, 1, 'image', 'https://kyndspace.com/betamattblog/public/uploads/postsimages/1582623273_1 (2).jpg', '0', '0', '0', '0', '0', '1', 0, '1', '1', '2020-02-25 09:34:33', '2020-02-25 16:35:02'),
(270, 17, 'text', 'sdfdsg', '0', '0', '1', '0', '0', '1', 0, '17', '17', '2020-02-26 04:31:31', '2020-02-26 11:56:31'),
(271, 17, 'text', 'dssdds', '0', '0', '0', '0', '0', '1', 0, '17', '17', '2020-02-26 04:40:23', '2020-02-26 11:54:29'),
(268, 1, 'image', 'https://kyndspace.com/betamattblog/public/uploads/postsimages/1582623210_Video walls 2.jpg', '0', '0', '0', '0', '0', '1', 0, '1', '1', '2020-02-25 09:33:30', '2020-02-25 16:35:14'),
(267, 15, 'image', 'https://kyndspace.com/betamattblog/public/uploads/postsimages/1582621903_1582598211_BObama_Img.jpg', '0', '0', '1', '0', '0', '1', 0, '15', '15', '2020-02-25 09:11:43', '2020-02-25 17:55:29'),
(266, 15, 'image', 'https://kyndspace.com/betamattblog/public/uploads/postsimages/1582621703_1582598372_BObama_Img2.jpg', '0', '0', '1', '1', '1', '0', 0, '15', '15', '2020-02-25 09:08:23', '2020-02-25 17:55:45'),
(260, 9, 'image', 'https://kyndspace.com/betamattblog/public/uploads/postsimages/1582615573_img_snow_wide.jpg', '0', '0', '1', '0', '1', '0', 0, '9', NULL, '2020-02-25 07:26:13', NULL),
(259, 16, 'image', 'https://kyndspace.com/betamattblog/public/uploads/postsimages/1582615322_Portrait+MIS+graduates+2017+www.starupphoto.com-3+copy.jpg', '0', '1', '0', '0', '1', '0', 0, '16', '16', '2020-02-25 07:22:02', '2020-02-25 15:47:04'),
(258, 16, 'text', 'asdfasfasdfasdfdas', '0', '0', '0', '0', '0', '1', 0, '16', '16', '2020-02-25 07:16:44', '2020-02-25 14:16:57'),
(257, 9, 'text', 'hujghjg', '0', '0', '1', '1', '1', '0', 0, '9', NULL, '2020-02-25 05:55:35', NULL),
(253, 1, 'text', 'sdfsdfdfads', '0', '0', '0', '0', '1', '0', 0, '1', NULL, '2020-02-25 04:59:04', NULL),
(254, 1, 'image', 'https://kyndspace.com/betamattblog/public/uploads/postsimages/1582606768_11.7.16-motivational-monday.png', '0', '0', '0', '0', '1', '0', 0, '1', NULL, '2020-02-25 04:59:28', NULL),
(255, 9, 'image', 'https://kyndspace.com/betamattblog/public/uploads/postsimages/1582607068_11.7.16-motivational-monday.png', '0', '0', '0', '0', '1', '0', 0, '9', NULL, '2020-02-25 05:04:29', NULL),
(256, 16, 'image', 'https://kyndspace.com/betamattblog/public/uploads/postsimages/1582610108_cs_ace-05hc_1.jpg', '0', '0', '0', '0', '0', '1', 0, '16', '16', '2020-02-25 05:55:08', '2020-02-25 14:40:12'),
(265, 15, 'text', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsumsfadddddddddddddddddddddddddddddddddddddddd', '0', '0', '0', '0', '1', '0', 0, '15', '15', '2020-02-25 09:04:02', '2020-02-25 16:04:34'),
(252, 16, 'video', 'https://www.youtube.com/watch?v=CRHsb0usNV4', '1', '1', '1', '1', '1', '0', 0, '16', '16', '2020-02-25 04:31:15', '2020-02-25 11:31:44'),
(251, 16, 'text', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '0', '1', '0', '1', '0', '1', 0, '16', '16', '2020-02-25 04:30:05', '2020-02-25 13:06:29'),
(250, 16, 'image', 'https://kyndspace.com/betamattblog/public/uploads/postsimages/1582604953_11.7.16-motivational-monday.png', '0', '0', '1', '0', '0', '1', 0, '16', '16', '2020-02-25 04:29:13', '2020-02-25 14:39:55'),
(249, 16, 'image', 'https://kyndspace.com/betamattblog/public/uploads/postsimages/1582604910_article_full@1x.jpg', '0', '0', '1', '1', '1', '0', 0, '16', NULL, '2020-02-25 04:28:30', NULL),
(246, 4, 'image', 'https://kyndspace.com/betamattblog/public/uploads/postsimages/1582598372_BObama_Img2.jpg', '0', '0', '0', '0', '1', '0', 0, '4', NULL, '2020-02-25 02:39:32', NULL),
(247, 4, 'video', 'https://kyndspace.com/betamattblog/public/uploads/postsvideos/1582603050_great_passion_play_promo.mp4', '0', '0', '0', '0', '1', '0', 0, '4', NULL, '2020-02-25 03:57:37', NULL),
(248, 4, 'video', 'https://kyndspace.com/betamattblog/public/uploads/postsvideos/1582603066_great_passion_play_promo.mp4', '0', '0', '0', '0', '1', '0', 0, '4', NULL, '2020-02-25 03:57:59', NULL),
(245, 4, 'image', 'https://kyndspace.com/betamattblog/public/uploads/postsimages/1582598211_BObama_Img.jpg', '0', '0', '0', '0', '1', '0', 0, '4', '4', '2020-02-25 02:21:11', '2020-02-25 09:36:51'),
(244, 4, 'text', 'Testing Testing Card 1 Testing\r\n\r\n\r\nTesting Testing Card 1 3 Lines', '0', '1', '1', '1', '1', '0', 0, '4', '4', '2020-02-25 01:29:26', '2020-02-25 09:16:27'),
(278, 17, 'text', 'hello every one this is a,\r\nmitsfd asd adsfmkasdjf dsgajk\r\nlasdf', '0', '0', '0', '0', '0', '1', 0, '17', '17', '2020-02-26 05:11:13', '2020-02-26 12:11:56'),
(279, 17, 'text', 'Hello this is amit form asfd\'dsfn\r\n\r\nsdfdsf\r\n\r\nsdfafd', '0', '0', '0', '0', '0', '1', 0, '17', '17', '2020-02-26 05:11:45', '2020-02-26 12:11:52'),
(280, 17, 'text', 'amit kumar sadfasdf amit kumar sadfasdf amit kumar sadfasdf amit kumar sadfasdf amit kumar sadfasdf amit kumar sadfasdf amit kumar sadfasdf', '0', '0', '0', '0', '0', '1', 0, '17', '17', '2020-02-26 05:12:32', '2020-02-26 13:29:23'),
(281, 17, 'text', 'sdfvsadfsdf\r\n\r\nadsfasdf\r\n\r\n\r\nadsfasdfsdaf', '0', '0', '0', '0', '0', '1', 0, '17', '17', '2020-02-26 06:10:09', '2020-02-26 13:29:19'),
(282, 17, 'text', 'fasdfasdfasdf\r\n\r\n\r\nsafsadf\r\n\r\nsadf', '0', '0', '0', '0', '0', '1', 0, '17', '17', '2020-02-26 06:19:28', '2020-02-26 13:29:15'),
(283, 17, 'text', 'dsdfsd', '0', '0', '0', '0', '0', '1', 0, '17', '17', '2020-02-26 06:23:17', '2020-02-26 13:29:12'),
(284, 17, 'text', 'fgfgfg', '0', '0', '0', '0', '0', '1', 0, '17', '17', '2020-02-26 06:24:53', '2020-02-26 13:25:35'),
(285, 17, 'text', 'ddddd', '0', '0', '0', '0', '0', '1', 0, '17', '17', '2020-02-26 06:25:47', '2020-02-26 13:25:58'),
(286, 17, 'text', '4', '0', '0', '0', '0', '0', '1', 0, '17', '17', '2020-02-26 06:27:42', '2020-02-26 13:29:08'),
(287, 17, 'text', 'dd', '0', '0', '0', '0', '1', '0', 0, '17', NULL, '2020-02-26 06:29:28', NULL),
(349, 4, 'image', 'https://kyndspace.com/betamattblog/public/uploads/postsimages/1582767017_beta.jpg', '0', '0', '0', '0', '1', '0', 0, '4', NULL, '2020-02-27 01:30:19', NULL),
(350, 4, 'text', 'Testing Testing First Line\r\n\r\nTesting Testing Third Line\r\n\r\nTesting Testing Fifth Line\r\n\r\nTesting Testing Seventh Line\r\n\r\nTesting Testing Ninth Line\r\n\r\nTesting Testing Eleventh Line', '0', '0', '0', '0', '1', '0', 0, '4', NULL, '2020-02-27 01:34:12', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) NOT NULL,
  `username` varchar(191) NOT NULL,
  `email` varchar(191) NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) NOT NULL,
  `hd_password` varchar(191) DEFAULT NULL,
  `firstname` varchar(191) DEFAULT NULL,
  `lastname` varchar(191) DEFAULT NULL,
  `gender` varchar(191) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `phone` varchar(191) DEFAULT NULL,
  `address` text,
  `city` varchar(191) DEFAULT NULL,
  `state` varchar(191) DEFAULT NULL,
  `country` varchar(191) DEFAULT NULL,
  `zipcode` varchar(191) DEFAULT NULL,
  `lat` varchar(191) DEFAULT NULL,
  `lng` varchar(191) DEFAULT NULL,
  `profilepic` text,
  `description` text,
  `role` varchar(191) DEFAULT NULL,
  `device` text,
  `browser` text,
  `ipaddress` varchar(191) DEFAULT NULL,
  `active_code` varchar(191) DEFAULT NULL,
  `is_approved` enum('0','1','2') NOT NULL DEFAULT '1' COMMENT '1=Approved,0=Not Approved,2=Declined',
  `is_active` enum('0','1') NOT NULL DEFAULT '1' COMMENT '1=Active,0=Inactive',
  `is_deleted` enum('0','1') NOT NULL DEFAULT '0' COMMENT '1=Deleted,0=Not Deleted',
  `is_online` enum('0','1') NOT NULL DEFAULT '0' COMMENT '1=Online,0=Offline',
  `llhid` varchar(191) DEFAULT NULL,
  `logins` int(11) NOT NULL DEFAULT '0',
  `last_login` timestamp NULL DEFAULT NULL,
  `login_status` enum('0','1') DEFAULT '1' COMMENT '1=enable,0=disable',
  `register_status` enum('0','1') DEFAULT '1' COMMENT '1=enable,0=disable',
  `fp_token` varchar(191) DEFAULT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_by` varchar(191) DEFAULT NULL,
  `updated_by` varchar(191) DEFAULT NULL,
  `created_at` timestamp NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `Register_disableby` int(11) DEFAULT NULL
) ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `email`, `email_verified_at`, `password`, `hd_password`, `firstname`, `lastname`, `gender`, `dob`, `phone`, `address`, `city`, `state`, `country`, `zipcode`, `lat`, `lng`, `profilepic`, `description`, `role`, `device`, `browser`, `ipaddress`, `active_code`, `is_approved`, `is_active`, `is_deleted`, `is_online`, `llhid`, `logins`, `last_login`, `login_status`, `register_status`, `fp_token`, `remember_token`, `created_by`, `updated_by`, `created_at`, `updated_at`, `Register_disableby`) VALUES
(1, 'Beta Matt', 'betamattgmai', 'betamatt@gmail.com', NULL, '$2y$10$bUGirvc77rLso3r7Dby4PuXu/k6SU7laQvgk3emQfiReTJob5ewoS', '123456', 'Bet', 'Mat', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1581912167_LOGO.png', NULL, '1', 'Apache/2.4.37 (Unix) OpenSSL/1.0.2p PHP/7.2.12 mod_perl/2.0.8-dev Perl/v5.16.3', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:66.0) Gecko/20100101 Firefox/66.0', '2130706433', 'c9uzgwlttmsaqludscngyoqzer6gzl', '1', '1', '0', '0', NULL, 0, NULL, '1', '1', NULL, 'sWpjWOyUwVfv9segEwqo3vlsX513EPPxpdy2NKW6mzwXBu5pZfUnoc6wfJBP', NULL, '1', '2020-02-04 01:21:03', '2020-02-26 11:30:55', 16),
(3, 'ssss dd', 'sdsdgmailcom', 'sdsd@gmail.com', NULL, '$2y$10$E1YUVro2vwl1lylSKttdR.5zEASwIjFasFGMmfZOSd6u5JHHVyYZC', 'sdsdsd', 'ssss', 'dd', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2', 'Apache', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36', '1860289613', 'xd7dhj1sea4v5afrg3ramxu2jz6rym', '1', '0', '1', '0', NULL, 0, NULL, '1', '1', NULL, 'boQQprHlWph8YGAP6LknXn4cSBPRgiKO2MUn5DeFLezwbyCBM2DZLGdhruOM', NULL, NULL, '2020-02-04 20:25:48', '2020-02-21 19:35:45', NULL),
(5, 'demo name', 'dev3bdplgmai', 'dev3.bdpl@gmail.com', NULL, '$2y$10$OqklYj3HxWMwc8gG4G8neOwoZmqMeAHNFihtOBRoS34YlEcQRWFgS', '-*,H~2:2[UWJ', 'demo', 'name', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2', 'Apache', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', '2058204539', 'rrbhpygotjakqzv4pydatatrj6whjc', '1', '1', '0', '0', NULL, 0, NULL, '1', '1', NULL, '7VCXOFwicf2ZkintJsU2kAUSb5DJtylDfOP0NtyjoUxj6tAyn58ZeBCP76bV', NULL, NULL, '2020-02-07 19:15:51', '2020-02-17 06:22:00', NULL),
(4, 'test matt', 'mattdburdgma', 'mattdburd@gmail.com', NULL, '$2y$10$i2/ErZdq6pVJf8j1yYoj5eIIW6pQ/9qwhaJyQs8Jj2BgWlp.Becza', 'test123', 'test', 'matt', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1582595898_HW_JPeg.jpg', NULL, '2', 'Apache', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36', '-1899649618', 'ac1kstpxzkufszspzlfuhoetbyxb5l', '1', '1', '0', '0', NULL, 0, NULL, '1', '1', NULL, 'VeeLoSNDWEURoCxeIXCw5bJ98ZbT6GW2D7iL4cWCSkNQbGUh8SEvDOXSTqA5', NULL, '4', '2020-02-07 08:04:08', '2020-02-27 03:40:06', NULL),
(13, 'jack robin', 'jackrobinmai', 'jackrobin@mailinator.com', NULL, '$2y$10$i/hq4Q3sR2zwbnTwrwCjs.bD2gstG5anNOrpGMgvtNkpL3rOJVDxm', '123456', 'jack', 'robin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', 'Apache', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', '2058215600', 'xxdtcvpdk822pmelxik2qrwawarrsy', '1', '1', '0', '0', NULL, 0, NULL, '1', '1', NULL, 'OTX06MpSjNemwI4UWxuB3rl6gSboRJd1YE1715g9IhwMv6A8V9M0yEGR70RE', NULL, NULL, '2020-02-17 11:03:07', '2020-02-26 11:30:55', 16),
(6, 'amit kumar', 'amitgmailcom', 'amit@gmail.com', NULL, '$2y$10$/KjX4UPOs/hIHsCKQAZV2.kCJDpt9GogUyibb6vX0x1/lSAWeOC3m', 'm$*,T_;E^5^7', 'amit', 'kumar', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', 'Apache', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36', '2058205088', 'fm7cnqnykphurfk5tycxecitr7ngz7', '1', '0', '1', '0', NULL, 0, NULL, '1', '1', NULL, 'zUp7Pd7fliB20GYTqXP4iiR2C2WV3FcFEYi0Buprqrt2zxAPHpPc2hfVmTr0', NULL, NULL, '2020-02-10 11:18:55', '2020-02-26 11:30:55', 16),
(7, 'test tstiny', 'testgmailcom', 'test@gmail.com', NULL, '$2y$10$Owrn7KGVbFxYrKEFFT.BiOmKnv9r68aB0TnT.usQuPby6q8iWRFay', '12345678', 'test', 'tstinysdfsdf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2', 'Apache', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36', '2058205088', '0jhludccnfjxtvchrl0jqertxy22o3', '1', '1', '0', '0', NULL, 0, NULL, '1', '1', NULL, 'VjenVaTEtygDrjQEnY7yEcvlVr18Y7QABXOmP28RqVHPaW9iu7Ht6bgIl2JA', NULL, '7', '2020-02-10 16:54:15', '2020-02-19 07:08:59', NULL),
(8, 'Binary Data', 'binarydatasa', 'binarydata.sale@gmail.com', NULL, '$2y$10$xKNwIBVN2xxlZn3N.SY9C.VaY2Cwpv/P4aa9QSO.u1jFhGXxcAU0O', '123456', 'Binary', 'Data', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2', 'Apache', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36', '2058205088', '5yenwnwhszm1bxmjbdjto7hy0xcabm', '1', '0', '1', '0', NULL, 0, NULL, '1', '1', NULL, '47YMrW3qOLm7UNCH7xhFDOZHm8zYXSSA4cvDQlQuuyuaoOACjC25auCK6wqd', NULL, NULL, '2020-02-14 11:24:04', '2020-02-25 17:20:35', NULL),
(9, 'mohit kumar', 'mohitgmailco', 'mohit@gmail.com', NULL, '$2y$10$P0n975sb/lG3PwvHs4pKPOemtGXrMwkEzfOkhsozMMQGJoqsTvfmy', '123456', 'mohit', 'kumar', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2', 'Apache', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36', '2058205088', 'uyzngjlf2c1uk8e4pi84nuy2odtfum', '1', '0', '1', '0', NULL, 0, NULL, '1', '1', NULL, 'b1FUA32moisRfAvMMkCXFij49hLIADvO18RIzXfhIntXWtvlHMkmV6Cdvk60', NULL, NULL, '2020-02-14 11:29:29', '2020-02-25 19:34:50', NULL),
(10, 'amit kumar', 'amit111gmail', 'amit111@gmail.com', NULL, '$2y$10$KHv3cxRVVSx4w3FvZMtVBetfqBRv7azTp2C/VCtuftdCC7oV5bf1G', '123456', 'amit', 'kumar', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2', 'Apache', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36', '2058205088', 'rbptbapt6r28iuy8yeodewxsdxsc7w', '1', '0', '1', '0', NULL, 0, NULL, '1', '1', NULL, 'ebAFzNuDrNOvorF9C4WrJeyd3sKC6xAIVm0RhMZYZrSrmtYysSBTvdxfAOOi', NULL, NULL, '2020-02-14 14:12:33', '2020-02-26 11:22:41', NULL),
(11, 'James Peter', 'jamespetergm', 'jamespeter@gmail.com', NULL, '$2y$10$wdQ02p1bGboeT74RZImt4eqQ5aT1CEE0mAc04Hirlbr2JoJ14VGYG', '123456', 'James', 'Peter', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2', 'Apache', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', '2058205088', 'qy16fsik2fxyy8juldiycbzljmsqbg', '1', '1', '0', '0', NULL, 0, NULL, '1', '1', NULL, 'Qlk5SR1Uq1LGK0to4D9O0r8xbJwfb8ebxmMqOTEmD8RCdv3wOsD4AbfSeD59', NULL, NULL, '2020-02-14 14:35:12', '2020-02-17 06:21:26', NULL),
(12, 'John Pascal', 'johnpascalne', 'johnpascalnew@mailinator.com', NULL, '$2y$10$WIPZf2xQSZHWadQhl66DAe3bqrlTmSQi8pI7g7j5lXZGcmW/iQTwi', '123456', 'John', 'Pascal', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2', 'Apache', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', '2058205088', 'uafwyilvwtjh7l1sddux1lvcpn1rcq', '1', '1', '0', '0', NULL, 0, NULL, '1', '1', NULL, 'dcKHWDY8bHOU3fMhOAjUwBvkPNwFhYasRb4aml2G7uukTMbhthXS8qiv9T60', NULL, NULL, '2020-02-14 14:36:42', '2020-02-17 06:21:22', NULL),
(14, 'Nitin kumar', 'nitingmailco', 'nitin@gmail.com', NULL, '$2y$10$RL9SreOGXDcWe0BGEx6KN.gPoVvwSPwikAsy7Ug7OPqRq9lCzh6/W', '123456', 'Nitin', 'kumar', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', 'Apache', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', '2058216678', 'uol3vow2dsbfrfkwlcwpb5cyqcgsdk', '1', '1', '0', '0', NULL, 0, NULL, '1', '1', NULL, 'cFmeedHlwQs68XQ2m4a2zG5yXRu3LJF6hnMDJe2MvcwSr8raRbg2WN08PHK8', NULL, NULL, '2020-02-17 13:22:44', '2020-02-26 11:30:55', 16),
(15, 'testing testing', 'betamatt1gma', 'betamatt1@gmail.com', NULL, '$2y$10$mz4z9Fp/1GmdP2rUG5b43OiVzXr73.FMJljzLbK67Y7RdV4AGFlFa', '123456', 'testing', 'testing', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', 'Apache', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36', '-1899649618', 'nevb6xodobnwveiqujvbeirbohe2mr', '1', '1', '0', '0', NULL, 0, NULL, '1', '1', NULL, 'N7e5TxsPMiBfQEwYr8PFSh1xxvRa2cu6ZtKwBQiIvjL3JIFwJkFrwm6MBlT4', NULL, '15', '2020-02-19 08:46:48', '2020-02-26 11:30:55', 16),
(16, 'Jagmohan Krishan', 'jagmohanmail', 'jagmohan@mailinator.com', NULL, '$2y$10$NYca90sMf9wELbCgcpP7HOYaqZbEZoxvvQHtw.sXJy84udj/307I6', '123456', 'Jagmohan', 'Krishan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1582605546_11.7.16-motivational-monday.png', NULL, '1', 'Apache', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36', '2058176062', 'jxvcwi45vmittsgnb40uhvfntw3ajf', '1', '1', '0', '0', NULL, 0, NULL, '1', '1', NULL, 'H52YyWus5N4tYwjJVbGmCMRU0MruW6s8FyCEAy2Bwqa5nYvr3Mwd7IpJaKHY', NULL, '16', '2020-02-25 11:25:47', '2020-02-26 04:30:59', 16),
(17, 'Dummy user', 'dummyusergma', 'dummyuser@gmail.com', NULL, '$2y$10$ymC5gqBn48i3P.ys7LTmv.hMKf9jNNT9CEz/pSEMG2LB/jud2/cI6', '123456', 'Dummy', 'user', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2', 'Apache', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36', '2058176062', 'amyowutqbkxzkb7ewpqs7irvnignmv', '1', '1', '0', '0', NULL, 0, NULL, '1', '1', NULL, 'rxStIn4lRKla3xYYC4tkamDT7uNNMFN7mEXP50CK0rRwcCf7j2aveXGMNNVg', NULL, NULL, '2020-02-26 11:31:25', '2020-02-26 09:56:58', NULL),
(18, 'binarydata kumar', 'binarydatagm', 'binarydata@gmail.com', NULL, '$2y$10$B3JFir27Cmhm3b60Y6CK8eHpk6F4NUz0Yi3N0eqKMs3i2WgkAFEeW', '123456', 'binarydata', 'kumar', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', 'Apache', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36', '2058176062', 'c0dhfpegqolh6zkleo9rhcdmeu5tvo', '1', '1', '0', '0', NULL, 0, NULL, '1', '1', NULL, NULL, NULL, NULL, '2020-02-26 16:57:38', '2020-02-26 11:51:57', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `errorlog`
--
ALTER TABLE `errorlog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `feedposts`
--
ALTER TABLE `feedposts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_username_unique` (`username`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `errorlog`
--
ALTER TABLE `errorlog`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `feedposts`
--
ALTER TABLE `feedposts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
