@extends('layouts.lgrg-default')
@section('title', 'BetaMatt | Login Register')

@section('content')

<?php
$rem_email = "rem_email";
$rem_pwd = "rem_pwd";
$rem_chk = "rem_chk";
?>
<div class="row log" style="border-top: 1px solid #ccc;">
         <div class="col-lg-6 login" data-aos="fade-right">
            <h3 class="head"> Please Login Here</h3>
            <div class="form align-items-center">

              @if (count($errors->login) > 0)
              <div class="alert alert-danger">
                <ul>
                  @foreach ($errors->login->all() as $error)
                  <P>{{ $error }}</p>
                  @endforeach
                </ul>
              </div>
              @endif 
              @if (Session::has('message'))
              <div class="alert alert-danger allrquired_login">{{ Session::get('message') }}</div>
              @endif

               <form action="{{url('/userlogin')}}" method="POST" id="LogForm">
                {{ csrf_field() }}
                <input type="hidden" name="redirurl" value="{{ $_SERVER['REQUEST_URI'] }}">
                  <div class="input-group form-group">
                     <label> Email or Username
                     <input type="text" class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}" name="username" id="username" value="<?php echo @$_COOKIE[$rem_email]; ?>"  placeholder="Enter Your Email Address">
                     </label>
                     @if ($errors->has('username'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                                @endif
                  </div>
                  <div class="input-group form-group">
                     <label> Password
                     <input id="password-field" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Password" value="<?php echo @$_COOKIE[$rem_pwd]; ?>" required>
                     <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                     </label>

                     @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                  </div>
                  <div class="input-group form-group">
                    <!--  <label> 
                     <input type="checkbox" class="remember" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>Remember My Password</label> -->

                      <label>         
                      <input type="checkbox" name="remeber_me" value="1" <?php if(@$_COOKIE[$rem_chk] == 1) { echo 'checked';} ?>/><span class="rembe_me" style=" margin: 0px 20px 0 0px;">Remember me</span>

                     <label>
                     <a href="{{ route('password.request') }}" class="forgot ">Forgot your password?</a>
                     </label>
                  </div>
                  <div class="input-group form-group">
                     <button type="submit" name="SignIn" id="SignIn" class="btn login_btn">LOGIN TO BETAMATT</button>
                  </div>
               </form>
            </div>
         </div>

            <?php 

            $user_enabledata =   DB::table('users')->where('role','1')->where('register_status', '0')->orderBy('id','DESC')->first();
            if (!empty($user_enabledata)) {
              $user_status_register = $user_enabledata->register_status;
            }
            ?>
             <div class="col-lg-6 Signup" data-aos="fade-left">
             <?php if(isset( $user_status_register) &&  $user_status_register == '0') { ?>
              <h4 class="Signup_disable">New account registration is disabled by admin Side</h4><?php  } else {
              } ?>
            <h3 class="head"> Signup Here to Join BetaMatt</h3>
            <div class="form align-items-center">

            @if (count($errors->register) > 0)
            <div class="alert alert-danger allrquired_resgister">
              <ul>
                @foreach ($errors->register->all() as $error)
                <P>{{ $error }}</p>
                @endforeach
              </ul>
            </div>
            @endif
              <form action="{{url('/userregister')}}" method="POST" id="regForm">
                {{ csrf_field() }}
                <input type="hidden" name="redirurl" value="{{ $_SERVER['REQUEST_URI'] }}">
                  <div class="input-group form-group">
                     <label> First Name
                     <input type="text" class="form-control{{ $errors->has('firstname') ? ' is-invalid' : '' }} sign" name="firstname" id="firstname" value="{{ old('firstname') }}"  placeholder="Enter Your First Name" required <?php echo isset( $user_status_register) &&  $user_status_register == '0' ? 'readonly' : ''; ?>>
                     </label>
                     @if ($errors->has('firstname'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('firstname') }}</strong>
                        </span>
                    @endif
                  </div>
                  <div class="input-group form-group">
                     <label> Last Name
                     <input type="text" class="form-control{{ $errors->has('lastname') ? ' is-invalid' : '' }} sign" name="lastname" id="lastname" value="{{ old('lastname') }}" placeholder="Enter Your First Name"  required <?php echo isset( $user_status_register) &&  $user_status_register == '0' ? 'readonly' : ''; ?>>
                     </label>
                     @if ($errors->has('lastname'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('lastname') }}</strong>
                        </span>
                    @endif
                  </div>
                  <div class="input-group form-group">
                     <label> Email Address
                     <input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }} sign" name="email" id="email" value="{{ old('email') }}" placeholder="Enter Your Email Address" required <?php echo isset( $user_status_register) &&  $user_status_register == '0' ? 'readonly' : ''; ?>>
                     </label>
                     @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                  </div>
                  <div class="input-group form-group">
                     <label> Password
                     <input id="password-field1" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Password" required <?php echo isset( $user_status_register) &&  $user_status_register == '0' ? 'readonly' : ''; ?>>
                      <span toggle="#password-field1" class="fa fa-fw fa-eye field-icon toggle-password-sign"></span> 
                     </label>

                     @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif

                  </div>
                  <div class="input-group form-group">
                     <button type="submit" name="SignUp" id="SignUp" class="btn sign_btn" <?php echo isset( $user_status_register) &&  $user_status_register == '0' ? 'disabled' : ''; ?>>SIGNUP HERE</button>
                  </div>
               </form>
            </div>
         </div>
      </div>
<style type="text/css">
  
h4.Signup_disable {
    margin: 0 auto;
    display: block;
    color: red;
    width: 75%;
    margin-bottom: 20px;

}

</style>
@endsection
