@extends('layouts.lgrg-default')
@section('title', 'BetaMatt | Login Register')

@section('content')
<div class="row log" style="border-top: 1px solid #ccc;">
         <div class="col-lg-6 login" data-aos="fade-right">
            <h3 class="head"> Please Login Here</h3>
            <div class="form align-items-center">

              @if (count($errors->login) > 0)
              <div class="alert alert-danger">
                <ul>
                  @foreach ($errors->login->all() as $error)
                  <P>{{ $error }}</p>
                  @endforeach
                </ul>
              </div>
              @endif 
              @if (Session::has('message'))
              <div class="alert alert-warning">{{ Session::get('message') }}</div>
              @endif

               <form action="{{url('/userlogin')}}" method="POST" id="LogForm">
                {{ csrf_field() }}
                <input type="hidden" name="redirurl" value="{{ $_SERVER['REQUEST_URI'] }}">
                  <div class="input-group form-group">
                     <label> Email or Username
                     <input type="text" class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}" name="username" id="username" value="{{ old('username') }}"  placeholder="Enter your Email Address" required>
                     </label>
                     @if ($errors->has('username'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                                @endif
                  </div>
                  <div class="input-group form-group">
                     <label> Password
                     <input id="password-field" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Password" required>
                     <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                     </label>

                     @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                  </div>
                  <div class="input-group form-group">
                     <label> 
                     <input type="checkbox" class="remember" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>Remember My Password</label>
                     <label>
                     <a href="{{ route('password.request') }}" class="forgot ">Forgot your password?</a>
                     </label>
                  </div>
                  <div class="input-group form-group">
                     <button type="submit" name="SignIn" id="SignIn" class="btn login_btn">LOGIN TO BETAMATT</button>
                  </div>
               </form>
            </div>
         </div>

         <div class="col-lg-6 Signup" data-aos="fade-left">
            <h3 class="head"> Signup Here to Join BetaMatt</h3>
            <div class="form align-items-center">

            @if (count($errors->register) > 0)
            <div class="alert alert-danger">
              <ul>
                @foreach ($errors->register->all() as $error)
                <P>{{ $error }}</p>
                @endforeach
              </ul>
            </div>
            @endif
            
              <form action="{{url('/userregister')}}" method="POST" id="regForm">
                {{ csrf_field() }}
                <input type="hidden" name="redirurl" value="{{ $_SERVER['REQUEST_URI'] }}">
                  <div class="input-group form-group">
                     <label> First Name
                     <input type="text" class="form-control{{ $errors->has('firstname') ? ' is-invalid' : '' }} sign" name="firstname" id="firstname" value="{{ old('firstname') }}" placeholder="Enter your First Name" required>
                     </label>
                     @if ($errors->has('firstname'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('firstname') }}</strong>
                        </span>
                    @endif
                  </div>
                  <div class="input-group form-group">
                     <label> Last Name
                     <input type="text" class="form-control{{ $errors->has('lastname') ? ' is-invalid' : '' }} sign" name="lastname" id="lastname" value="{{ old('lastname') }}" placeholder="Enter your First Name" required>
                     </label>
                     @if ($errors->has('lastname'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('lastname') }}</strong>
                        </span>
                    @endif
                  </div>
                  <div class="input-group form-group">
                     <label> Email Address
                     <input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }} sign" name="email" id="email" value="{{ old('email') }}" placeholder="Enter your Email Address" required>
                     </label>
                     @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                  </div>
                  <div class="input-group form-group">
                     <label> Password
                     <input id="password-field1" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Password" required>
                      <span toggle="#password-field1" class="fa fa-fw fa-eye field-icon toggle-password-sign"></span> 
                     </label>

                     @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif

                  </div>
                  <div class="input-group form-group">
                     <button type="submit" name="SignUp" id="SignUp" class="btn sign_btn">SIGNUP HERE</button>
                  </div>
               </form>
            </div>
         </div>
      </div>
@endsection
