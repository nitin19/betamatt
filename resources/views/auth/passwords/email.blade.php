@extends('layouts.lgrg-default')
@section('title', 'BetaMatt | Forgotpassword')

@section('content')
<style type="text/css">
    label {
    display: inline-block;
    margin-bottom: .5rem;
    text-align: center;
    margin: auto;
}
</style>
<hr>
<div class="col-lg-12 forgot">
   <h3 class="head" style="text-align: center;"> Forgot your Password?</h3>
   <p style="text-align: center; font-family: Montserrat">Enter your email address below. We'll look for your account and send you a password reset email.</p>
   <div class="form align-items-center">
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif
      <form method="POST" action="{{ route('password.email') }}">
        @csrf
         <div class="input-group form-group">
            <label> Email Address
            <!-- <input type="text" class="form-control" placeholder="Enter your Email Address"> -->

             <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="Enter your Email Address" required>

            </label>
            @if ($errors->has('email'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
         </div>
         <div class="input-group form-group">
            <button type="submit" name="ForgotPassword" id="ForgotPassword" class="btn reset_btn">SUBMIT</button>
         </div>
      </form>
   </div>
</div>
@endsection
