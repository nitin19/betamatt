@extends('layouts.default')
@section('title', 'BetaMatt | Home')
@section('content')


<div class="container-fluid">
    <div class="container my-4">
    	<div class="post-error-message col-md-12">
				@if (session('error'))
				<p class="alert alert-danger" id="danger_timeout">
			      {{ session('error') }}
			    </p>
			    @endif
			    @if (session('success'))
			    <p class="alert alert-success" id="sucess_timeout">
			      {{ session('success') }}
			  </p>
			    @endif
	    </div>
		<p class="welcome"> Welcome to <span style="color: #000;">Beta Matt</span></p>
		<div class="row user-profile">
			<div class="col-md-12">
				<h1 style="color:#000; font-size: 25px; font-family: Montserrat; font-weight: 600;" >My Profile</h1>
			</div>
		</div>
	     <?php $user = Auth::user(); ?>
		<div class='user-profile-container'>

			<div class="inner-profile container">
				<form method="post" name="user_editFrm" id="user_editFrm_{{$user->id}}" action="{{ url('/user/updatepasssword', $user->id) }}" enctype="multipart/form-data">
					@csrf
					<input type="hidden" class="useridDefault" id="useridDefault_{{$user->id}}" name="user_id" value="{{ $user->id }}">
					<div class="input-group form-group">
						<label> Old Password
							<input type="password" id="oldpassword-field" name="old_password" class="form-control" placeholder="Old Password" required />
							<span toggle="#oldpassword-field" class="fa fa-fw fa-eye field-icon toggle-password-old"></span>

						</label>
					</div>
	                <div class="input-group form-group">
						<label> New Password
							<input type="password" id="Newpassword-field" name="password" class="form-control" placeholder="New Password" required />
							<span toggle="#Newpassword-field" class="fa fa-fw fa-eye field-icon toggle-password-new"></span>
						</label>
					</div>
					<div class="input-group form-group">
						<label> Confirm Password
							<input type="password" id="Confirmpassword-field" name="password_confirmation" class="form-control" placeholder="Confirm Password" required>
							<span toggle="#Confirmpassword-field" class="fa fa-fw fa-eye field-icon toggle-password-confirm"></span>
						</label>
					</div>	                    
					<div class="input-group form-group">
						<button type="submit" value="updateuserbtn" id="userprofiletBtnDefault_{{$user->id}}" class="btn  updateuser_btn">UPDATE</button>
					</div>		   
	        </div>  
       </div>
    </div>
</div> 

<script type="text/javascript">
        $( document ).ready(function() {
	      $(".toggle-password-old").click(function() {
	        $(this).toggleClass("fa-eye fa-eye-slash");
	        var input = $($(this).attr("toggle"));
	        if (input.attr("type") == "password") {
	          input.attr("type", "text");
	        } else {
	          input.attr("type", "password");
	        }
	      });
	      $(".toggle-password-new").click(function() {
	     
	        $(this).toggleClass("fa-eye fa-eye-slash");
	        var input = $($(this).attr("toggle"));
	        if (input.attr("type") == "password") {
	          input.attr("type", "text");
	        } else {
	          input.attr("type", "password");
	        }
	      });
	      $(".toggle-password-confirm").click(function() {
	     
	        $(this).toggleClass("fa-eye fa-eye-slash");
	        var input = $($(this).attr("toggle"));
	        if (input.attr("type") == "password") {
	          input.attr("type", "text");
	        } else {
	          input.attr("type", "password");
	        }
	      });
        });  
         AOS.init();
</script>

@endsection