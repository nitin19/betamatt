@extends('layouts.default')
@section('title', 'BetaMatt | Feed')

@section('content')
      <div class="container-fluid">
         <div class="container my-4">
            <div class="row posts">
               <div class="col-md-">
                  <h1 style="color:#000; font-size: 25px; font-family: Montserrat; font-weight: 600;" >Shared Posts</h1>
               </div>
               <div class="col-md-2 pop" id="create">
                  <h4 class="icon"><a href="#"><i class="fa fa-plus"></i></a></h4>
               </div>
            </div>
            <hr class="hor-line">
            <div class="row feed" id="shared-post">
               
               @if( count($posts) > 0 )
               @foreach($posts as $post)
               <?php 
               if (!in_array($post->id, $hide_feed_posts)) { ?>
               <div class="col-md-3 col-sm-12 col-xs-12 clearfix d-md-block pop-default" id="{{$post->id}}" data-toggle="modal" data-target="#myModalDefault_{{$post->id}}">
                  <div class="card mb-3">
                       @if($post->post_type=='image')
                     <div class="card-body" style="background: url({{ $post->post_description }});background-repeat:no-repeat;background-size: contain;background-position: 50%;"></div>
                       <!--  <img class="card-img-top" src="{{ $post->post_description }}" alt="postimage"> -->
                        @elseif($post->post_type=='video')
                        @if($post->post_media_choice=='0') 
                        <video class="card-img-top video" controls>
                         <source src="{{ $post->post_description }}" type="video/mp4">
                        </video>
                        @else
                        <?php
                        $videoLiveUrl = explode("=", $post->post_description);
                        if(!empty($videoLiveUrl[1])) { ?>
                        <img class="card-img-top video"src="https://img.youtube.com/vi/<?php echo $videoLiveUrl[1]; ?>/0.jpg"> 
                        <?php } ?>
                        @endif
                        @else
                         <?php
                         $post_data1 = ($post->post_description);                                
                         if(strlen($post_data1) > 315){
                         $stringCut1 = substr($post_data1,0, 315,)."...."; ?>                                                    
                          <p class="card-text">{{ $stringCut1 }}</p><?php                              
                         }else {  ?>                                                                   
                          <p class="card-text">{{ $post_data1}}</p>  
                        <?php } ?>
                        @endif

                  </div>
               </div>
               <?php } ?>
               @endforeach
               @endif


            </div>
         </div>
      </div>

      @if( count($posts) > 0 )
      @foreach($posts as $post)

      <div class="modal fade" id="myModalDefault_{{$post->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" data-toggle="modal">

         <div class="modal-dialog">
            <div class="modal-content">

            <form class="defaultpostfeedFrm" name="defaultpostfeedFrm" id="defaultpostfeedFrm_{{$post->id}}" method="POST" action="{{url('/feed/postfeed') }}">

                  {{ csrf_field() }}

                  <input type="hidden" class="postidDefault" name="postidDefault" id="postidDefault_{{$post->id}}" value="{{$post->id}}">
                  
                  <input type="hidden" class="postuseridDefault" name="postuseridDefault" id="postuseridDefault_{{$post->id}}" value="{{$post->user_id}}">

                  <input type="hidden" class="likeDefault" name="likeDefault" id="likeDefault_{{$post->id}}" value="<?php if (in_array($post->id, $like_feed_posts)) { echo '1'; } else { echo '0'; }?>">

                  <input type="hidden" class="prayDefault" name="prayDefault" id="prayDefault_{{$post->id}}" value="<?php if (in_array($post->id, $pray_feed_posts)) { echo '1'; } else { echo '0'; }?>">

                  <input type="hidden" class="hideDefault" name="hideDefault" id="hideDefault_{{$post->id}}" value="0">  

               <div class="modal-body">

               <p style="float: left; font-family: Montserrat;">{{$post->post_type}}</p>
               <p style="float: right; font-family: Montserrat;">Everyone</p>

               <button type="button" class="close" data-dismiss="modal"><span aria-hidden="false">&times;</span><span class="sr-only">Close</span></button>
                  
                  <hr class="feed_hrline">
                        
                  <div class="card-preview">

                     @if($post->post_type=='image')

                     <img class="card-img-feed" src="{{ $post->post_description }}" alt="postimage">

                     @elseif($post->post_type=='video')

                     @if($post->post_media_choice=='0')

                     <video class="card-img-top video" controls>
                        <source src="{{ $post->post_description }}" type="video/mp4">
                     </video>

                     @else

                     <?php
                        $videoLiveUrl = explode("=", $post->post_description);
                        if(!empty($videoLiveUrl[1])) { ?>
                     <iframe class="card-img-top video" src="https://www.youtube.com/embed/<?php echo $videoLiveUrl[1]; ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> 
                     <?php } ?>

                     @endif

                     @else

                     
                     <p class="text-preview">{{ $post->post_description }}</p>

                     @endif

                  </div>

               </div>
               <div class="modal-footer">
                  <div class="row footer">
                     <div class="col-md-3 footer-btn">
                        <div class="share isLikeDefault <?php if (in_array($post->id, $like_feed_posts)) { echo 'islikedyncls'; }?>" id="isLikeDefault_{{$post->id}}">
                           <img src="{{ url('/public') }}/images/like.png" class="share-img" />
                           <p style="font-family: Montserrat;">Like</p>
                        </div>
                     </div>
                     <div class="col-md-3 footer-btn">
                        <div class="share isPrayDefault <?php if (in_array($post->id, $pray_feed_posts)) { echo 'ispraydyncls'; }?>" id="isPrayDefault_{{$post->id}}">
                           <img src="{{ url('/public') }}/images/pray.png" class="share-img" />
                           <p style="font-family: Montserrat;">Pray</p>
                        </div>
                     </div>
                     <div class="col-md-3 footer-btn">
                        <div class="share isHideDefault" id="isHideDefault_{{$post->id}}">
                           <img src="{{ url('/public') }}/images/hide.png" class="del-img"/>
                           <p style="font-family: Montserrat;">Hide</p>
                        </div>
                     </div>

                     <div class="col-md-3 footer-btn">
                           <button type="submit" name="addpostfeedBtnDefault" id="addpostfeedBtnDefault_{{$post->id}}" class="save">
                              <img src="{{ url('/public') }}/images/save.png" class="save-img"/>
                              <p style="font-family: Montserrat;">Save</p>
                           </button>
                        </div>
                  </div>
               </div>

            </form>   

            </div>
         </div>
      </div>

      @endforeach
      @endif
<script type="text/javascript">
   $(document).ready(function(){

      $(".isLikeDefault").click(function(){
         var choiceId = $(this).attr('id');
         var postId = choiceId.replace('isLikeDefault_','');
         var islike = $('#likeDefault_'+postId).val();
         if(islike==0) {
            $('#isLikeDefault_'+postId).addClass('islikedyncls');
            $('#likeDefault_'+postId).val('1');
         } else {
            $('#isLikeDefault_'+postId).removeClass('islikedyncls');
            $('#likeDefault_'+postId).val('0');
         }
      }); 
   
      $(".isPrayDefault").click(function(){
         var choiceId = $(this).attr('id');
         var postId = choiceId.replace('isPrayDefault_','');
         var ispray = $('#prayDefault_'+postId).val();
         if(ispray==0) {
            $('#isPrayDefault_'+postId).addClass('ispraydyncls');
            $('#prayDefault_'+postId).val('1');
         } else {
            $('#isPrayDefault_'+postId).removeClass('ispraydyncls');
            $('#prayDefault_'+postId).val('0');
         }
      }); 

      $(".isHideDefault").click(function(){
         var choiceId = $(this).attr('id');
         var postId = choiceId.replace('isHideDefault_','');
         var ispray = $('#hideDefault_'+postId).val();
         if(ispray==0) {
            $('#isHideDefault_'+postId).addClass('ishidedyncls');
            $('#hideDefault_'+postId).val('1');
         } else {
            $('#isHideDefault_'+postId).removeClass('ishidedyncls');
            $('#hideDefault_'+postId).val('0');
         }
      });

      /*$(".isHideDefault").click(function(){
         if(!confirm("Are You Sure to hide this post?")) {
            event.preventDefault();
         } else {
          var choiceId = $(this).attr('id');
          var postId = choiceId.replace('isHideDefault_','');
          var ispray = $('#hideDefault_'+postId).val();
            if(ispray==0) {
               $('#isHideDefault_'+postId).addClass('ishidedyncls');
               $('#hideDefault_'+postId).val('1');
            } else {
               $('#isHideDefault_'+postId).removeClass('ishidedyncls');
               $('#hideDefault_'+postId).val('0');
            }  
         }
      });*/

   });
</script> 
<style type="text/css">
   .islikedyncls { 

      background-color: #ECECEC;
      text-align: center;

    }
   .ispraydyncls { 

      background-color: #ECECEC;
      text-align: center;

    }
   .ishidedyncls {

    background-color: #ECECEC;
    text-align: center; 

 }

   hr.feed_hrline {
    margin-top: 30px;
}
img.card-img-feed {
    height: auto;
    width: 250px;
    margin: 0 auto;
    display: block;
    margin-bottom: 5px;
}

</style>      
@endsection
