@extends('layouts.default')
@section('title', 'BetaMatt | Home')
@section('content')

<div class="container-fluid">
	<div class="container my-4">
		<div class="post-error-message col-md-12">
				@if (session('error'))
				<p class="alert alert-danger" id="danger_timeout">
			      {{ session('error') }}
			    </p>
			    @endif
			    @if (session('success'))
			    <p class="alert alert-success" id="sucess_timeout">
			      {{ session('success') }}
			  </p>
			    @endif
	    </div>
		<p class="welcome"> Welcome to <span style="color: #000;">Beta Matt</span></p>
		<div class="row user-profile">
			<div class="col-md-12">
				<h1 style="color:#000; font-size: 25px; font-family: Montserrat; font-weight: 600;" >My Profile</h1>
			</div>
		</div>
	     <?php $user = Auth::user(); ?>
		<div class='user-profile-container'>

			<div class="inner-profile container">
				<form method="post" name="user_editFrm" id="user_editFrm_{{$user->id}}" action="{{ url('/user/updateprofile', $user->id) }}" enctype="multipart/form-data">
					@csrf
					<input type="hidden" class="useridDefault" id="useridDefault_{{$user->id}}" name="user_id" value="{{ $user->id }}">
					<div class="input-group form-group">
						<label> First name
							<input type="text" name="user_firstname" class="form-control sign" id="userfirstDefault_{{$user->id}}" placeholder="Enter your First Name" value="{{ $user->firstname }}">
						</label>
					</div>
					<div class="input-group form-group">
						<label> Last Name
							<input type="text" name="user_laststname" class="form-control sign"  id="userlastDefault_{{$user->id}}" placeholder="Enter your Last Name" value="{{ $user->lastname }}">
						</label>
					</div>
					<div class="input-group form-group">
						<label> Email Address
							<input type="email" name="user_email" class="form-control sign" id="useremailDefault_{{$user->id}}" placeholder="Enter your Email Address" value="{{ $user->email }}" readonly>
						</label>
					</div>
					<div class="input-group form-group">
						<label> User Image
							<div class="user-img">
							<input id="user_img" name="user_img" id="userpicDefault_{{$user->id}}" type="file">
						    </div>
						</label>
					</div>
	                <div class="input-group form-group">
						<a class="profile_changepassword" href="{{url('/user/changepasssword', Auth::id())}}"><button type="button" class="btn changepassword_btn">CHANGE PASSWORD</button></a>
					</div>
	                    
					<div class="input-group form-group">
						<button type="submit" value="updateuserbtn" id="userprofiletBtnDefault_{{$user->id}}" class="btn  updateuser_btn">UPDATE</button>
					</div>
				</form>
		    </div>
	    </div>  
    </div>
</div>


 


@endsection