@extends('layouts.default')
@section('title', 'BetaMatt | Home')
@section('content')
<div class="container-fluid">
   <div class="container my-4">
      <div class="post-error-message col-md-12">
            @if (session('error'))
            <p class="alert alert-danger" id="danger_timeout">
               {{ session('error') }}
             </p>
             @endif
             @if (session('success'))
             <p class="alert alert-success" id="sucess_timeout">
               {{ session('success') }}
           </p>
             @endif
       </div>
      <p class="welcome"> Welcome to <span style="color: #000;">Beta Matt</span></p>
      <div class="row posts">
         <div class="col-md-2 rowposts_data">
            <h1 style="color:#000; font-size: 25px; font-family: Montserrat; font-weight: 600;" >My Posts</h1>
         </div>
         <div class="col-md-2 pop123" id="create123">
            <h4 class="icon"><a href="#" data-toggle="modal" data-target="#myModalCreate"><i class="fa fa-plus"></i></a></h4>
         </div>
      </div>


      <div class="modal fade" id="myModalCreate" tabindex="0" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" data-toggle="modal">
         <div class="modal-dialog">
            <div class="modal-content" >
               <form name="addpostFrm" id="addpostFrm" method="POST" action="{{url('/posts/addpost') }}" enctype="multipart/form-data" onsubmit="return validateAddForm()">
                  {{ csrf_field() }}
                  <input type="hidden" name="likeCreate" id="likeCreate" value="0">
                  <input type="hidden" name="prayCreate" id="prayCreate" value="0">
                  <div class="modal-body">
                     <select class="modal-select-privacy fa" id="privacyCreate" name="privacyCreate">
                        <option class="fa" value="0">
                           &#xf023 
                           <p style="font-family: Montserrat"> Only Me</p>
                        </option>
                        <option class="fa" value="1">
                           &#xf0ac 
                           <p style="font-family: Montserrat"> All</p>
                        </option>
                     </select>
                     <button type="button" class="close1" data-dismiss="modal"><span aria-hidden="false">&times;</span><span class="sr-only">Close</span></button>
                     <select class="modal-select fa" id="choiceCreate" name="choiceCreate">
                        <option value="text" class="fa">
                           &#xf15c 
                           <p style="font-family: Montserrat">Text</p>
                        </option>
                        <option value="image" class="fa">
                           &#xf03e  
                           <p style="font-family: Montserrat">Image</p>
                        </option>
                        <option value="video" class="fa">
                           &#xf1c8 
                           <p style="font-family: Montserrat"> Video</p>
                        </option>
                     </select>
                     <hr>
                     <div class="card-preview">
                        <div class="text" id="text-box-create">
                           <textarea name="messageCreate" id="messageCreate" rows="10" cols="30" onfocusout="messageCreateFunction()"></textarea>
                        </div>
                        <div class="image" id="image-box-create" style="display: none;">
                           <label class="radio-inline"><input type="radio" class="imgvalCreate" name="imgchoiceCreate" id="imgchoiceCreate0" value="0" checked>Upload Image</label>
                           <label class="radio-inline"><input type="radio" class="imgvalCreate" name="imgchoiceCreate" id="imgchoiceCreate1" value="1">Save From Site</label>
                           <div id="localImgCreate">
                              <input type="file" name="postpicCreate" id="postpicCreate" accept="image/*" onchange="postpicCreateFunction()">
                           </div>
                        <div class="save-btn" id="liveImgurlCreate" style="display: none;">
                              <div class="text-box-url-img-create" id="text-box-url-img-create">
                                 <input type="text" name="postpicurlCreate" id="postpicurlCreate" value="" placeholder="Enter website" onfocusout="postpicurlCreateFunction()">
                                 <button type="button" id="liveImgPrvBtn"><i class="fa fa-chevron-right arrow" aria-hidden="true"></i>
                                 </button>
                              </div>
                           </div>
                           <div id="liveImpPrv"></div>
                        </div>

                        <div class="video" id="video-box-create" style="display: none;">
                           <label class="radio-inline"><input type="radio" class="vdovalCreate" name="vdochoiceCreate" id="vdochoiceCreate0" value="0" checked>Upload Video</label>
                           <label class="radio-inline"><input type="radio" class="vdovalCreate" name="vdochoiceCreate" id="vdochoiceCreate1" value="1">Save From Site</label>
                           <div id="localVdoCreate">
                              <input type="file" name="postvideoCreate" id="postvideoCreate" accept="video/*" onchange="postvideoCreateFunction()">
                           </div>
                           <div class="save-btn-video" id="liveVdourlCreate" style="display: none;">
                           <div class="text-box-url-create" id="text-box-url-video-create" >
                                 <input type="text" name="postvideourlCreate" id="postvideourlCreate" value=""  placeholder="Enter website" onfocusout="postvideourlCreateFunction()">
                                 <button type="button" id="liveVdoPrvBtn"><i  class="fa fa-chevron-right arrow" aria-hidden="true"></i>
                                 </button>
                              </div>
                           </div>
                           <div id="liveVdoPrv"></div>
                        </div>
                     </div>
                  </div>
                  <div class="modal-footer">
                     <div class="row">
                        <div class="col-md-4 footer-btn">
                           <div class="share" id="isLikeCreate">
                              <img src="{{ url('/public') }}/images/like.png" class="share-img" />
                              <p style="font-family: Montserrat;">Like</p>
                           </div>
                        </div>
                        <div class="col-md-4 footer-btn">
                           <div class="share" id="isPrayCreate">
                              <img src="{{ url('/public') }}/images/pray.png" class="share-img" />
                              <p style="font-family: Montserrat;">Pray</p>
                           </div>
                        </div>
                        <div class="col-md-4 footer-btn">
                           <button type="submit" name="addpostBtn" class="save">
                              <img src="{{ url('/public') }}/images/save.png" class="save-img"/>
                              <p style="font-family: Montserrat;">Save</p>
                           </button>
                        </div>
                     </div>
                  </div>
               </form>
            </div>
         </div>
      </div>


      <div id="multi-item-example" class="carousel slide carousel-multi-item" data-ride="carousel">
         <div class="row slider">
            <div class="col-md-12">
               <div class="carousel-inner" role="listbox">
                  <div class="carousel-item active">
                     <hr class="my-4">
                     <div class="row center-last ">
                        @if( count($posts) > 0 )
                        @foreach($posts as $post)     
                                  
                        <div class="col-xl-12 col-lg-3 col-md-4 col-sm-6 col-12" id="{{$post->id}}" data-toggle="modal" data-target="#myModalDefault_{{$post->id}}">

                           <div class="card mb-2">
                              <!-- <img class="card-img-top" src="{{ $post->post_description }}" alt="postimage"> -->
                             @if($post->post_type=='image')
                              <div class="card-body" style="background: url({{ $post->post_description }});background-repeat:no-repeat;background-size: contain;background-position: 50%;"></div>
                                 @elseif($post->post_type=='video')
                                 @if($post->post_media_choice=='0')   
                                 <video class="card-img-top video" controls>
                                    <source src="{{ $post->post_description }}" type="video/mp4">
                                 </video>
                                 @else
                                 <?php
                                    $videoLiveUrl = explode("=", $post->post_description);
                                    if(!empty($videoLiveUrl[1])) { ?>
                                    <img class="card-img-top video"src="https://img.youtube.com/vi/<?php echo $videoLiveUrl[1]; ?>/0.jpg">
                                 <?php } ?>
                                 @endif
                                 @else
                                 <?php
                                  $post_data = ($post->post_description);                                
                                  if(strlen($post_data) > 305){
                                  $stringCut = substr($post_data,0, 305,)."...."; ?>                                                    
                                   <p class="card-text">{{ $stringCut }}</p><?php                              
                                  }else {  ?>                                                                   
                                   <p class="card-text">{{ $post_data }}</p>  
                                 <?php } ?>
                                 @endif
                           </div>
                        </div>
                        @endforeach
                        @endif
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- modal section -->
      @if( count($posts) > 0 )
      @foreach($posts as $post)

      <div class="modal fade" id="myModalDefault_{{$post->id}}" tabindex="0" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" data-toggle="modal">

         <div class="modal-dialog">
            <div class="modal-content" >

               <form class="defaulteditpostFrm" name="defaulteditpostFrm" id="defaulteditpostFrm_{{$post->id}}" method="POST" action="{{url('/posts/editdefaultpost') }}" enctype="multipart/form-data" onsubmit="return validateDefaultForm(<?php echo $post->id;?>)">

                  {{ csrf_field() }}

                  <input type="hidden" class="postidDefault" name="postidDefault" id="postidDefault_{{$post->id}}" value="{{$post->id}}">

                  <input type="hidden" class="likeDefault" name="likeDefault" id="likeDefault_{{$post->id}}" value="{{$post->is_like_post}}">

                  <input type="hidden" class="prayDefault" name="prayDefault" id="prayDefault_{{$post->id}}" value="{{$post->is_pray_post}}">

                  <div class="modal-body">
                     
                     <select class="modal-select-privacy fa privacyDefault" name="privacyDefault" id="privacyDefault_{{$post->id}}">

                        <option class="fa" value="0" <?php if($post->post_privacy=='0') { echo 'selected'; }?>>
                           &#xf023 
                           <p style="font-family: Montserrat"> Only Me</p>
                        </option>
                        <option class="fa" value="1" <?php if($post->post_privacy=='1') { echo 'selected'; }?>>
                           &#xf0ac 
                           <p style="font-family: Montserrat"> All</p>
                        </option>
                     </select>

                     <button type="button" class="close1" data-dismiss="modal"><span aria-hidden="false">&times;</span><span class="sr-only">Close</span></button>

                     <select class="modal-select fa choiceDefault" name="choiceDefault" id="choiceDefault_{{$post->id}}">

                        <option class="fa" value="text" <?php if($post->post_type=='text') { echo 'selected'; }?>>
                           &#xf15c 
                           <p style="font-family: Montserrat">Text</p>
                        </option>
                        <option class="fa" value="image" <?php if($post->post_type=='image') { echo 'selected'; }?>>
                           &#xf03e  
                           <p style="font-family: Montserrat">Image</p>
                        </option>
                        <option class="fa" value="video" <?php if($post->post_type=='video') { echo 'selected'; }?>>
                           &#xf1c8 
                           <p style="font-family: Montserrat"> Video</p>
                        </option>
                     </select>

                     <hr>
                     <div class="card-preview">

                        <div class="text" id="text-box-default_{{$post->id}}" <?php if($post->post_type=='text') { ?> style="display: block;" <?php } else { ?> style="display: none;" <?php }?>>
                           <?php if($post->post_type=='text') { ?>
                           <textarea  class="messageDefault" name="messageDefault" id="messageDefault_{{$post->id}}" rows="10" cols="30" onfocusout="messageDefaultFunction(<?php echo $post->id;?>)"><?php echo  $string = trim(preg_replace('/\s\s+/', ' ', $post->post_description));?>  </textarea>
                        <?php } else { ?>
                           <textarea class="messageDefault" name="messageDefault" id="messageDefault_{{$post->id}}" rows="10" cols="30" onfocusout="messageDefaultFunction(<?php echo $post->id;?>)"></textarea>
                        <?php } ?>
                        </div>

                 

                        <div class="image" id="image-box-default_{{$post->id}}" <?php if($post->post_type=='image') { ?> style="display: block;" <?php } else { ?> style="display: none;" <?php }?>>
                           
                           <label class="radio-inline"><input type="radio" class="imgvalDefault" name="imgchoiceDefault" id="imgchoiceDefault0_{{$post->id}}" value="0" <?php if($post->post_media_choice=='0') { echo 'checked'; }?>>Upload Image</label>

                           <label class="radio-inline"><input type="radio" class="imgvalDefault" name="imgchoiceDefault" id="imgchoiceDefault1_{{$post->id}}" value="1" <?php if($post->post_media_choice=='1') { echo 'checked'; }?>>Save From Site</label>

                           <div class="localImgDefault" id="localImgDefault_{{$post->id}}" <?php if($post->post_media_choice=='0') { ?> style="display: block;" <?php } else { ?> style="display: none;" <?php }?>>

                              <input type="file" class="postpicDefault" name="postpicDefault" id="postpicDefault_{{$post->id}}" accept="image/*" onchange="postpicDefaultFunction(<?php echo $post->id;?>)">

                              <input type="hidden" name="postpicDefaultHd" id="postpicDefaultHd_{{$post->id}}" value="{{$post->post_description}}">

                           </div>

                           <div class="save-btn liveImgurlDefault" id="liveImgurlDefault_{{$post->id}}" <?php if($post->post_media_choice=='1') { ?> style="display: block;" <?php } else { ?> style="display: none;" <?php }?>>

                              <div class="text-box-url-img-default" id="text-box-url-img-default_{{$post->id}}">
                                 <?php if($post->post_type=='image' && $post->post_media_choice=='1'){ ?>
                                 <input type="text" class="postpicurlDefault" name="postpicurlDefault" id="postpicurlDefault_{{$post->id}}" placeholder="Enter website" Value="{{$post->post_description}}" onfocusout="postpicurlDefaultFunction(<?php echo $post->id;?>)">
                                  <?php } else { ?>
                                     <input type="text" class="postpicurlDefault" name="postpicurlDefault" id="postpicurlDefault_{{$post->id}}" placeholder="Enter website" onfocusout="postpicurlDefaultFunction(<?php echo $post->id;?>)">
                                  <?php } ?>
                                 <button type="button" class="liveImgPrvBtnDefault" id="{{$post->id}}"><i class="fa fa-chevron-right arrow" aria-hidden="true"></i>
                                 </button>                               
                              </div>
                           </div>

                           <div class="liveImpPrvDefault" id="liveImpPrvDefault_{{$post->id}}">
                              <?php if($post->post_type=='image'){ ?>
                              <img class="card-img-top-one" src="{{ $post->post_description }}">
                           <?php } else { ?>

                           <?php } ?>
                           </div>

                        </div>


                        <div class="video" id="video-box-default_{{$post->id}}" <?php if($post->post_type=='video') { ?> style="display: block;" <?php } else { ?> style="display: none;" <?php }?>>

                           <label class="radio-inline"><input type="radio" class="vdovalDefault" name="vdochoiceDefault" id="vdochoiceDefault0_{{$post->id}}" value="0" <?php if($post->post_media_choice=='0') { echo 'checked'; }?>>Upload Video</label>

                           <label class="radio-inline"><input type="radio" class="vdovalDefault" name="vdochoiceDefault" id="vdochoiceDefault1_{{$post->id}}" value="1" <?php if($post->post_media_choice=='1') { echo 'checked'; }?>>Save From Site</label>

                           <div class="localVdoDefault" id="localVdoDefault_{{$post->id}}" <?php if($post->post_media_choice=='0') { ?> style="display: block;" <?php } else { ?> style="display: none;" <?php }?>>

                              <input type="file" class="postvideoDefault" name="postvideoDefault" id="postvideoDefault_{{$post->id}}" accept="video/*" onchange="postvideoDefaultFunction(<?php echo $post->id;?>)">

                              <input type="hidden" name="postvideoDefaultHd" id="postvideoDefaultHd_{{$post->id}}" value="{{$post->post_description}}">

                           </div>

                           <div class="save-btn-video liveVdourlDefault" id="liveVdourlDefault_{{$post->id}}" <?php if($post->post_media_choice=='1') { ?> style="display: block;" <?php } else { ?> style="display: none;" <?php }?>>


                              <div class="text-box-url-default" id="text-box-url-video-default_{{$post->id}}">

                                 <?php if($post->post_type=='video' && $post->post_media_choice=='1'){ ?>

                                  <input type="text" class="postvideourlDefault" name="postvideourlDefault" id="postvideourlDefault_{{$post->id}}" value="{{$post->post_description}}" placeholder="Enter website" onfocusout="postvideourlDefaultFunction(<?php echo $post->id;?>)">

                                  <?php } else { ?>
                                   <input type="text" class="postvideourlDefault" name="postvideourlDefault" id="postvideourlDefault_{{$post->id}}" value="{{$post->post_description}}" placeholder="Enter website" onfocusout="postvideourlDefaultFunction(<?php echo $post->id;?>)">
                                  <?php } ?> 

                                 <button type="button" class="liveVdoPrvBtnDefault" id="liveVdoPrvBtnDefault_{{$post->id}}"><i class="fa fa-chevron-right arrow" aria-hidden="true"></i>
                                 </button>
                                 
                              </div>

                           </div>

                           <div class="liveVdoPrvDefault" id="liveVdoPrvDefault_{{$post->id}}">
                              @if($post->post_type=='video')
                                 @if($post->post_media_choice=='0')   
                                    <video class="card-img-top video video_popup" controls>
                                       <source src="{{ $post->post_description }}" type="video/mp4">
                                    </video>
                                 @else
                                    <?php
                                       $videoLiveUrl = explode("=", $post->post_description);
                                       if(!empty($videoLiveUrl[1])) { ?>
                                    <iframe class="card-img-top video" src="https://www.youtube.com/embed/<?php echo $videoLiveUrl[1]; ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> 
                                    <?php } ?>
                                 @endif
                              @else
                              @endif

                           </div>
                        </div>


                     </div>
                  </div>
                  <div class="modal-footer">
                     <div class="row">
                     
                        <div class="col-md-3 footer-btn">                         
                           <div class="share isLikeDefault <?php if($post->is_like_post=='1') { echo 'islikedyncls'; }?>" id="isLikeDefault_{{$post->id}}">
                              <img src="{{ url('/public') }}/images/like.png" class="share-img" />
                              <?php
                                 $posts_like_count = DB::table('posts')->where('id', $post->id)->where('is_like_post', '1')->count();
                                 $feed_like_count = DB::table('feedposts')->where('post_id', $post->id)->where('feed_like', '1')->count();
                                 $total_like_count = $posts_like_count + $feed_like_count;  
                              ?>
                              <p style="font-family: Montserrat;">Like</p><span>{{$total_like_count}}</span>
                           </div>
                        </div>
                        <div class="col-md-3 footer-btn">
                           <div class="share isPrayDefault <?php if($post->is_pray_post=='1') { echo 'ispraydyncls'; }?>" id="isPrayDefault_{{$post->id}}">
                              <img src="{{ url('/public') }}/images/pray.png" class="share-img" />
                              <?php
                                 $posts_praylike_count = DB::table('posts')->where('id', $post->id)->where('is_pray_post', '1')->count();
                                 $feed_praylike_count = DB::table('feedposts')->where('post_id', $post->id)->where('feed_pray', '1')->count();
                                 $total_praylike_count = $posts_praylike_count + $feed_praylike_count;  
                              ?>
                              <p style="font-family: Montserrat;">Pray</p><span>{{$total_praylike_count}}</span>
                           </div>
                        </div>
                        <div class="col-md-3 footer-btn">
                           <a href="{{url('/posts/deletepost', $post->id)}}" onclick="return DefaultDelFunction();" class="delete isDelDefault" id="isDelDefault_{{$post->id}}">
                              <img src="{{ url('/public') }}/images/delete.png" class="del-img"/>
                              <p style="font-family: Montserrat;">Delete</p>
                           </a>

                        </div>
                        <div class="col-md-3 footer-btn">
                           <button type="submit" name="editpostBtnDefault" id="editpostBtnDefault_{{$post->id}}" class="save">
                              <img src="{{ url('/public') }}/images/save.png" class="save-img"/>
                              <p style="font-family: Montserrat;">Save</p>
                           </button>
                        </div>
                     </div>
                  </div>
               </form>
            </div>
         </div>
      </div>
      @endforeach
      @endif
     
      <!-- modal section end -->
      <!-- first section end -->
      <!-- second section -->
      <h1 style="color:#000; font-size: 25px; font-family: Montserrat; font-weight: 600;     margin-top: 40px;" >Likes</h1>
      <!--Carousel Wrapper-->
      <div id="multi-item-example1" class="carousel slide carousel-multi-item" data-ride="carousel">
         <!--Slides-->
         <div class="carousel-inner" role="listbox">
            <!--First slide-->
            <div class="carousel-item active">
               <hr class="my-4">
               <div class="row center-last"> 
                        <!-- 10 -feb_19  START-->    
                           <?php
                          
                           if(!empty($like_fetch)) {
                             foreach ($like_fetch as $like_fetchdata ) { 
                          
                                 $fetchlike_data = $like_fetchdata->post_id;
                              
                                 $fetch_like =   DB::table('posts')->where('id',$fetchlike_data)->where('is_active', '1')->where('is_deleted', '0')->orderBy('id','DESC')->get(); 
                                    foreach($fetch_like as $fetch_likedata) { ?>
                                    <div class="col-xl-12 col-lg-3 col-md-4 col-sm-6 col-12" id="second-{{$fetch_likedata->id}}" data-toggle="modal" data-target="#myModalLike_{{$fetch_likedata->id}}">
                                       <div class="card mb-2">
                                             @if($fetch_likedata->post_type=='image')
                                             <div class="card-body" style="background: url({{ $fetch_likedata->post_description }});background-repeat:no-repeat;background-size: contain;background-position: 50%;"></div>
                                             <!-- <img class="card-img-top" src="{{ $fetch_likedata->post_description }}" alt="postimage"> -->
                                             @elseif($fetch_likedata->post_type=='video')
                                             @if($fetch_likedata->post_media_choice=='0')   
                                             <video class="card-img-top video" controls>
                                                <source src="{{ $fetch_likedata->post_description }}" type="video/mp4">
                                             </video>
                                             @else
                                             <?php
                                                $videoLiveUrl = explode("=", $fetch_likedata->post_description);
                                                if(!empty($videoLiveUrl[1])) { ?>
                                             <img class="card-img-top video"src="https://img.youtube.com/vi/<?php echo $videoLiveUrl[1]; ?>/0.jpg">
                                             <?php } ?>
                                             @endif
                                             @else
                                                <?php
                                                 $post_data = ($fetch_likedata->post_description);                                
                                                 if(strlen($post_data) > 310){
                                                 $stringCut = substr($post_data,0, 310,)."...."; ?>                                                    
                                                  <p class="card-text">{{ $stringCut }}</p><?php                              
                                                 }else {  ?>                                                                   
                                                  <p class="card-text">{{ $post_data }}</p>  
                                                <?php } ?>                         
                                                @endif
                                       </div>  
                                    </div><?php
                                    }
                                 }
                              }
                            
                           ?>  
                   <!-- 10 -feb_19  END-->                      
                  @if( count($likeposts) > 0 )
                  @foreach($likeposts as $likepost)             
                  <div class="col-xl-12 col-lg-3 col-md-4 col-sm-6 col-12" id="second-{{$likepost->id}}" data-toggle="modal" data-target="#myModalLike_{{$likepost->id}}">
                     <div class="card mb-2">                  
                           @if($likepost->post_type=='image')
                           <!-- <img class="card-img-top" src="{{ $likepost->post_description }}" alt="postimage"> -->
                           <div class="card-body" style="background: url({{ $likepost->post_description }});background-repeat:no-repeat;background-size: contain;background-position: 50%;"></div>
                           @elseif($likepost->post_type=='video')
                           @if($likepost->post_media_choice=='0')   
                           <video class="card-img-top video" controls>
                              <source src="{{ $likepost->post_description }}" type="video/mp4">
                           </video>
                           @else
                           <?php
                              $videoLiveUrl = explode("=", $likepost->post_description);
                              if(!empty($videoLiveUrl[1])) { ?>
                           <img class="card-img-top video"src="https://img.youtube.com/vi/<?php echo $videoLiveUrl[1]; ?>/0.jpg">
                           <?php } ?>
                           @endif
                           @else
                              <?php
                               $post_data = ($likepost->post_description);                                
                               if(strlen($post_data) > 310){
                               $stringCut = substr($post_data,0, 310,)."...."; ?>                                                    
                                <p class="card-text">{{ $stringCut }}</p><?php                              
                               }else {  ?>                                                                   
                                <p class="card-text">{{ $post_data }}</p>  
                              <?php } ?>                         
                           @endif
                     </div>
                  </div>

                  @endforeach
                  @endif
               </div>
            </div>
         </div>
      </div>

      <!-- 10 -feb_19 start -->
     
    <?php                        
      if(!empty($like_fetch)) {
         foreach ($like_fetch as $like_fetchdata ) {      
            $fetchlike_data = $like_fetchdata->post_id; 

            $fetch_like =   DB::table('posts')->where('id',$fetchlike_data)->where('is_active', '1')->where('is_deleted', '0')->orderBy('id','DESC')->get();  
            foreach($fetch_like as $fetch_likedata) { 

               ?>
            <div class="modal fade" id="myModalLike_{{$fetch_likedata->id}}" tabindex="0" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false"  data-toggle="modal">
               <div class="modal-dialog">
                  
                  <div class="modal-content">
                   {{ csrf_field() }}

                    <input type="hidden" class="feedidDefault" name="feedidDefault" id="postidDefault_{{$like_fetchdata->post_id}}" value="{{$like_fetchdata->post_id}}">

                    <input type="hidden" class="feedlikeDefault" name="feedlikeDefault" id="postuseridDefault_{{$like_fetchdata->user_id}}" value="{{$like_fetchdata->user_id}}">

                    <!-- <input type="hidden" class="hideDefault" name="hideDefault" id="hideDefault_{{$like_fetchdata->feed_hide}}" value="1">  -->


                     <div class="modal-body">
                         <p style="float: left; font-family: Montserrat;">{{$fetch_likedata->post_type}}</p>
                       <button type="button" class="close_Data" data-dismiss="modal"><span aria-hidden="false">&times;</span><span class="sr-only">Close</span></button>
                        <hr class="home_hrline">                                                
                        <div class="card-preview">
                          @if($fetch_likedata->post_type=='image')
                           <img class="card-img-like_privatehide" src="{{ $fetch_likedata->post_description }}" alt="postimage">
                           @elseif($fetch_likedata->post_type=='video')
                           @if($fetch_likedata->post_media_choice=='0')
                           <video class="card-img-top video" controls>
                              <source src="{{ $fetch_likedata->post_description }}" type="video/mp4">
                           </video>
                           @else
                             <?php
                              $videoLiveUrl = explode("=", $fetch_likedata->post_description);
                              if(!empty($videoLiveUrl[1])) { ?>
                           <iframe class="card-img-top video" src="https://www.youtube.com/embed/<?php echo $videoLiveUrl[1]; ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> 
                           <?php } ?>
                           @endif
                           @else
                           <p class="text-preview">{{ $fetch_likedata->post_description }}</p>
                           @endif
                         </div>
                      </div>
                         <div class="modal-footer">
                           <div class="row justify-content-center">
                              <div class="col-md-3 footer-btn">
                                 <button type="submit" name="hidefeedBtnDefault" id="addpostfeedBtnDefault_{{$like_fetchdata->user_id}}" post_id ="{{$like_fetchdata->post_id}}"  user_id = "{{$like_fetchdata->user_id}}" class="save_hide_like">
                                     <img src="{{ url('/public') }}/images/hide.png" class="share-img" />
                                    <p style="font-family: Montserrat;">Hide</p>
                                 </button>
                              </div>
                           </div>
                        </div>                 
                   </div>
                </div>
            </div><?php
         }
      }
   }
      ?>
   <!-- 10 -feb_19  END-->

      <!--/.Carousel Wrapper-->
      <!-- modal section -->
      @if( count($likeposts) > 0 )
      @foreach($likeposts as $likepost)

      <div class="modal fade" id="myModalLike_{{$likepost->id}}" tabindex="0" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" data-toggle="modal">

         <div class="modal-dialog">
            <div class="modal-content" >

               <form class="likeeditpostFrm" name="likeeditpostFrm" id="likeeditpostFrm_{{$likepost->id}}" method="POST" action="{{url('/posts/editlikepost') }}" enctype="multipart/form-data" onsubmit="return validateLikeForm(<?php echo $likepost->id;?>)">

                  {{ csrf_field() }}

                  <input type="hidden" class="postidLike" name="postidLike" id="postidLike_{{$likepost->id}}" value="{{$likepost->id}}">

                  <input type="hidden" class="likeLike" name="likeLike" id="likeLike_{{$likepost->id}}" value="{{$likepost->is_like_post}}">

                  <input type="hidden" class="prayLike" name="prayLike" id="prayLike_{{$likepost->id}}" value="{{$likepost->is_pray_post}}">

                  <div class="modal-body">
                     
                     <select class="modal-select-privacy fa privacyLike" name="privacyLike" id="privacyLike_{{$likepost->id}}">

                        <option class="fa" value="0" <?php if($likepost->post_privacy=='0') { echo 'selected'; }?>>
                           &#xf023 
                           <p style="font-family: Montserrat"> Only Me</p>
                        </option>
                        <option class="fa" value="1" <?php if($likepost->post_privacy=='1') { echo 'selected'; }?>>
                           &#xf0ac 
                           <p style="font-family: Montserrat"> All</p>
                        </option>
                     </select>

                     <button type="button" class="close1" data-dismiss="modal"><span aria-hidden="false">&times;</span><span class="sr-only">Close</span></button>

                     <select class="modal-select fa choiceLike" name="choiceLike" id="choiceLike_{{$likepost->id}}">

                        <option class="fa" value="text" <?php if($likepost->post_type=='text') { echo 'selected'; }?>>
                           &#xf15c 
                           <p style="font-family: Montserrat">Text</p>
                        </option>
                        <option class="fa" value="image" <?php if($likepost->post_type=='image') { echo 'selected'; }?>>
                           &#xf03e  
                           <p style="font-family: Montserrat">Image</p>
                        </option>
                        <option class="fa" value="video" <?php if($likepost->post_type=='video') { echo 'selected'; }?>>
                           &#xf1c8 
                           <p style="font-family: Montserrat"> Video</p>
                        </option>
                     </select>

                     <hr>
                     <div class="card-preview">

                        <div class="text" id="text-box-like_{{$likepost->id}}" <?php if($likepost->post_type=='text') { ?> style="display: block;" <?php } else { ?> style="display: none;" <?php }?>>

                           @if($likepost->post_type=='text')  

                              <textarea class="messageLike" name="messageLike" id="messageLike_{{$likepost->id}}" rows="10" cols="30" onfocusout="messageLikeFunction(<?php echo $likepost->id;?>)"><?php echo  $string = trim(preg_replace('/\s\s+/', ' ', $likepost->post_description));?></textarea>
                               
                               @else


                              <textarea class="messageLike" name="messageLike" id="messageLike_{{$likepost->id}}" rows="10" cols="30" onfocusout="messageLikeFunction(<?php echo $likepost->id;?>)"></textarea>
                           @endif  
                        </div>

                        <div class="image" id="image-box-like_{{$likepost->id}}" <?php if($likepost->post_type=='image') { ?> style="display: block;" <?php } else { ?> style="display: none;" <?php }?>>
                           
                           <label class="radio-inline"><input type="radio" class="imgvalLike" name="imgchoiceLike" id="imgchoiceLike0_{{$likepost->id}}" value="0" <?php if($likepost->post_media_choice=='0') { echo 'checked'; }?>>Upload Image</label>

                           <label class="radio-inline"><input type="radio" class="imgvalLike" name="imgchoiceLike" id="imgchoiceLike1_{{$likepost->id}}" value="1" <?php if($likepost->post_media_choice=='1') { echo 'checked'; }?>>Save From Site</label>

                           <div class="localImgLike" id="localImgLike_{{$likepost->id}}" <?php if($likepost->post_media_choice=='0') { ?> style="display: block;" <?php } else { ?> style="display: none;" <?php }?>>

                              <input type="file" class="postpicLike" name="postpicLike" id="postpicLike_{{$likepost->id}}" accept="image/*" onchange="postpicLikeFunction(<?php echo $likepost->id;?>)">

                              <input type="hidden" name="postpicLikeHd" id="postpicLikeHd_{{$likepost->id}}" value="{{$likepost->post_description}}">

                           </div>

                           <div class="save-btn liveImgurlLike" id="liveImgurlLike_{{$likepost->id}}" <?php if($likepost->post_media_choice=='1') { ?> style="display: block;" <?php } else { ?> style="display: none;" <?php }?>>

                              <div class="text-box-url-img-like" id="text-box-url-img-like_{{$likepost->id}}">

                                 @if($likepost->post_type=='image' && $likepost->post_media_choice=='1')
                                    <input type="text" class="postpicurlLike" value="{{ $likepost->post_description }}" name="postpicurlLike" id="postpicurlLike_{{$likepost->id}}" placeholder="Enter website" onfocusout="postpicurlLikeFunction(<?php echo $likepost->id;?>)">
                                  @else

                                    <input type="text" class="postpicurlLike" name="postpicurlLike" id="postpicurlLike_{{$likepost->id}}" placeholder="Enter website" onfocusout="postpicurlLikeFunction(<?php echo $likepost->id;?>)">
                                 @endif
                                 <button type="button" class="liveImgPrvBtnLike" id="{{$likepost->id}}"><i class="fa fa-chevron-right arrow" aria-hidden="true"></i>
                                 </button>
                                
                              </div>
                           </div>
                           @if($likepost->post_type=='image')
                           <div class="liveImpPrvLike" id="liveImpPrvLike_{{$likepost->id}}">
                              <img class="card-img-top-one" src="{{ $likepost->post_description }}">
                           </div>
                           @else
                           @endif
                        </div>

                        <div class="video" id="video-box-like_{{$likepost->id}}" <?php if($likepost->post_type=='video') { ?> style="display: block;" <?php } else { ?> style="display: none;" <?php }?>>

                           <label class="radio-inline"><input type="radio" class="vdovalLike" name="vdochoiceLike" id="vdochoiceLike0_{{$likepost->id}}" value="0" <?php if($likepost->post_media_choice=='0') { echo 'checked'; }?>>Upload Video</label>

                           <label class="radio-inline"><input type="radio" class="vdovalLike" name="vdochoiceLike" id="vdochoiceLike1_{{$likepost->id}}" value="1" <?php if($likepost->post_media_choice=='1') { echo 'checked'; }?>>Save From Site</label>

                           <div class="localVdoLike" id="localVdoLike_{{$likepost->id}}" <?php if($likepost->post_media_choice=='0') { ?> style="display: block;" <?php } else { ?> style="display: none;" <?php }?>>

                              <input type="file" class="postvideoLike" name="postvideoLike" id="postvideoLike_{{$likepost->id}}" accept="video/*" onchange="postvideoLikeFunction(<?php echo $likepost->id;?>)">

                              <input type="hidden" name="postvideoLikeHd" id="postvideoLikeHd_{{$likepost->id}}" value="{{$likepost->post_description}}">

                           </div>

                           <div class="save-btn-video liveVdourlLike" id="liveVdourlLike_{{$likepost->id}}" <?php if($likepost->post_media_choice=='1') { ?> style="display: block;" <?php } else { ?> style="display: none;" <?php }?>>

                              <div class="text-box-url-like" id="text-box-url-video-like_{{$likepost->id}}">

                                  @if($likepost->post_type=='video' && $likepost->post_media_choice=='1')
                                    <input type="text" class="postvideourlLike" name="postvideourlLike" id="postvideourlLike_{{$likepost->id}}" value="{{$likepost->post_description}}" placeholder="Enter website" onfocusout="postvideourlLikeFunction(<?php echo $likepost->id;?>)">
                                    @else
                                    <input type="text" class="postvideourlLike" name="postvideourlLike" id="postvideourlLike_{{$likepost->id}}" placeholder="Enter website" onfocusout="postvideourlLikeFunction(<?php echo $likepost->id;?>)">
                                 @endif
                                 <button type="button" class="liveVdoPrvBtnLike" id="liveVdoPrvBtnLike_{{$likepost->id}}"><i class="fa fa-chevron-right arrow" aria-hidden="true"></i>
                                 </button>
                                 
                              </div>

                           </div>

                           <div class="liveVdoPrvLike" id="liveVdoPrvLike_{{$likepost->id}}">
                              @if($likepost->post_type=='video')
                              @if($likepost->post_media_choice=='0')   
                                 <video class="card-img-top video" controls>
                                    <source src="{{ $likepost->post_description }}" type="video/mp4">
                                 </video>
                                 @else
                                 <?php
                                    $videoLiveUrl = explode("=", $likepost->post_description);
                                    if(!empty($videoLiveUrl[1])) { ?>
                                 <iframe class="card-img-top video" src="https://www.youtube.com/embed/<?php echo $videoLiveUrl[1]; ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> 
                                 <?php } ?>
                                 @endif
                                 @else
                              @endif
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="modal-footer">
                     <div class="row">
                        <div class="col-md-3 footer-btn">
                           <div class="share isLikeLike <?php if($likepost->is_like_post=='1') { echo 'islikedyncls'; }?>" id="isLikeLike_{{$likepost->id}}">
                              <img src="{{ url('/public') }}/images/like.png" class="share-img" />
                              <?php  

                              $total_likecount = DB::table('posts')->where('id',$likepost->id)->where('is_like_post', '1')->count();
                              $total_feedlikecount = DB::table('feedposts')->where('post_id',$likepost->id)->where('feed_like', '1')->count();
                              $likepost_total = $total_likecount + $total_feedlikecount;  
                                ?>
                              <p style="font-family: Montserrat;">Like</p><span>{{$likepost_total}}</span>
                           </div>
                        </div>
                        <div class="col-md-3 footer-btn">
                           <div class="share isPrayLike <?php if($likepost->is_pray_post=='1') { echo 'ispraydyncls'; }?>" id="isPrayLike_{{$likepost->id}}">
                              <img src="{{ url('/public') }}/images/pray.png" class="share-img" />
                              <p style="font-family: Montserrat;">Pray</p>
                           </div>
                        </div>
                        <div class="col-md-3 footer-btn">
                           <a href="{{url('/posts/deletepost', $likepost->id)}}" onclick="return LikeDelFunction();" class="delete isDelLike" id="isDelLike_{{$likepost->id}}">
                              <img src="{{ url('/public') }}/images/delete.png" class="del-img"/>
                              <p style="font-family: Montserrat;">Delete</p>
                           </a>
                        </div>
                        <div class="col-md-3 footer-btn">
                           <button type="submit" name="editpostBtnLike" id="editpostBtnLike_{{$likepost->id}}" class="save">
                              <img src="{{ url('/public') }}/images/save.png" class="save-img"/>
                              <p style="font-family: Montserrat;">Save</p>
                           </button>
                        </div>
                     </div>
                  </div>
               </form>
            </div>
         </div>
      </div>

      @endforeach
      @endif

      <!-- modal section end -->
      <!-- secondsection end -->
      <!-- SECTION THIRD -->
      <h1 style="color:#000; font-size: 25px; font-family: Montserrat; font-weight: 600;margin-top: 40px;" >Prayers</h1>
      <!--Carousel Wrapper-->
      <div id="multi-item-example3" class="carousel slide carousel-multi-item" data-ride="carousel">
         <!--Slides-->
         <div class="carousel-inner" role="listbox">
            <!--First slide-->
            <div class="carousel-item active">
               <hr class="my-4">
               <div class="row center-last">
                <!-- 10 -feb_19  START-->
                  <?php
                  if(!empty($pray_fetch)) {
                     foreach ($pray_fetch as $praypostsdata ) { 
                           $prayfetch_data = $praypostsdata->post_id;
                           $prayfetch_like =   DB::table('posts')->where('id',$prayfetch_data)->where('is_active', '1')->where('is_deleted', '0')->orderBy('id','DESC')->get(); 
                                    foreach($prayfetch_like as $prayfetch_likedata) { ?>
                                  <div class="col-xl-12 col-lg-3 col-md-4 col-sm-6 col-12" id="third-{{$prayfetch_likedata->id}}" data-toggle="modal" data-target="#myModalPray_{{$prayfetch_likedata->id}}">
                                    <div class="card mb-2">
                                          @if($prayfetch_likedata->post_type=='image')
                                         <!--  <img class="card-img-top" src="{{ $prayfetch_likedata->post_description }}" alt="postimage"> -->
                                         <div class="card-body" style="background: url({{ $prayfetch_likedata->post_description }});background-repeat:no-repeat;background-size: contain;background-position: 50%;"></div>
                                          @elseif($prayfetch_likedata->post_type=='video')
                                          @if($prayfetch_likedata->post_media_choice=='0')   
                                          <video class="card-img-top video" controls>
                                             <source src="{{ $prayfetch_likedata->post_description }}" type="video/mp4">
                                          </video>
                                          @else
                                          <?php
                                             $videoLiveUrl = explode("=", $prayfetch_likedata->post_description);
                                             if(!empty($videoLiveUrl[1])) { ?>
                                          <img class="card-img-top video"src="https://img.youtube.com/vi/<?php echo $videoLiveUrl[1]; ?>/0.jpg">
                                          <?php } ?>
                                          @endif
                                          @else
                                             <?php
                                              $post_data = ($prayfetch_likedata->post_description);                                
                                              if(strlen($post_data) > 310){
                                              $stringCut = substr($post_data,0, 310,)."...."; ?>                                                    
                                               <p class="card-text">{{ $stringCut }}</p><?php                              
                                              }else {  ?>                                                                   
                                               <p class="card-text">{{ $post_data }}</p>  
                                             <?php } ?>                         
                                          @endif
                                 </div>  
                              </div><?php
                           }
                        }
                     }                          
                   ?>
                   <!-- 10 -feb_19  END-->                   
                  @if( count($prayposts) > 0 )
                  @foreach($prayposts as $praypost)
                  <div class="col-xl-12 col-lg-3 col-md-4 col-sm-6 col-12" id="third-{{$praypost->id}}" data-toggle="modal" data-target="#myModalPray_{{$praypost->id}}">
                     <div class="card mb-2">
                          <!--  <h4 class="card-title">Post {{ $post->id }}</h4> -->
                           @if($praypost->post_type=='image')
                          <!--  <img class="card-img-top" src="{{ $praypost->post_description }}" alt="postimage"> -->
                          <div class="card-body" style="background: url({{ $praypost->post_description }});background-repeat:no-repeat;background-size: contain;background-position: 50%;"></div>                        
                           @elseif($praypost->post_type=='video')
                           @if($praypost->post_media_choice=='0')   
                           <video class="card-img-top video" controls>
                              <source src="{{ $praypost->post_description }}" type="video/mp4">
                           </video>
                           @else
                           <?php
                              $videoLiveUrl = explode("=", $praypost->post_description);
                              if(!empty($videoLiveUrl[1])) { ?>
                          <img class="card-img-top video"src="https://img.youtube.com/vi/<?php echo $videoLiveUrl[1]; ?>/0.jpg">
                           <?php } ?>
                           @endif
                           @else
                              <?php
                               $post_data = ($praypost->post_description);                                
                               if(strlen($post_data) > 310){
                               $stringCut = substr($post_data,0, 310,)."...."; ?>                                                    
                                <p class="card-text">{{ $stringCut }}</p><?php                              
                               }else {  ?>                                                                   
                                <p class="card-text">{{ $post_data }}</p>  
                              <?php } ?>   
                           @endif
                     </div>
                  </div>
                  @endforeach
                  @endif
               </div>
            </div>
            <!--/.First slide-->
         </div>
         <!--/.Slides-->
      </div>
   </div>
   <!--/.Carousel Wrapper-->
   <!-- modal section -->

        <!-- 10 -feb_19 start -->  
    <?php                        
      if(!empty($pray_fetch)) {
         foreach ($pray_fetch as $praypostsdata ) {      
            $prayfetch_data = $praypostsdata->post_id;         
            $prayfetch_like =   DB::table('posts')->where('id',$prayfetch_data)->where('is_active', '1')->where('is_deleted', '0')->orderBy('id','DESC')->get();  
            foreach($prayfetch_like as $prayfetch_likedata) { ?>

             <div class="modal fade" id="myModalPray_{{$prayfetch_likedata->id}}" tabindex="0" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" data-toggle="modal">

               <div class="modal-dialog">
                  <div class="modal-content">
                     <div class="modal-body">

                          <input type="hidden" class="prayidDefault" name="prayidDefault" id="postidDefault_{{$praypostsdata->post_id}}" value="{{$praypostsdata->post_id}}">

                         <input type="hidden" class="praylikeDefault" name="praylikeDefault" id="postuseridDefault_{{$praypostsdata->user_id}}" value="{{$praypostsdata->user_id}}">

                        <p style="float: left; font-family: Montserrat;">{{$prayfetch_likedata->post_type}}</p>
                       <button type="button" class="close_Data" data-dismiss="modal"><span aria-hidden="false">&times;</span><span class="sr-only">Close</span></button>
                       <hr class="home_hrline">                                            
                        <div class="card-preview">
                          @if($prayfetch_likedata->post_type=='image')
                           <img class="card-img-like_privatehide " src="{{ $prayfetch_likedata->post_description }}" alt="postimage">
                           @elseif($prayfetch_likedata->post_type=='video')
                           @if($prayfetch_likedata->post_media_choice=='0')
                           <video class="card-img-top video" controls>
                              <source src="{{ $prayfetch_likedata->post_description }}" type="video/mp4">
                           </video>
                           @else
                             <?php
                              $videoLiveUrl = explode("=", $prayfetch_likedata->post_description);
                              if(!empty($videoLiveUrl[1])) { ?>
                           <iframe class="card-img-top video" src="https://www.youtube.com/embed/<?php echo $videoLiveUrl[1]; ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> 
                           <?php } ?>
                           @endif
                           @else
                           <p class="text-preview">{{ $prayfetch_likedata->post_description }}</p>
                           @endif
                         </div>
                      </div>
                          <div class="modal-footer">
                           <div class="row justify-content-center">
                              <div class="col-md-3 footer-btn">
                                 <button type="submit" name="hidefeedBtnDefault" id="addpostfeedBtnDefault_{{$praypostsdata->user_id}}" post_id ="{{$praypostsdata->post_id}}"  user_id = "{{$praypostsdata->user_id}}" class="save_hide_pray">
                                     <img src="{{ url('/public') }}/images/hide.png" class="share-img" />
                                    <p style="font-family: Montserrat;">Hide</p>
                                 </button>
                              </div>
                           </div>
                        </div>                  
                   </div>
                </div>
            </div><?php
         }
      }
   }
      ?>
   <!-- 10 -feb_19  END-->


   @if( count($prayposts) > 0 )
   @foreach($prayposts as $praypost)

   <div class="modal fade" id="myModalPray_{{$praypost->id}}" tabindex="0" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" data-toggle="modal">

         <div class="modal-dialog">
            <div class="modal-content" >

               <form class="prayeditpostFrm" name="prayeditpostFrm" id="prayeditpostFrm_{{$praypost->id}}" method="POST" action="{{url('/posts/editpraypost') }}" enctype="multipart/form-data" onsubmit="return validatePrayForm(<?php echo $praypost->id;?>)">

                  {{ csrf_field() }}

                  <input type="hidden" class="postidPray" name="postidPray" id="postidPray_{{$praypost->id}}" value="{{$praypost->id}}">

                  <input type="hidden" class="likePray" name="likePray" id="likePray_{{$praypost->id}}" value="{{$praypost->is_like_post}}">

                  <input type="hidden" class="prayPray" name="prayPray" id="prayPray_{{$praypost->id}}" value="{{$praypost->is_pray_post}}">

                  <div class="modal-body">
                     
                     <select class="modal-select-privacy fa privacyPray" name="privacyPray" id="privacyPray_{{$praypost->id}}">

                        <option class="fa" value="0" <?php if($praypost->post_privacy=='0') { echo 'selected'; }?>>
                           &#xf023 
                           <p style="font-family: Montserrat"> Only Me</p>
                        </option>
                        <option class="fa" value="1" <?php if($praypost->post_privacy=='1') { echo 'selected'; }?>>
                           &#xf0ac 
                           <p style="font-family: Montserrat"> All</p>
                        </option>
                     </select>

                     <button type="button" class="close1" data-dismiss="modal"><span aria-hidden="false">&times;</span><span class="sr-only">Close</span></button>

                     <select class="modal-select fa choicePray" name="choicePray" id="choicePray_{{$praypost->id}}">

                        <option class="fa" value="text" <?php if($praypost->post_type=='text') { echo 'selected'; }?>>
                           &#xf15c 
                           <p style="font-family: Montserrat">Text</p>
                        </option>
                        <option class="fa" value="image" <?php if($praypost->post_type=='image') { echo 'selected'; }?>>
                           &#xf03e  
                           <p style="font-family: Montserrat">Image</p>
                        </option>
                        <option class="fa" value="video" <?php if($praypost->post_type=='video') { echo 'selected'; }?>>
                           &#xf1c8 
                           <p style="font-family: Montserrat"> Video</p>
                        </option>
                     </select>

                     <hr>
                     <div class="card-preview">

                        <div class="text" id="text-box-pray_{{$praypost->id}}" <?php if($praypost->post_type=='text') { ?> style="display: block;" <?php } else { ?> style="display: none;" <?php }?>>

                        @if($praypost->post_type=='text')
                           <textarea class="messagePray" name="messagePray" id="messagePray_{{$praypost->id}}" rows="10" cols="30" onfocusout="messagePrayFunction(<?php echo $praypost->id;?>)"><?php echo  $string = trim(preg_replace('/\s\s+/', ' ', $praypost->post_description));?></textarea>
                         @else 
                           <textarea class="messagePray" name="messagePray" id="messagePray_{{$praypost->id}}" rows="10" cols="30" onfocusout="messagePrayFunction(<?php echo $praypost->id;?>)"></textarea>
                        @endif
                        </div>

                        <div class="image" id="image-box-pray_{{$praypost->id}}" <?php if($praypost->post_type=='image') { ?> style="display: block;" <?php } else { ?> style="display: none;" <?php }?>>
                           
                           <label class="radio-inline"><input type="radio" class="imgvalPray" name="imgchoicePray" id="imgchoicePray0_{{$praypost->id}}" value="0" <?php if($praypost->post_media_choice=='0') { echo 'checked'; }?>>Upload Image</label>

                           <label class="radio-inline"><input type="radio" class="imgvalPray" name="imgchoicePray" id="imgchoicePray1_{{$praypost->id}}" value="1" <?php if($praypost->post_media_choice=='1') { echo 'checked'; }?>>Save From Site</label>

                           <div class="localImgPray" id="localImgPray_{{$praypost->id}}" <?php if($praypost->post_media_choice=='0') { ?> style="display: block;" <?php } else { ?> style="display: none;" <?php }?>>

                              <input type="file" class="postpicPray" name="postpicPray" id="postpicPray_{{$praypost->id}}" accept="image/*" onchange="postpicPrayFunction(<?php echo $praypost->id;?>)">

                              <input type="hidden" name="postpicPrayHd" id="postpicPrayHd_{{$praypost->id}}" value="{{$praypost->post_description}}">

                           </div>

                           <div class="save-btn liveImgurlPray" id="liveImgurlPray_{{$praypost->id}}" <?php if($praypost->post_media_choice=='1') { ?> style="display: block;" <?php } else { ?> style="display: none;" <?php }?>>

                              <div class="text-box-url-img-pray" id="text-box-url-img-pray_{{$praypost->id}}">

                              @if( $praypost->post_type=='image'  && $praypost->post_media_choice=='1')
                                 <input type="text" class="postpicurlPray" name="postpicurlPray" value="{{ $praypost->post_description }}" id="postpicurlPray_{{$praypost->id}}" placeholder="Enter website" onfocusout="postpicurlPrayFunction(<?php echo $praypost->id;?>)">
                                 @else
                                 <input type="text" class="postpicurlPray" name="postpicurlPray" id="postpicurlPray_{{$praypost->id}}" placeholder="Enter website" onfocusout="postpicurlPrayFunction(<?php echo $praypost->id;?>)">
                              @endif

                                 <button type="button" class="liveImgPrvBtnPray" id="{{$praypost->id}}"><i class="fa fa-chevron-right arrow" aria-hidden="true"></i>
                                 </button> 
                              </div>
                           </div>

                           @if($praypost->post_type=='image')
                              <div class="liveImpPrvPray" id="liveImpPrvPray_{{$praypost->id}}">
                                 <img class="card-img-top-one" src="{{ $praypost->post_description }}" >
                              </div>
                              @else
                           @endif   
                        </div>

                        <div class="video" id="video-box-pray_{{$praypost->id}}" <?php if($praypost->post_type=='video') { ?> style="display: block;" <?php } else { ?> style="display: none;" <?php }?>>

                           <label class="radio-inline"><input type="radio" class="vdovalPray" name="vdochoicePray" id="vdochoicePray0_{{$praypost->id}}" value="0" <?php if($praypost->post_media_choice=='0') { echo 'checked'; }?>>Upload Video</label>

                           <label class="radio-inline"><input type="radio" class="vdovalPray" name="vdochoicePray" id="vdochoicePray1_{{$praypost->id}}" value="1" <?php if($praypost->post_media_choice=='1') { echo 'checked'; }?>>Save From Site</label>

                           <div class="localVdoPray" id="localVdoPray_{{$praypost->id}}" <?php if($praypost->post_media_choice=='0') { ?> style="display: block;" <?php } else { ?> style="display: none;" <?php }?>>

                              <input type="file" class="postvideoPray" name="postvideoPray" id="postvideoPray_{{$praypost->id}}" accept="video/*" onchange="postvideoPrayFunction(<?php echo $praypost->id;?>)">

                              <input type="hidden" name="postvideoPrayHd" id="postvideoPrayHd_{{$praypost->id}}" value="{{$praypost->post_description}}">

                           </div>

                           <div class="save-btn-video liveVdourlPray" id="liveVdourlPray_{{$praypost->id}}" <?php if($praypost->post_media_choice=='1') { ?> style="display: block;" <?php } else { ?> style="display: none;" <?php }?>>

                              <div class="text-box-url-pray" id="text-box-url-video-pray_{{$praypost->id}}">

                                 @if( $praypost->post_type=='video'  && $praypost->post_media_choice=='1') 
                                    <input type="text" class="postvideourlPray" name="postvideourlPray" id="postvideourlPray_{{$praypost->id}}" value="{{$praypost->post_description}}" placeholder="Enter website" onfocusout="postvideourlPrayFunction(<?php echo $praypost->id;?>)">
                                  @else

                                  <input type="text" class="postvideourlPray" name="postvideourlPray" id="postvideourlPray_{{$praypost->id}}"placeholder="Enter website" onfocusout="postvideourlPrayFunction(<?php echo $praypost->id;?>)">

                                 @endif
                                 <button type="button" class="liveVdoPrvBtnPray" id="liveVdoPrvBtnPray_{{$praypost->id}}"><i class="fa fa-chevron-right arrow" aria-hidden="true"></i>
                                 </button>
                                 
                              </div>

                           </div>

                           <div class="liveVdoPrvPray" id="liveVdoPrvPray_{{$praypost->id}}">
                              
                              @if( $praypost->post_type=='video')
                                 @if($praypost->post_media_choice=='0')   
                                    <video class="card-img-top video" controls>
                                       <source src="{{ $praypost->post_description }}" type="video/mp4">
                                    </video>
                                    @else
                                    <?php
                                       $videoLiveUrl = explode("=", $praypost->post_description);
                                       if(!empty($videoLiveUrl[1])) { ?>
                                    <iframe class="card-img-top video" src="https://www.youtube.com/embed/<?php echo $videoLiveUrl[1]; ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> 
                                    <?php } ?>
                                 @endif
                                 @else
                              @endif   
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="modal-footer">
                     <div class="row">
                        <div class="col-md-3 footer-btn">
                           <div class="share isLikePray <?php if($praypost->is_like_post=='1') { echo 'islikedyncls'; }?>" id="isLikePray_{{$praypost->id}}">
                              <img src="{{ url('/public') }}/images/like.png" class="share-img" />
                              <p style="font-family: Montserrat;">Like</p>0
                           </div>
                        </div>
                        <div class="col-md-3 footer-btn">
                           <div class="share isPrayPray <?php if($praypost->is_pray_post=='1') { echo 'ispraydyncls'; }?>" id="isPrayPray_{{$praypost->id}}">
                              <img src="{{ url('/public') }}/images/pray.png" class="share-img" />
                              <?php 
                              $total_praycount = DB::table('posts')->where('id',$praypost->id)->where('is_pray_post', '1')->count();
                              $total_feedpraycount = DB::table('feedposts')->where('post_id',$praypost->id)->where('feed_pray', '1')->count();
                              $praypost_total = $total_praycount + $total_feedpraycount;  
                              ?>
                              <p style="font-family: Montserrat;">Pray</p><span>{{$praypost_total}}</span>
                           </div>
                        </div>
                        <div class="col-md-3 footer-btn">
                           <a href="{{url('/posts/deletepost', $praypost->id)}}" onclick="return PrayDelFunction();" class="delete isDelPray" id="isDelPray_{{$praypost->id}}">
                              <img src="{{ url('/public') }}/images/delete.png" class="del-img"/>
                              <p style="font-family: Montserrat;">Delete</p>
                           </a>
                        </div>
                        <div class="col-md-3 footer-btn">
                           <button type="submit" name="editpostBtnPray" id="editpostBtnPray_{{$praypost->id}}" class="save">
                              <img src="{{ url('/public') }}/images/save.png" class="save-img"/>
                              <p style="font-family: Montserrat;">Save</p>
                           </button>
                        </div>
                     </div>
                  </div>
               </form>
            </div>
         </div>
      </div>
  
   @endforeach
   @endif
   <!-- modal section end -->
   <!-- SECTION THIRD END -->
</div>

<script>
/************************Delete Post Start*******************************/
   function DefaultDelFunction() {
      if(!confirm("Are You Sure to delete this post?"))
      event.preventDefault();
   }
   function LikeDelFunction() {
      if(!confirm("Are You Sure to delete this post?"))
      event.preventDefault();
   }
   function PrayDelFunction() {
      if(!confirm("Are You Sure to delete this post?"))
      event.preventDefault();
  }
/************************Delete Post End*********************************/
/************************Create Post Validation Start********************/
function validateAddForm() {

  var choiceCreate = document.getElementById('choiceCreate').value;

  if(choiceCreate=='image') {

   var imgchoiceCreateVal = document.querySelector('input[name="imgchoiceCreate"]:checked').value;
   if(imgchoiceCreateVal=='1') {
      var postpicurlCreateVal = document.getElementById('postpicurlCreate').value;
      var postpicurlCreate = document.getElementById('postpicurlCreate');
      if(postpicurlCreateVal!='') {
         postpicurlCreate.classList.remove("postpicurlCreateError");
         return true;
      } else {
         postpicurlCreate.classList.add("postpicurlCreateError");
         return false;
      }
   } else {
      var postpicCreateVal = document.getElementById('postpicCreate').value;
      var postpicCreate = document.getElementById('postpicCreate');
      if(postpicCreateVal!='') {
         postpicCreate.classList.remove("postpicCreateError");
         return true;
         } else {
         postpicCreate.classList.add("postpicCreateError");
         return false;
      }
   }

  } else if(choiceCreate=='video') {

   var vdochoiceCreateVal = document.querySelector('input[name="vdochoiceCreate"]:checked').value;
   if(vdochoiceCreateVal=='1') {
      var postvideourlCreateVal = document.getElementById('postvideourlCreate').value;
      var postvideourlCreate = document.getElementById('postvideourlCreate');
      if(postvideourlCreateVal!='') {
         postvideourlCreate.classList.remove("postvideourlCreateError");
         return true;
      } else {
         postvideourlCreate.classList.add("postvideourlCreateError");
         return false;
      }
   } else {
      var postvideoCreateVal = document.getElementById('postvideoCreate').value;
      var postvideoCreate = document.getElementById('postvideoCreate');
      if(postvideoCreateVal!='') {
         postvideoCreate.classList.remove("postvideoCreateError");
         return true;
         } else {
         postvideoCreate.classList.add("postvideoCreateError");   
         return false;
      }
   }

  } else {
    var messageCreateVal = document.getElementById('messageCreate').value;
    var messageCreate = document.getElementById('messageCreate');
     if(messageCreateVal!='') {
      messageCreate.classList.remove("messageCreateError");
      return true;
     } else {
      messageCreate.classList.add("messageCreateError");
      return false;
     }
  }
 
}

function messageCreateFunction() {
     var messageCreateVal = document.getElementById('messageCreate').value;
     var messageCreate = document.getElementById('messageCreate');
     if(messageCreateVal!='') {
      messageCreate.classList.remove("messageCreateError");
     } else {
      messageCreate.classList.add("messageCreateError");
     }
  }

function postpicCreateFunction() {
     var postpicCreateVal = document.getElementById('postpicCreate').value;
     var postpicCreate = document.getElementById('postpicCreate');
     if(postpicCreateVal!='') {
      postpicCreate.classList.remove("postpicCreateError");
     } else {
      postpicCreate.classList.add("postpicCreateError");
     }
  } 

function postpicurlCreateFunction() {
      var postpicurlCreateVal = document.getElementById('postpicurlCreate').value;
      var postpicurlCreate = document.getElementById('postpicurlCreate');
      if(postpicurlCreateVal!='') {
         postpicurlCreate.classList.remove("postpicurlCreateError");
      } else {
         postpicurlCreate.classList.add("postpicurlCreateError");
      }
  }

function postvideoCreateFunction() {
      var postvideoCreateVal = document.getElementById('postvideoCreate').value;
      var postvideoCreate = document.getElementById('postvideoCreate');
      if(postvideoCreateVal!='') {
         postvideoCreate.classList.remove("postvideoCreateError");
         } else {
         postvideoCreate.classList.add("postvideoCreateError");   
      }
  } 

function postvideourlCreateFunction() {
      var postvideourlCreateVal = document.getElementById('postvideourlCreate').value;
      var postvideourlCreate = document.getElementById('postvideourlCreate');
      if(postvideourlCreateVal!='') {
         postvideourlCreate.classList.remove("postvideourlCreateError");
      } else {
         postvideourlCreate.classList.add("postvideourlCreateError");
      }
  } 
/************************Create Post Validation End********************/
/************************Default Post Validation Start*****************/

function validateDefaultForm(id) {
  var postId = id;
  
  var choiceDefault = document.getElementById('choiceDefault_'+postId).value;
  
  if(choiceDefault=='image') {

    if(document.getElementById('imgchoiceDefault0_'+postId).checked) { 
      var imgchoiceDefaultVal = document.getElementById('imgchoiceDefault0_'+postId).value;
    } else {
      var imgchoiceDefaultVal = document.getElementById('imgchoiceDefault1_'+postId).value;
    }

   if(imgchoiceDefaultVal=='1') {
      var postpicurlDefaultVal = document.getElementById('postpicurlDefault_'+postId).value;
      var postpicurlDefault = document.getElementById('postpicurlDefault_'+postId);
      if(postpicurlDefaultVal!='') {
         postpicurlDefault.classList.remove("postpicurlDefaultError");
         return true;
      } else {
         postpicurlDefault.classList.add("postpicurlDefaultError");
         return false;
      }
   } else {
      var postpicDefaultVal = document.getElementById('postpicDefaultHd_'+postId).value;
      var postpicDefault = document.getElementById('postpicDefault_'+postId);
      if(postpicDefaultVal!='') {
         postpicDefault.classList.remove("postpicDefaultError");
         return true;
         } else {
         postpicDefault.classList.add("postpicDefaultError");
         return false;
      }
   }

  } else if(choiceDefault=='video') {

   if(document.getElementById('vdochoiceDefault0_'+postId).checked) { 
      var vdochoiceDefaultVal = document.getElementById('vdochoiceDefault0_'+postId).value;
    } else {
      var vdochoiceDefaultVal = document.getElementById('vdochoiceDefault1_'+postId).value;
    }

   if(vdochoiceDefaultVal=='1') {
      var postvideourlDefaultVal = document.getElementById('postvideourlDefault_'+postId).value;
      var postvideourlDefault = document.getElementById('postvideourlDefault_'+postId);
      if(postvideourlDefaultVal!='') {
         postvideourlDefault.classList.remove("postvideourlDefaultError");
         return true;
      } else {
         postvideourlDefault.classList.add("postvideourlDefaultError");
         return false;
      }
   } else {
      var postvideoDefaultVal = document.getElementById('postvideoDefaultHd_'+postId).value;
      var postvideoDefault = document.getElementById('postvideoDefault_'+postId);
      if(postvideoDefaultVal!='') {
         postvideoDefault.classList.remove("postvideoDefaultError");
         return true;
         } else {
         postvideoDefault.classList.add("postvideoDefaultError");   
         return false;
      }
   }

  } else {
    var messageDefaultVal = document.getElementById('messageDefault_'+postId).value;
    var messageDefault = document.getElementById('messageDefault_'+postId);
     if(messageDefaultVal!='') {
      messageDefault.classList.remove("messageDefaultError");
      return true;
     } else {
      messageDefault.classList.add("messageDefaultError");
      return false;
     }
  }
 
} 

function messageDefaultFunction(id) {
     var postId = id;
     var messageDefaultVal = document.getElementById('messageDefault_'+postId).value;
     var messageDefault = document.getElementById('messageDefault_'+postId);
     if(messageDefaultVal!='') {
      messageDefault.classList.remove("messageDefaultError");
     } else {
      messageDefault.classList.add("messageDefaultError");
     }
  }

function postpicDefaultFunction(id) {
      var postId = id;
      var postpicDefaultVal = document.getElementById('postpicDefaultHd_'+postId).value;
      var postpicDefault = document.getElementById('postpicDefault_'+postId);
      if(postpicDefaultVal!='') {
         postpicDefault.classList.remove("postpicDefaultError");
         } else {
         postpicDefault.classList.add("postpicDefaultError");
      }
  } 

function postpicurlDefaultFunction(id) {
      var postId = id;
      var postpicurlDefaultVal = document.getElementById('postpicurlDefault_'+postId).value;
      var postpicurlDefault = document.getElementById('postpicurlDefault_'+postId);
      if(postpicurlDefaultVal!='') {
         postpicurlDefault.classList.remove("postpicurlDefaultError");
      } else {
         postpicurlDefault.classList.add("postpicurlDefaultError");
      }
  }

function postvideoDefaultFunction(id) {
      var postId = id;
      var postvideoDefaultVal = document.getElementById('postvideoDefaultHd_'+postId).value;
      var postvideoDefault = document.getElementById('postvideoDefault_'+postId);
      if(postvideoDefaultVal!='') {
         postvideoDefault.classList.remove("postvideoDefaultError");
         } else {
         postvideoDefault.classList.add("postvideoDefaultError");   
      }
  } 

function postvideourlDefaultFunction(id) {
      var postId = id;
      var postvideourlDefaultVal = document.getElementById('postvideourlDefault_'+postId).value;
      var postvideourlDefault = document.getElementById('postvideourlDefault_'+postId);
      if(postvideourlDefaultVal!='') {
         postvideourlDefault.classList.remove("postvideourlDefaultError");
      } else {
         postvideourlDefault.classList.add("postvideourlDefaultError");
      }
  }
/************************Default Post Validation End*****************/
/************************Like Post Validation Start******************/

function validateLikeForm(id) {
  var postId = id;
  
  var choiceLike = document.getElementById('choiceLike_'+postId).value;
  
  if(choiceLike=='image') {

    if(document.getElementById('imgchoiceLike0_'+postId).checked) { 
      var imgchoiceLikeVal = document.getElementById('imgchoiceLike0_'+postId).value;
    } else {
      var imgchoiceLikeVal = document.getElementById('imgchoiceLike1_'+postId).value;
    }

   if(imgchoiceLikeVal=='1') {
      var postpicurlLikeVal = document.getElementById('postpicurlLike_'+postId).value;
      var postpicurlLike = document.getElementById('postpicurlLike_'+postId);
      if(postpicurlLikeVal!='') {
         postpicurlLike.classList.remove("postpicurlLikeError");
         return true;
      } else {
         postpicurlLike.classList.add("postpicurlLikeError");
         return false;
      }
   } else {
      var postpicLikeVal = document.getElementById('postpicLikeHd_'+postId).value;
      var postpicLike = document.getElementById('postpicLike_'+postId);
      if(postpicLikeVal!='') {
         postpicLike.classList.remove("postpicLikeError");
         return true;
         } else {
         postpicLike.classList.add("postpicLikeError");
         return false;
      }
   }

  } else if(choiceLike=='video') {

   if(document.getElementById('vdochoiceLike0_'+postId).checked) { 
      var vdochoiceLikeVal = document.getElementById('vdochoiceLike0_'+postId).value;
    } else {
      var vdochoiceLikeVal = document.getElementById('vdochoiceLike1_'+postId).value;
    }

   if(vdochoiceLikeVal=='1') {
      var postvideourlLikeVal = document.getElementById('postvideourlLike_'+postId).value;
      var postvideourlLike = document.getElementById('postvideourlLike_'+postId);
      if(postvideourlLikeVal!='') {
         postvideourlLike.classList.remove("postvideourlLikeError");
         return true;
      } else {
         postvideourlLike.classList.add("postvideourlLikeError");
         return false;
      }
   } else {
      var postvideoLikeVal = document.getElementById('postvideoLikeHd_'+postId).value;
      var postvideoLike = document.getElementById('postvideoLike_'+postId);
      if(postvideoLikeVal!='') {
         postvideoLike.classList.remove("postvideoLikeError");
         return true;
         } else {
         postvideoLike.classList.add("postvideoLikeError");   
         return false;
      }
   }

  } else {
    var messageLikeVal = document.getElementById('messageLike_'+postId).value;
    var messageLike = document.getElementById('messageLike_'+postId);
     if(messageLikeVal!='') {
      messageLike.classList.remove("messageLikeError");
      return true;
     } else {
      messageLike.classList.add("messageLikeError");
      return false;
     }
  }
 
} 

function messageLikeFunction(id) {
     var postId = id;
     var messageLikeVal = document.getElementById('messageLike_'+postId).value;
     var messageLike = document.getElementById('messageLike_'+postId);
     if(messageLikeVal!='') {
      messageLike.classList.remove("messageLikeError");
     } else {
      messageLike.classList.add("messageLikeError");
     }
  }

function postpicLikeFunction(id) {
      var postId = id;
      var postpicLikeVal = document.getElementById('postpicLikeHd_'+postId).value;
      var postpicLike = document.getElementById('postpicLike_'+postId);
      if(postpicLikeVal!='') {
         postpicLike.classList.remove("postpicLikeError");
         } else {
         postpicLike.classList.add("postpicLikeError");
      }
  } 

function postpicurlLikeFunction(id) {
      var postId = id;
      var postpicurlLikeVal = document.getElementById('postpicurlLike_'+postId).value;
      var postpicurlLike = document.getElementById('postpicurlLike_'+postId);
      if(postpicurlLikeVal!='') {
         postpicurlLike.classList.remove("postpicurlLikeError");
      } else {
         postpicurlLike.classList.add("postpicurlLikeError");
      }
  }

function postvideoLikeFunction(id) {
      var postId = id;
      var postvideoLikeVal = document.getElementById('postvideoLikeHd_'+postId).value;
      var postvideoLike = document.getElementById('postvideoLike_'+postId);
      if(postvideoLikeVal!='') {
         postvideoLike.classList.remove("postvideoLikeError");
         } else {
         postvideoLike.classList.add("postvideoLikeError");   
      }
  } 

function postvideourlLikeFunction(id) {
      var postId = id;
      var postvideourlLikeVal = document.getElementById('postvideourlLike_'+postId).value;
      var postvideourlLike = document.getElementById('postvideourlLike_'+postId);
      if(postvideourlLikeVal!='') {
         postvideourlLike.classList.remove("postvideourlLikeError");
      } else {
         postvideourlLike.classList.add("postvideourlLikeError");
      }
  }
/************************Like Post Validation End********************/
/************************Pray Post Validation Start******************/

function validatePrayForm(id) {
  var postId = id;
  
  var choicePray = document.getElementById('choicePray_'+postId).value;
  
  if(choicePray=='image') {

    if(document.getElementById('imgchoicePray0_'+postId).checked) { 
      var imgchoicePrayVal = document.getElementById('imgchoicePray0_'+postId).value;
    } else {
      var imgchoicePrayVal = document.getElementById('imgchoicePray1_'+postId).value;
    }

   if(imgchoicePrayVal=='1') {
      var postpicurlPrayVal = document.getElementById('postpicurlPray_'+postId).value;
      var postpicurlPray = document.getElementById('postpicurlPray_'+postId);
      if(postpicurlPrayVal!='') {
         postpicurlPray.classList.remove("postpicurlPrayError");
         return true;
      } else {
         postpicurlPray.classList.add("postpicurlPrayError");
         return false;
      }
   } else {
      var postpicPrayVal = document.getElementById('postpicPrayHd_'+postId).value;
      var postpicPray = document.getElementById('postpicPray_'+postId);
      if(postpicPrayVal!='') {
         postpicPray.classList.remove("postpicPrayError");
         return true;
         } else {
         postpicPray.classList.add("postpicPrayError");
         return false;
      }
   }

  } else if(choicePray=='video') {

   if(document.getElementById('vdochoicePray0_'+postId).checked) { 
      var vdochoicePrayVal = document.getElementById('vdochoicePray0_'+postId).value;
    } else {
      var vdochoicePrayVal = document.getElementById('vdochoicePray1_'+postId).value;
    }

   if(vdochoicePrayVal=='1') {
      var postvideourlPrayVal = document.getElementById('postvideourlPray_'+postId).value;
      var postvideourlPray = document.getElementById('postvideourlPray_'+postId);
      if(postvideourlPrayVal!='') {
         postvideourlPray.classList.remove("postvideourlPrayError");
         return true;
      } else {
         postvideourlPray.classList.add("postvideourlPrayError");
         return false;
      }
   } else {
      var postvideoPrayVal = document.getElementById('postvideoPrayHd_'+postId).value;
      var postvideoPray = document.getElementById('postvideoPray_'+postId);
      if(postvideoPrayVal!='') {
         postvideoPray.classList.remove("postvideoPrayError");
         return true;
         } else {
         postvideoPray.classList.add("postvideoPrayError");   
         return false;
      }
   }

  } else {
    var messagePrayVal = document.getElementById('messagePray_'+postId).value;
    var messagePray = document.getElementById('messagePray_'+postId);
     if(messagePrayVal!='') {
      messagePray.classList.remove("messagePrayError");
      return true;
     } else {
      messagePray.classList.add("messagePrayError");
      return false;
     }
  }
 
} 

function messagePrayFunction(id) {
     var postId = id;
     var messagePrayVal = document.getElementById('messagePray_'+postId).value;
     var messagePray = document.getElementById('messagePray_'+postId);
     if(messagePrayVal!='') {
      messagePray.classList.remove("messagePrayError");
     } else {
      messagePray.classList.add("messagePrayError");
     }
  }

function postpicPrayFunction(id) {
      var postId = id;
      var postpicPrayVal = document.getElementById('postpicPrayHd_'+postId).value;
      var postpicPray = document.getElementById('postpicPray_'+postId);
      if(postpicPrayVal!='') {
         postpicPray.classList.remove("postpicPrayError");
         } else {
         postpicPray.classList.add("postpicPrayError");
      }
  } 

function postpicurlPrayFunction(id) {
      var postId = id;
      var postpicurlPrayVal = document.getElementById('postpicurlPray_'+postId).value;
      var postpicurlPray = document.getElementById('postpicurlPray_'+postId);
      if(postpicurlPrayVal!='') {
         postpicurlPray.classList.remove("postpicurlPrayError");
      } else {
         postpicurlPray.classList.add("postpicurlPrayError");
      }
  }

function postvideoPrayFunction(id) {
      var postId = id;
      var postvideoPrayVal = document.getElementById('postvideoPrayHd_'+postId).value;
      var postvideoPray = document.getElementById('postvideoPray_'+postId);
      if(postvideoPrayVal!='') {
         postvideoPray.classList.remove("postvideoPrayError");
         } else {
         postvideoPray.classList.add("postvideoPrayError");   
      }
  } 

function postvideourlPrayFunction(id) {
      var postId = id;
      var postvideourlPrayVal = document.getElementById('postvideourlPray_'+postId).value;
      var postvideourlPray = document.getElementById('postvideourlPray_'+postId);
      if(postvideourlPrayVal!='') {
         postvideourlPray.classList.remove("postvideourlPrayError");
      } else {
         postvideourlPray.classList.add("postvideourlPrayError");
      }
  } 
/************************Pray Post Validation End******************/          
 </script>

<script type="text/javascript">
   $(document).ready(function(){

   /************************Create Post Start******************/

     $("#choiceCreate").change(function(){
      $('#messageCreate').removeClass('messageCreateError');
      $('#postpicCreate').removeClass('postpicCreateError');
      $('#postpicurlCreate').removeClass('postpicurlCreateError');
      $('#postvideoCreate').removeClass('postvideoCreateError');
      $('#postvideourlCreate').removeClass('postvideourlCreateError');
       var choice = $(this).val();
       if(choice == "text"){
            $("#text-box-create").show();
            $("#image-box-create").hide();
            $("#video-box-create").hide();
         } else if(choice== "image"){
            $("#text-box-create").hide();
            $("#image-box-create").show();
            $("#video-box-create").hide();
         } else if(choice== "video"){
            $("#text-box-create").hide();
            $("#image-box-create").hide();
            $("#video-box-create").show();
         } else {
            $("#text-box-create").show();
            $("#image-box-create").hide();
            $("#video-box-create").hide();
         }
     });
   
      $("#isLikeCreate").click(function(){
         var islike = $('#likeCreate').val();
         if(islike==0) {
            $('#isLikeCreate').addClass('islikedyncls');
            $('#likeCreate').val('1');
         } else {
            $('#isLikeCreate').removeClass('islikedyncls');
            $('#likeCreate').val('0');
         }
      }); 
   
      $("#isPrayCreate").click(function(){
         var ispray = $('#prayCreate').val();
         if(ispray==0) {
            $('#isPrayCreate').addClass('ispraydyncls');
            $('#prayCreate').val('1');
         } else {
            $('#isPrayCreate').removeClass('ispraydyncls');
            $('#prayCreate').val('0');
         }
      }); 
   
      $(".imgvalCreate").click(function(){
         $('#postpicCreate').removeClass('postpicCreateError');
         $('#postpicurlCreate').removeClass('postpicurlCreateError');
         var imgVal = $(this).val();
         var imgValId = $(this).attr('id');
         if(imgVal==1) {
            $("#postpicCreate").val('');
            $('#localImgCreate').hide();
            $('#liveImgurlCreate').show();
            $("#liveImpPrv").html('');
         } else {
            $("#postpicurlCreate").val('');
            $('#liveImgurlCreate').hide();
            $('#localImgCreate').show();
            $("#liveImpPrv").html('');
         }
      }); 
   
      $("#liveImgPrvBtn").click(function(){
       var srcImg  = $("#postpicurlCreate").val();
       if(srcImg != '' ){
         $('#postpicurlCreate').removeClass('postpicurlCreateError');
         $("#liveImpPrv").html('<img src = "'+ srcImg +'" width="100%" height ="250px">');
       } else {
         $('#postpicurlCreate').addClass('postpicurlCreateError');
       }
     });
   
      $(".vdovalCreate").click(function(){
         $('#postvideoCreate').removeClass('postvideoCreateError');
         $('#postvideourlCreate').removeClass('postvideourlCreateError');
         var vdoVal = $(this).val();
         var vdoValId = $(this).attr('id');
         if(vdoVal==1) {
            $("#postvideoCreate").val('');
            $('#localVdoCreate').hide();
            $('#liveVdourlCreate').show();
            $("#liveVdoPrv").html('');
         } else {
            $("#postvideourlCreate").val('');
            $('#liveVdourlCreate').hide();
            $('#localVdoCreate').show();
            $("#liveVdoPrv").html('');
         }
      }); 
   
      $("#liveVdoPrvBtn").click(function(){
         var srcVdo  = $("#postvideourlCreate").val();
      if(srcVdo!='') {
         $('#postvideourlCreate').removeClass('postvideourlCreateError');
         var numbersArray = srcVdo.split('=');
         if(numbersArray[1] != ''){ 
         $("#liveVdoPrv").html('<iframe width="100%" height ="250px" src="https://www.youtube.com/embed/'+numbersArray[1]+'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>');
         } 
      } else {
         $('#postvideourlCreate').addClass('postvideourlCreateError');
      }
     });

/************************Create Post End******************/
/************************Default Post Start***************/

    $(".choiceDefault").change(function(){
       var choice = $(this).val();
       var choiceId = $(this).attr('id');
       var postId = choiceId.replace('choiceDefault_','');
       // alert(choice+'=='+choiceId+'>>>'+postId);
       $('#messageDefault_'+postId).removeClass('messageDefaultError');
       $('#postpicDefault_'+postId).removeClass('postpicDefaultError');
       $('#postpicurlDefault_'+postId).removeClass('postpicurlDefaultError');
       $('#postvideoDefault_'+postId).removeClass('postvideoDefaultError');
       $('#postvideourlDefault_'+postId).removeClass('postvideourlDefaultError');

       if(choice == "text"){
            $("#text-box-default_"+postId).show();
            $("#image-box-default_"+postId).hide();
            $("#video-box-default_"+postId).hide();
         } else if(choice== "image"){
            $("#text-box-default_"+postId).hide();
            $("#image-box-default_"+postId).show();
            $("#video-box-default_"+postId).hide();
         } else if(choice== "video"){
            $("#text-box-default_"+postId).hide();
            $("#image-box-default_"+postId).hide();
            $("#video-box-default_"+postId).show();
         }/* else {
            $("#text-box-default_"+postId).show();
            $("#image-box-default_"+postId).hide();
            $("#video-box-default_"+postId).hide();
         }*/
     });
   
      $(".isLikeDefault").click(function(){
         var choiceId = $(this).attr('id');
         var postId = choiceId.replace('isLikeDefault_','');
         var islike = $('#likeDefault_'+postId).val();
        // alert(islike+'=='+choiceId+'>>>'+postId);
         if(islike==0) {
            $('#isLikeDefault_'+postId).addClass('islikedyncls');
            $('#likeDefault_'+postId).val('1');
         } else {
            $('#isLikeDefault_'+postId).removeClass('islikedyncls');
            $('#likeDefault_'+postId).val('0');
         }
      }); 
   
      $(".isPrayDefault").click(function(){
         var choiceId = $(this).attr('id');
         var postId = choiceId.replace('isPrayDefault_','');
         var ispray = $('#prayDefault_'+postId).val();
        // alert(ispray+'=='+choiceId+'>>>'+postId);
         if(ispray==0) {
            $('#isPrayDefault_'+postId).addClass('ispraydyncls');
            $('#prayDefault_'+postId).val('1');
         } else {
            $('#isPrayDefault_'+postId).removeClass('ispraydyncls');
            $('#prayDefault_'+postId).val('0');
         }
      }); 
   
      $(".imgvalDefault").click(function(){
         var imgVal = $(this).val();
         var imgValId = $(this).attr('id');
         var imgIdArr =  imgValId.split("_");
         var postId = imgIdArr[1];

         $('#postpicDefault_'+postId).removeClass('postpicDefaultError');
         $('#postpicurlDefault_'+postId).removeClass('postpicurlDefaultError');

         if(imgVal==1) {
            $("#postpicDefault_"+postId).val('');
            $('#localImgDefault_'+postId).hide();
            $('#liveImgurlDefault_'+postId).show();
            $("#liveImpPrvDefault_"+postId).html('');
         } else {
            $("#postpicurlDefault_"+postId).val('');
            $('#liveImgurlDefault_'+postId).hide();
            $('#localImgDefault_'+postId).show();
            $("#liveImpPrvDefault_"+postId).html('');
         }
      }); 
   
      $(".liveImgPrvBtnDefault").click(function(){
         var choiceId = $(this).attr('id');
         var postId = choiceId.replace('liveImgPrvBtnDefault_','');
         var srcImg  = $("#postpicurlDefault_"+postId).val();
       if(srcImg != '' ){
         $('#postpicurlDefault_'+postId).removeClass('postpicurlDefaultError');
         $("#liveImpPrvDefault_"+postId).html('<img src = "'+ srcImg +'" width="100%" height ="250px">');
       } else {
         $('#postpicurlDefault_'+postId).addClass('postpicurlDefaultError');
       }
     });
   
      $(".vdovalDefault").click(function(){
         var vdoVal = $(this).val();
         var vdoValId = $(this).attr('id');
         var videoIdArr =  vdoValId.split("_");
         var postId = videoIdArr[1];

         $('#postvideoDefault_'+postId).removeClass('postvideoDefaultError');
         $('#postvideourlDefault_'+postId).removeClass('postvideourlDefaultError');

         if(vdoVal==1) {
            $("#postvideoDefault_"+postId).val('');
            $('#localVdoDefault_'+postId).hide();
            $('#liveVdourlDefault_'+postId).show();
            $("#liveVdoPrvDefault_"+postId).html('');
         } else {
            $("#postvideourlDefault_"+postId).val('');
            $('#liveVdourlDefault_'+postId).hide();
            $('#localVdoDefault_'+postId).show();
            $("#liveVdoPrvDefault_"+postId).html('');
         }
      }); 
   
      $(".liveVdoPrvBtnDefault").click(function(){
         var choiceId = $(this).attr('id');
         var postId = choiceId.replace('liveVdoPrvBtnDefault_','');
         var srcVdo  = $("#postvideourlDefault_"+postId).val();
         if(srcVdo!='') {
          $('#postvideourlDefault_'+postId).removeClass('postvideourlDefaultError');  
         var numbersArray = srcVdo.split('=');
         if(numbersArray[1] != ''){ 
         $("#liveVdoPrvDefault_"+postId).html('<iframe width="100%" height ="250px" src="https://www.youtube.com/embed/'+numbersArray[1]+'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>');
         } 
        } else {
          $('#postvideourlDefault_'+postId).addClass('postvideourlDefaultError');
       }
     });
/************************Default Post End***************/ 
/************************Like Post Start****************/

    $(".choiceLike").change(function(){
       var choice = $(this).val();
       var choiceId = $(this).attr('id');
       var postId = choiceId.replace('choiceLike_','');
       // alert(choice+'=='+choiceId+'>>>'+postId);

       $('#messageLike_'+postId).removeClass('messageLikeError');
       $('#postpicLike_'+postId).removeClass('postpicLikeError');
       $('#postpicurlLike_'+postId).removeClass('postpicurlLikeError');
       $('#postvideoLike_'+postId).removeClass('postvideoLikeError');
       $('#postvideourlLike_'+postId).removeClass('postvideourlLikeError');

       if(choice == "text"){
            $("#text-box-like_"+postId).show();
            $("#image-box-like_"+postId).hide();
            $("#video-box-like_"+postId).hide();
         } else if(choice== "image"){
            $("#text-box-like_"+postId).hide();
            $("#image-box-like_"+postId).show();
            $("#video-box-like_"+postId).hide();
         } else if(choice== "video"){
            $("#text-box-like_"+postId).hide();
            $("#image-box-like_"+postId).hide();
            $("#video-box-like_"+postId).show();
         }/* else {
            $("#text-box-like_"+postId).show();
            $("#image-box-like_"+postId).hide();
            $("#video-box-like_"+postId).hide();
         }*/
     });
   
      $(".isLikeLike").click(function(){
         var choiceId = $(this).attr('id');
         var postId = choiceId.replace('isLikeLike_','');
         var islike = $('#likeLike_'+postId).val();
        // alert(islike+'=='+choiceId+'>>>'+postId);
         if(islike==0) {
            $('#isLikeLike_'+postId).addClass('islikedyncls');
            $('#likeLike_'+postId).val('1');
         } else {
            $('#isLikeLike_'+postId).removeClass('islikedyncls');
            $('#likeLike_'+postId).val('0');
         }
      }); 
   
      $(".isPrayLike").click(function(){
         var choiceId = $(this).attr('id');
         var postId = choiceId.replace('isPrayLike_','');
         var ispray = $('#prayLike_'+postId).val();
        // alert(ispray+'=='+choiceId+'>>>'+postId);
         if(ispray==0) {
            $('#isPrayLike_'+postId).addClass('ispraydyncls');
            $('#prayLike_'+postId).val('1');
         } else {
            $('#isPrayLike_'+postId).removeClass('ispraydyncls');
            $('#prayLike_'+postId).val('0');
         }
      }); 
   
      $(".imgvalLike").click(function(){
         var imgVal = $(this).val();
         var imgValId = $(this).attr('id');
         var imgIdArr =  imgValId.split("_");
         var postId = imgIdArr[1];

         $('#postpicLike_'+postId).removeClass('postpicLikeError');
         $('#postpicurlLike_'+postId).removeClass('postpicurlLikeError');

         if(imgVal==1) {
            $("#postpicLike_"+postId).val('');
            $('#localImgLike_'+postId).hide();
            $('#liveImgurlLike_'+postId).show();
            $("#liveImpPrvLike_"+postId).html('');
         } else {
            $("#postpicurlLike_"+postId).val('');
            $('#liveImgurlLike_'+postId).hide();
            $('#localImgLike_'+postId).show();
            $("#liveImpPrvLike_"+postId).html('');
         }
      }); 
   
      $(".liveImgPrvBtnLike").click(function(){
         var choiceId = $(this).attr('id');
         var postId = choiceId.replace('liveImgPrvBtnLike_','');
         var srcImg  = $("#postpicurlLike_"+postId).val();
       if(srcImg != '' ){
         $('#postpicurlLike_'+postId).removeClass('postpicurlLikeError');
         $("#liveImpPrvLike_"+postId).html('<img src = "'+ srcImg +'" width="100%" height ="250px">');
       } else {
         $('#postpicurlLike_'+postId).addClass('postpicurlLikeError');
       }
     });
   
      $(".vdovalLike").click(function(){
         var vdoVal = $(this).val();
         var vdoValId = $(this).attr('id');
         var videoIdArr =  vdoValId.split("_");
         var postId = videoIdArr[1];

         $('#postvideoLike_'+postId).removeClass('postvideoLikeError');
         $('#postvideourlLike_'+postId).removeClass('postvideourlLikeError');

         if(vdoVal==1) {
            $("#postvideoLike_"+postId).val('');
            $('#localVdoLike_'+postId).hide();
            $('#liveVdourlLike_'+postId).show();
            $("#liveVdoPrvLike_"+postId).html('');
         } else {
            $("#postvideourlLike_"+postId).val('');
            $('#liveVdourlLike_'+postId).hide();
            $('#localVdoLike_'+postId).show();
            $("#liveVdoPrvLike_"+postId).html('');
         }
      }); 
   
      $(".liveVdoPrvBtnLike").click(function(){
         var choiceId = $(this).attr('id');
         var postId = choiceId.replace('liveVdoPrvBtnLike_','');
         var srcVdo  = $("#postvideourlLike_"+postId).val();
         if(srcVdo!='') {
            $('#postvideourlLike_'+postId).removeClass('postvideourlLikeError');
         var numbersArray = srcVdo.split('=');
         if(numbersArray[1] != ''){ 
         $("#liveVdoPrvLike_"+postId).html('<iframe width="100%" height ="250px" src="https://www.youtube.com/embed/'+numbersArray[1]+'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>');
         } 
       } else {
         $('#postvideourlLike_'+postId).addClass('postvideourlLikeError');
       }
     });
/************************Like Post End****************/
/************************Pray Post Start**************/

    $(".choicePray").change(function(){
       var choice = $(this).val();
       var choiceId = $(this).attr('id');
       var postId = choiceId.replace('choicePray_','');
       // alert(choice+'=='+choiceId+'>>>'+postId);

       $('#messagePray_'+postId).removeClass('messagePrayError');
       $('#postpicPray_'+postId).removeClass('postpicPrayError');
       $('#postpicurlPray_'+postId).removeClass('postpicurlPrayError');
       $('#postvideoPray_'+postId).removeClass('postvideoPrayError');
       $('#postvideourlPray_'+postId).removeClass('postvideourlPrayError');

       if(choice == "text"){
            $("#text-box-pray_"+postId).show();
            $("#image-box-pray_"+postId).hide();
            $("#video-box-pray_"+postId).hide();
         } else if(choice== "image"){
            $("#text-box-pray_"+postId).hide();
            $("#image-box-pray_"+postId).show();
            $("#video-box-pray_"+postId).hide();
         } else if(choice== "video"){
            $("#text-box-pray_"+postId).hide();
            $("#image-box-pray_"+postId).hide();
            $("#video-box-pray_"+postId).show();
         }/* else {
            $("#text-box-pray_"+postId).show();
            $("#image-box-pray_"+postId).hide();
            $("#video-box-pray_"+postId).hide();
         }*/
     });
   
      $(".isLikePray").click(function(){
         var choiceId = $(this).attr('id');
         var postId = choiceId.replace('isLikePray_','');
         var islike = $('#likePray_'+postId).val();
        // alert(islike+'=='+choiceId+'>>>'+postId);
         if(islike==0) {
            $('#isLikePray_'+postId).addClass('islikedyncls');
            $('#likePray_'+postId).val('1');
         } else {
            $('#isLikePray_'+postId).removeClass('islikedyncls');
            $('#likePray_'+postId).val('0');
         }
      }); 
   
      $(".isPrayPray").click(function(){
         var choiceId = $(this).attr('id');
         var postId = choiceId.replace('isPrayPray_','');
         var ispray = $('#prayPray_'+postId).val();
        // alert(ispray+'=='+choiceId+'>>>'+postId);
         if(ispray==0) {
            $('#isPrayPray_'+postId).addClass('ispraydyncls');
            $('#prayPray_'+postId).val('1');
         } else {
            $('#isPrayPray_'+postId).removeClass('ispraydyncls');
            $('#prayPray_'+postId).val('0');
         }
      }); 
   
      $(".imgvalPray").click(function(){
         var imgVal = $(this).val();
         var imgValId = $(this).attr('id');
         var imgIdArr =  imgValId.split("_");
         var postId = imgIdArr[1];

         $('#postpicPray_'+postId).removeClass('postpicPrayError');
         $('#postpicurlPray_'+postId).removeClass('postpicurlPrayError');

         if(imgVal==1) {
            $("#postpicPray_"+postId).val('');
            $('#localImgPray_'+postId).hide();
            $('#liveImgurlPray_'+postId).show();
            $("#liveImpPrvPray_"+postId).html('');
         } else {
            $("#postpicurlPray_"+postId).val('');
            $('#liveImgurlPray_'+postId).hide();
            $('#localImgPray_'+postId).show();
            $("#liveImpPrvPray_"+postId).html('');
         }
      }); 
   
      $(".liveImgPrvBtnPray").click(function(){
         var choiceId = $(this).attr('id');
         var postId = choiceId.replace('liveImgPrvBtnPray_','');
         var srcImg  = $("#postpicurlPray_"+postId).val();
       if(srcImg != '' ){
         $('#postpicurlPray_'+postId).removeClass('postpicurlPrayError');
         $("#liveImpPrvPray_"+postId).html('<img src = "'+ srcImg +'" width="100%" height ="250px">');
       } else {
         $('#postpicurlPray_'+postId).addClass('postpicurlPrayError');
       }
     });
   
      $(".vdovalPray").click(function(){
         var vdoVal = $(this).val();
         var vdoValId = $(this).attr('id');
         var videoIdArr =  vdoValId.split("_");
         var postId = videoIdArr[1];

         $('#postvideoPray_'+postId).removeClass('postvideoPrayError');
         $('#postvideourlPray_'+postId).removeClass('postvideourlPrayError');

         if(vdoVal==1) {
            $("#postvideoPray_"+postId).val('');
            $('#localVdoPray_'+postId).hide();
            $('#liveVdourlPray_'+postId).show();
            $("#liveVdoPrvPray_"+postId).html('');
         } else {
            $("#postvideourlPray_"+postId).val('');
            $('#liveVdourlPray_'+postId).hide();
            $('#localVdoPray_'+postId).show();
            $("#liveVdoPrvPray_"+postId).html('');
         }
      }); 
   
      $(".liveVdoPrvBtnPray").click(function(){
         var choiceId = $(this).attr('id');
         var postId = choiceId.replace('liveVdoPrvBtnPray_','');
         var srcVdo  = $("#postvideourlPray_"+postId).val();
         if(srcVdo!='') {
           $('#postvideourlPray_'+postId).removeClass('postvideourlPrayError');
         var numbersArray = srcVdo.split('=');
         if(numbersArray[1] != ''){ 
         $("#liveVdoPrvPray_"+postId).html('<iframe width="100%" height ="250px" src="https://www.youtube.com/embed/'+numbersArray[1]+'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>');
         } 
        } else {
         $('#postvideourlPray_'+postId).addClass('postvideourlPrayError');
       }
     });
/************************Pray Post Start**************/     
   
   });
</script> 
<style type="text/css">
/************************Dynamic Post Class Css Start*******/
   .islikedyncls { 

   background-color: #ECECEC; 
   text-align: center;
  }
   .ispraydyncls { 
   background-color: #ECECEC; 
    text-align: center;
  }
  .footer-btn .share {
   text-align: center;
}
.home_hrline {

   margin-top: 30px;
}
   .messageCreateError { border: 1px solid red; }
   .postpicCreateError { border: 1px solid red; }
   .postpicurlCreateError { border: 1px solid red; }
   .postvideoCreateError { border: 1px solid red; }
   .postvideourlCreateError { border: 1px solid red; }

   .messageDefaultError { border: 1px solid red; }
   .postpicDefaultError { border: 1px solid red; }
   .postpicurlDefaultError { border: 1px solid red; }
   .postvideoDefaultError { border: 1px solid red; }
   .postvideourlDefaultError { border: 1px solid red; }

   .messageLikeError { border: 1px solid red; }
   .postpicLikeError { border: 1px solid red; }
   .postpicurlLikeError { border: 1px solid red; }
   .postvideoLikeError { border: 1px solid red; }
   .postvideourlLikeError { border: 1px solid red; }

   .messagePrayError { border: 1px solid red; }
   .postpicPrayError { border: 1px solid red; }
   .postpicurlPrayError { border: 1px solid red; }
   .postvideoPrayError { border: 1px solid red; }
   .postvideourlPrayError { border: 1px solid red; }

   button#save_site_img_create {
   background: #EFEFEF;
   border: none;
   margin-top: 18px;
   padding: 15px;
   font-weight: bold;
   border-radius: 8px;
   width: 40%;
   }
   button#save_site_video_create {
   background: #EFEFEF;
   border: none;
   margin-top: 18px;
   padding: 15px;
   font-weight: bold;
   border-radius: 8px;
   width: 40%;
   }

   button.save_site_img_default {
   background: #EFEFEF;
   border: none;
   margin-top: 18px;
   padding: 15px;
   font-weight: bold;
   border-radius: 8px;
   width: 40%;
   }
   button.save_site_video_default {
   background: #EFEFEF;
   border: none;
   margin-top: 18px;
   padding: 15px;
   font-weight: bold;
   border-radius: 8px;
   width: 40%;
   }

   button.save_site_img_like {
   background: #EFEFEF;
   border: none;
   margin-top: 18px;
   padding: 15px;
   font-weight: bold;
   border-radius: 8px;
   width: 40%;
   }
   button.save_site_video_like {
   background: #EFEFEF;
   border: none;
   margin-top: 18px;
   padding: 15px;
   font-weight: bold;
   border-radius: 8px;
   width: 40%;
   }

   button.save_site_img_pray {
   background: #EFEFEF;
   border: none;
   margin-top: 18px;
   padding: 15px;
   font-weight: bold;
   border-radius: 8px;
   width: 40%;
   }
   button.save_site_video_pray {
   background: #EFEFEF;
   border: none;
   margin-top: 18px;
   padding: 15px;
   font-weight: bold;
   border-radius: 8px;
   width: 40%;
   }
   video.card-img-top.video.video_popup {
    margin: 15px 0px;
}
img.card-img-like_privatehide {
    width: 250px;
    margin: 0 auto;
    display: block;
    height: auto;
}
.slick-track {
    float: left;
}

/************************Dynamic Post Class Css End*******/   
</style>

<script src="{{ url('/public') }}/slick/slick.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">
$(".center-last").slick({
dots: false,
infinite: false,
centerMode: false,
centerPadding : '10px',
slidesToShow: 4,
slidesToScroll: 1,
responsive: [
{
breakpoint: 1200,
settings: {
   slidesToShow: 3,
   slidesToScroll: 1,
}
},
{
breakpoint: 990,
settings: {
   slidesToShow: 2,
   slidesToScroll: 1,
}
},
{
breakpoint: 768,
settings: {
   slidesToShow: 1,
   slidesToScroll: 1,
    variableWidth: true,
}
}


]
// arrows: true
});
$(function(){
$("#slider").slick({
speed: 1000,
dots: true,
prevArrow: '<button class="slide-arrow prev-arrow"></button>',
nextArrow: '<button class="slide-arrow next-arrow"></button>'
});
});
</script>

<script type="text/javascript">
   
$(document).ready(function(){

   $("#liveImgPrvBtn").click(function(){

      var str_image  = $("#postpicurlCreate").val();
   
      if(str_image != ''){

         url_validate = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;

         if(url_validate.test(str_image)){
         
          var str_image1  = $("#postpicurlCreate").val().split('.').pop().toLowerCase();

            if($.inArray(trim(str_image1), ['gif','png','jpg','jpeg']) == 1) {

              $("#liveImpPrv").html('<img src = "'+ str_image +'" width="100%" height ="250px">');
            }
            else{

              $("#liveImpPrv").html('<p style=color:red;height:20px;margin-top:10px;> Image Format is Wrong  </p>');
            }
         }else {
           

         $("#liveImpPrv").html('<p style=color:red;height:20px;margin-top:10px;> Image Format is Wrong  </p>');
      }

   }else {

         $("#liveImpPrv").html('<p style=color:red;height:10px></p>');
      }

});

   $(".liveImgPrvBtnDefault").click(function(){

   var btnID1 = $(this).attr('id');

   var str_image1  = ($('input#postpicurlDefault_'+btnID1).val().split('.').pop().toLowerCase());

   var str_image  = ($('input#postpicurlDefault_'+btnID1).val());

   if(str_image != ''){

      var str_imagedata1 = $('div#liveImpPrvDefault_'+btnID1);

      url_validate = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;

      if(url_validate.test(str_image)){

           if($.inArray(trim(str_image1), ['gif','png','jpg','jpeg']) == 1) {

              $(str_imagedata1).html('<img src = "'+ str_image +'" width="100%" height ="250px">');

            }else {

               $(str_imagedata1).html('<p style=color:red;height:20px;margin-top:10px;> Image Format is wrong  </p>');
            }
       }else {
         
            $(str_imagedata1).html('<p style=color:red;height:20px;margin-top:10px;> Image Format is wrong  </p>');
      }
   }else {

         $(str_imagedata1).html('<p style=color:red;height:10px> </p>');
      }     
});

   $(".liveImgPrvBtnLike").click(function(){

   var btnID1 = $(this).attr('id');
   var str_image1  = ($('input#postpicurlLike_'+btnID1).val().split('.').pop().toLowerCase());
   var str_image  = ($('input#postpicurlLike_'+btnID1).val());

   if(str_image != ""){

      var str_imagedata1 = $('div#liveImpPrvLike_'+btnID1);

       url_validate = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;

         if(url_validate.test(str_image)){

           if($.inArray(trim(str_image1), ['gif','png','jpg','jpeg']) == 1) {

              $(str_imagedata1).html('<img src = "'+ str_image +'" width="100%" height ="250px">');

            }else {

               $(str_imagedata1).html('<p style=color:red;height:20px;margin-top:10px;> Image Format is wrong  </p>');

            }
         }else{

            $(str_imagedata1).html('<p style=color:red;height:20px;margin-top:10px;> Image Format is wrong  </p>');
         }
   }else{

      $(str_imagedata1).html('<p style=color:red;height:10px> </p>');
   }
  
 });

   $(".liveImgPrvBtnPray").click(function(){
   var btnID1 = $(this).attr('id');
   var str_image1  = ($('input#postpicurlPray_'+btnID1).val().split('.').pop().toLowerCase());
   var str_image  = ($('input#postpicurlPray_'+btnID1).val());

   if(str_image != ""){

      var str_imagedata1 = $('div#liveImpPrvPray_'+btnID1);

       url_validate = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;

         if(url_validate.test(str_image)){

        if($.inArray(trim(str_image1), ['gif','png','jpg','jpeg']) == 1) {

           $(str_imagedata1).html('<img src = "'+ str_image +'" width="100%" height ="250px">');

         }else {

            $(str_imagedata1).html('<p style=color:red;height:20px;margin-top:10px;> Image Format is wrong  </p>');

         }
      }else{

          $(str_imagedata1).html('<p style=color:red;height:20px;margin-top:10px;> Image Format is wrong  </p>');
      }
   }else{
          $(str_imagedata1).html('<p style=color:red;height:10px> </p>');
      }
});

    $('.save_hide_like').on('click', function() {
            var postid = $(this).attr('post_id');
            var userid = $(this).attr('user_id');
            $.ajax({
               type: 'post',
               url: "{{url('/home/hidelike')}}",
               data:{_token: '{{csrf_token()}}', postid: postid,userid:userid}, 
               success: function(result){
                  $('#danger_timeout').html('');
                  if(result=='update'){
                     $('.post-error-message').html(
                        '<div class="alert alert-success alert-dismissable">'+
                          '<button type="button" class="close" data-dismiss="alert">'+
                              '<span aria-hidden="true">&times;</span>'+
                              '<span class="sr-only">Close</span>'+
                          '</button>'+
                          'Like Post hide Succesfully'+
                        '</div>'
                     );
                     jQuery('html, body').animate({scrollTop:0}, 'slow');
                     setInterval('location.reload()', 2000);  
                     return false;
                  }
               }
            });
         });

         $('.save_hide_pray').on('click', function() {
            var postid = $(this).attr('post_id');
            var userid = $(this).attr('user_id');
            $.ajax({
               type: 'post',
               url: "{{url('/home/hidepray')}}",
               data:{_token: '{{csrf_token()}}', postid: postid,userid:userid}, 
               success: function(result){
                  $('#danger_timeout').html('');
                  if(result=='prayupdate'){
                     $('.post-error-message').html(
                        '<div class="alert alert-success alert-dismissable">'+
                          '<button type="button" class="close" data-dismiss="alert">'+
                              '<span aria-hidden="true">&times;</span>'+
                              '<span class="sr-only">Close</span>'+
                          '</button>'+
                          'Pray Post hide Succesfully'+
                        '</div>'
                     );
                     jQuery('html, body').animate({scrollTop:0}, 'slow');
                     setInterval('location.reload()', 2000);  
                     return false;
                  }
               }
            });
         });

});


</script>

@endsection
