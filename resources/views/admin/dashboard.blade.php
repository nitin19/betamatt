@extends('layouts.default')
@section('title', 'BetaMatt | Home')
@section('content')
   <div class="container">

         <div class="message"></div>
           <div class="featurs azsdfsadfsa">
              <div class="col-md-12">
                     <h1 style="color:#000; font-size: 25px; font-family: Montserrat; font-weight: 600;" >FEATURES</h1>
               </div>
                  <div class="row login-acc">            
                      <div class="col-md-6 ">
                         <span class="dashboard_loginenable">Create New Account :</span>

                            <label class="switch">
                              <?php 
                                 if($admin_user_data->register_status=='1'){
                                    $status = 'true';
                                 } else{
                                    $status = 'false';
                                 }
                              ?>
                              <input class="register-switch-input" type="checkbox" id="registerintogBtn" value="<?php echo $status;?>" name="enabledisable" role_id="{{$admin_user_data->role}}" user_id ="{{$admin_user_data->id}}" <?php if($status=='true'){ echo 'checked';} ?>>
                              <div class="slider round">
                                 <span class="on enable_on">Enable</span>
                                 <span class="off enable_of">Disable</span>
                              </div>
                            </label>
                                
                       </div> 

                  </div>
            </div>
   </div>
         <!-- end -->
      <!-- first section -->
     <div class="container admin_container">
            <div class="row posts">
               <div class="col-md-12">
                  <h1 style="color:#000; font-size: 25px; font-family: Montserrat; font-weight: 600;" >THE FEED</h1>
               </div>
            </div>
            <!--Carousel Wrapper-->
            <div id="multi-item-example" class="carousel slide carousel-multi-item" data-ride="carousel">
               <!--Slides-->
               <div class="carousel-inner" role="listbox">
                  <!--First slide-->
                  <div class="carousel-item active">
                     <hr class="my-4">
                     <div class="row center-last admin_caro">
                        @if(count($posts_data) > 0)
                        @foreach($posts_data as $posts_datas)
                        <?php
                              $feed_post_data = DB::table('feedposts')
                                                ->where('user_id', Auth::user()->id)
                                                ->where('post_id', $posts_datas->id)
                                                ->where('post_user_id', $posts_datas->user_id)  
                                                ->first();
                           if($feed_post_data){
                              $hidedata = $feed_post_data->feed_hide;
                           } else {
                              $hidedata = '';
                           }

                        ?>
                           @if($posts_datas->post_type=='image')
                              <div class="col-mb-2 clearfix  d-md-block">
                                 <?php 
                                 if($feed_post_data){
                                    if($hidedata=='1'){?>
                                    <span class="card-notify-year cardbody_data hide_post img_hide_icon" id="post_id_{{$posts_datas->id}}" userid="{{$posts_datas->user_id}}" post-id="{{$posts_datas->id}}" curntuserid="{{Auth::user()->id}}" status="0"><i class="fa fa-eye-slash post_hide{{$posts_datas->id}}" aria-hidden="true"></i></span>
                                    <?php } else { ?>
                                    <span class="card-notify-year cardbody_data hide_post img_hide_icon" id="post_id_{{$posts_datas->id}}" userid="{{$posts_datas->user_id}}" post-id="{{$posts_datas->id}}" curntuserid="{{Auth::user()->id}}" status="1"><i class="fa fa-eye post_hide{{$posts_datas->id}}" aria-hidden="true"></i></span>
                                    <?php } 
                                 } else { ?>
                                    <span class="card-notify-year cardbody_data hide_post img_hide_icon" userid="{{$posts_datas->user_id}}" post-id="{{$posts_datas->id}}" curntuserid="{{Auth::user()->id}}" status="1"><i class="fa fa-eye" aria-hidden="true"></i></span>
                                 <?php }?>
                                 <div class="card mb-2 cardbody_data card_image" id="{{$posts_datas->id}}" data-toggle="modal" data-target="#imagemodal_{{$posts_datas->id}}">
                                    <div class="card-body card_datadetails" style="background: url({{ $posts_datas->post_description }});background-repeat:no-repeat;background-size: contain;background-position: 50%;">
                                    </div>
                                 </div>
                              </div>
                           @elseif($posts_datas->post_type=='video')
                              @if($posts_datas->post_media_choice=='0')
                              <div class="col-md-2 clearfix  d-md-block">
                                 <?php
                                 if($feed_post_data){
                                    if($hidedata=='1'){?>
                                    <span class="card-notify-year cardbody_data hide_post" userid="{{$posts_datas->user_id}}" post-id="{{$posts_datas->id}}" curntuserid="{{Auth::user()->id}}" status="0"><i class="fa fa-eye-slash" aria-hidden="true"></i></span>
                                    <?php } else { ?>
                                    <span class="card-notify-year cardbody_data hide_post" userid="{{$posts_datas->user_id}}" post-id="{{$posts_datas->id}}" curntuserid="{{Auth::user()->id}}" status="1"><i class="fa fa-eye" aria-hidden="true"></i></span>
                                    <?php } 
                                 } else { ?>
                                    <span class="card-notify-year cardbody_data hide_post" userid="{{$posts_datas->user_id}}" post-id="{{$posts_datas->id}}" curntuserid="{{Auth::user()->id}}" status="1"><i class="fa fa-eye" aria-hidden="true"></i></span>
                                 <?php } ?> 
                                 <div class="card mb-2 cardbody_data card_video" id="{{$posts_datas->id}}" data-toggle="modal" data-target="#imagemodal_{{$posts_datas->id}}">
                                    <div class="card-body card_datadetails">
                                       <video class="card-img-top video" controls>
                                          <source src="{{ $posts_datas->post_description }}" type="video/mp4">
                                       </video>
                                    </div>
                                 </div>
                              </div>    
                              @else
                              <?php
                              $videoLiveUrl = explode("=", $posts_datas->post_description);
                              if(!empty($videoLiveUrl[1])) { ?>
                                 <div class="col-md-2 clearfix  d-md-block">
                                    <?php
                                    if($feed_post_data){
                                       if($hidedata=='1'){?>
                                       <span class="card-notify-year cardbody_data hide_post" userid="{{$posts_datas->user_id}}" post-id="{{$posts_datas->id}}" curntuserid="{{Auth::user()->id}}" status="0"><i class="fa fa-eye-slash" aria-hidden="true"></i></span>
                                       <?php } else { ?>
                                       <span class="card-notify-year cardbody_data hide_post" userid="{{$posts_datas->user_id}}" post-id="{{$posts_datas->id}}" curntuserid="{{Auth::user()->id}}" status="1"><i class="fa fa-eye" aria-hidden="true"></i></span>
                                       <?php } 
                                    } else { ?>
                                       <span class="card-notify-year cardbody_data hide_post" userid="{{$posts_datas->user_id}}" post-id="{{$posts_datas->id}}" curntuserid="{{Auth::user()->id}}" status="1"><i class="fa fa-eye" aria-hidden="true"></i></span>
                                    <?php } ?>
                                    <div class="card mb-2 cardbody_data card_youtube" id="{{$posts_datas->id}}" data-toggle="modal" data-target="#imagemodal_{{$posts_datas->id}}">
                                       <div class="card-body card_datadetails">
                                          <iframe class="card-img-top video" src="https://www.youtube.com/embed/<?php echo $videoLiveUrl[1]; ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> 
                                       </div>
                                    </div>
                                 </div>
                              <?php } ?>
                              @endif
                           @else
                              <div class="col-md-2 clearfix  d-md-block">
                                 <?php
                                 if($feed_post_data){
                                    if($hidedata=='1'){?>
                                    <span class="card-notify-year cardbody_data hide_post" userid="{{$posts_datas->user_id}}" post-id="{{$posts_datas->id}}" curntuserid="{{Auth::user()->id}}" status="0"><i class="fa fa-eye-slash" aria-hidden="true"></i></span>
                                    <?php } else { ?>
                                    <span class="card-notify-year cardbody_data hide_post" userid="{{$posts_datas->user_id}}" post-id="{{$posts_datas->id}}" curntuserid="{{Auth::user()->id}}" status="1"><i class="fa fa-eye" aria-hidden="true"></i></span>
                                    <?php } 
                                 } else { ?>
                                    <span class="card-notify-year cardbody_data hide_post" userid="{{$posts_datas->user_id}}" post-id="{{$posts_datas->id}}" curntuserid="{{Auth::user()->id}}" status="1"><i class="fa fa-eye" aria-hidden="true"></i></span>
                                 <?php } ?>
                                 <div class="card mb-2 cardbody_data card_data_text" id="{{$posts_datas->id}}" data-toggle="modal" data-target="#imagemodal_{{$posts_datas->id}}">
                                    <div class="card-body card_datadetails ">
                                       <?php $posts_desc = $posts_datas->post_description; ?>
                                       <!-- <h4 class="card-title cardbody_title">{{ $posts_datas->post_description }}</h4> -->
                                       <p class="card-text"><?php echo substr($posts_desc, 0, 300) . '...'; ?></p>
                                    </div>
                                 </div>
                              </div>
                           @endif
                        @endforeach
                        @endif
                     </div>
                  </div>
                  <!--/.First slide-->
                  <!--/.Slides-->
               </div>
            </div>
            <div class="row">
               <div class="col-md-2">
                  <h1 style="color:#000; font-size: 25px; font-family: Montserrat; font-weight: 600;" >USERS</h1>
               </div>
               <div class="col-md-6"></div>
            </div>
            <div class="table-responsive">
               <table class="table table-striped table-bordered table-hover display" id="dataTables" style="width:100%">
                  <thead>
                     <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Admin</th>
                        <th>Email Address</th>
                        <th>Password</th>
                        <th>Account</th>
                     </tr>
                  </thead>
                  <tbody>
                     @foreach($user_data as $user_datas)
                     @if($user_datas->id == Auth::user()->id)
                     @else
                     <tr class="odd gradeX">
                        <td>{{$user_datas->firstname}}</td>
                        <td>{{$user_datas->lastname}}</td>
                        <td>
                           <div class="dropdown">
                              <select name="userrole" id="userrole" user-id="{{$user_datas->id}}" class="userrole">
                                 <option value="1" <?php if($user_datas->role=='1'){ echo "selected";}?>>Admin</option>
                                 <option value="2" <?php if($user_datas->role=='2'){ echo "selected";}?>>User</option>
                              </select>
                           </div>
                        </td>
                        <td class="email"><a href="mailto:{{$user_datas->email}}" target="_top">{{$user_datas->email}}</a></td>
                        <td class="center"><span class="reset_password_sec" user-id="{{$user_datas->id}}" >Reset Password</a></td>
                        <td class="center">
                           <div class="col-md-4 col-md-offset-2 ">
                              <div class="acc-btn">
                                 <label class="switch">
                                    <?php 
                                    if($user_datas->is_active=='1' && $user_datas->is_deleted=='0'){
                                       $status = 'true';
                                    } else{
                                       $status = 'false';
                                    }
                                    ?>
                                    <input class="switch-input" type="checkbox" id="togBtn" value="<?php echo $status;?>" name="enabledisable" user-id="{{$user_datas->id}}" <?php if($status=='true'){ echo 'checked';} ?>>
                                    <div class="slider round">
                                       <span class="on enable_on">Enable</span>
                                       <span class="off">Disable</span>
                                    </div>
                                 </label>
                                <!--  <label class="switch-acc switch-left-right">
                                 <input class="switch-input" type="checkbox" id="togBtn" value="true" name="enabledisable">
                                 <span class="switch-label" data-on="Enable Account" data-off="Disable Account" ></span> <span class="switch-handle"></span> </label> -->
                              </div>
                           </div>
                        </td>
                     </tr>
                     @endif
                     @endforeach
                  </tbody>
               </table>
               <!-- <div class="user_pagination">
                  $user_data->links()
               </div> -->
            </div>           
      </div>
   
      <script src="{{ url('/public') }}/slick/slick.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">
$(".center-last").slick({
dots: false,
infinite: false,
centerMode: false,
centerPadding : '10px',
slidesToShow: 4,
slidesToScroll: 1,
responsive: [
{
breakpoint: 1200,
settings: {
   slidesToShow: 3,
   slidesToScroll: 1,
}
},
{
breakpoint: 990,
settings: {
   slidesToShow: 2,
   slidesToScroll: 1,
}
},
{
breakpoint: 768,
settings: {
   slidesToShow: 1,
   slidesToScroll: 2,
    variableWidth: true,
}
}


]
// arrows: true
});
$(function(){
$("#slider").slick({
speed: 1000,
dots: true,
prevArrow: '<button class="slide-arrow prev-arrow"></button>',
nextArrow: '<button class="slide-arrow next-arrow"></button>'
});
});
</script>

      <script type="text/javascript">
         $(function() {
         	$('.pop').on('click', function() {
         		var tabId = $(this).attr('id');
         		
         		$('#imagemodal').modal({
         			backdrop: 'static',
         			keyboard: false,
         		});
         		$('#imagemodal .Clickpreview').addClass('hide');
         		$('#imagemodal .Clickpreview#'+tabId).removeClass('hide');
         		$('#imagemodal').modal('show');   
         	});		
         });
      </script>
      <script>
         $(document).ready(function(){
         	
         	$("#choice").change(function(){
         		var choice = $(this).val();
         //alert(choice);
         if(choice == "opt_text"){
         //alert('1');
         $("#text-box").show();
         $("#image-box").hide();
         $("#video-box").hide();
         } else if(choice== "opt_image"){
         //alert('2');
         $("#text-box").hide();
         $("#image-box").show();
         $("#video-box").hide();
         } else if(choice== "opt_video"){
         //alert('3');
         $("#text-box").hide();
         $("#image-box").hide();
         $("#video-box").show();
         } else {
         //alert('4');
         $("#text-box").show();
         $("#image-box").hide();
         $("#video-box").hide();
         }
         
         });
         	
         });
         
      </script>
      <script type="text/javascript">
         $(function() {
         	$('.save_site').on('click', function() {
         		$("#save_site").hide();
         		$("#text-box-url").show();
         		
         	});
         });
         $(function() {
         	$('.save_site_video').on('click', function() {
         		$("#save_site_video").hide();
         		$("#text-box-url-video").show();
         		
         	});
         });
      </script>
      <script type="text/javascript">
         var switchStatus = false;
         $(".switch-input").on('change', function() {
            var user_id = $(this).closest("div").find("input").attr('user-id');
            if ($(this).is(':checked')) {
               var switchStatus = $(this).is(':checked');
            }
            else {
               var switchStatus = $(this).is(':checked');
            }
            $.ajax({
               type: 'post',
               url: "{{url('admin/accountenabledisable')}}",
               data:{_token: '{{csrf_token()}}', userid: user_id, switchStatus: switchStatus}, 
               success: function(result){
                  $('.message').html('');
                  if(result.status=='success'){
                     $('.message').append(
                        '<div class="alert alert-success alert-dismissable">'+
                          result.message+
                        '</div>'
                     );
                     jQuery('html, body').animate({scrollTop:0}, 'slow'); 
                     $(".message").fadeTo(2000, 500).slideUp(500, function() {
                        $(".message").slideUp(500);
                     });
                  }else{
                     $('.message').append(
                        '<div class="alert alert-danger alert-dismissable">'+
                          '<button type="button" class="close" data-dismiss="alert">'+
                              '<span aria-hidden="true">&times;</span>'+
                              '<span class="sr-only">Close</span>'+
                          '</button>'+
                          result.message+
                        '</div>'
                     );
                     jQuery('html, body').animate({scrollTop:0}, 'slow');
                     setInterval('location.reload()', 1000);
                     return false;
                  }
               }
            });
         }); 
         $('.userrole').change(function() {
            var user_role_status = $(this).val(); 
            var user_id = $(this).closest("div").find("select").attr('user-id');
            $.ajax({
               type: 'post',
               url: "{{url('admin/updateuserrole')}}",
               data:{_token: '{{csrf_token()}}', userid: user_id, user_role_status: user_role_status}, 
               success: function(result){
                  $('.message').html('');
                  if(result.status=='success'){
                     $('.message').append(
                        '<div class="alert alert-success alert-dismissable">'+
                          result.message+
                        '</div>'
                     );
                      jQuery('html, body').animate({scrollTop:0}, 'slow'); 
                     $(".message").fadeTo(2000, 500).slideUp(500, function() {
                        $(".message").slideUp(500);
                     });
                  }else{
                     $('.message').append(
                        '<div class="alert alert-danger alert-dismissable">'+
                          '<button type="button" class="close" data-dismiss="alert">'+
                              '<span aria-hidden="true">&times;</span>'+
                              '<span class="sr-only">Close</span>'+
                          '</button>'+
                          result.message+
                        '</div>'
                     );
                     jQuery('html, body').animate({scrollTop:0}, 'slow');
                     setInterval('location.reload()', 1000);  
                     return false;
                  }
                  
               }
            });
         });
         $('.hide_post').on('click', function() {
            var userid = $(this).closest("div").find("span").attr('userid');
            var postid = $(this).closest("div").find("span").attr('post-id');
            var curntuserid = $(this).closest("div").find("span").attr('curntuserid');
            var status = $(this).closest("div").find("span").attr('status');
            if(status == 1){
               var status_val = 0;
            } else if(status == 0) {
               var status_val = 1;
            }
            $.ajax({
               type: 'post',
               url: "{{url('admin/updateposthidestatus')}}",
               data:{_token: '{{csrf_token()}}', userid: userid, postid: postid, curntuserid: curntuserid, status: status}, 
               success: function(result){
                  $('.message').html('');
                  if(result.status=='success'){
                     $('.message').append(
                        '<div class="alert alert-success alert-dismissable">'+
                          result.message+
                        '</div>'
                     );
                       jQuery('html, body').animate({scrollTop:0}, 'slow'); 
                     $(".message").fadeTo(2000, 500).slideUp(500, function() {
                        $(".message").slideUp(500);
                     });
                     $("#post_id_"+postid).attr("status", status_val);
                     if(jQuery('.post_hide'+postid).hasClass( "fa-eye-slash" )) {
                        jQuery('.post_hide'+postid).removeClass('fa-eye-slash').addClass('fa-eye');
                     } else {
                        jQuery('.post_hide'+postid).removeClass('fa-eye').addClass('fa-eye-slash');                        
                     }

                  }else{
                     $('.message').append(
                        '<div class="alert alert-danger alert-dismissable">'+
                          '<button type="button" class="close" data-dismiss="alert">'+
                              '<span aria-hidden="true">&times;</span>'+
                              '<span class="sr-only">Close</span>'+
                          '</button>'+
                          result.message+
                        '</div>'
                     );
                     jQuery('html, body').animate({scrollTop:0}, 'slow');
                    // setInterval('location.reload()', 1000); 
                     return false;     }
               }
            });
         });
         $('.reset_password_sec').on('click', function() {
            var user_role = $(this).closest("td").find("span").attr('user-id');
            $.ajax({
               type: 'post',
               url: "{{url('admin/adminresetpassword')}}",
               data:{_token: '{{csrf_token()}}', userrole: user_role}, 
               success: function(result){
                  $('.message').html('');
                  if(result.status=='success'){
                     $('.message').append(
                        '<div class="alert alert-success alert-dismissable">'+
                          result.message+
                        '</div>'
                     );
                      jQuery('html, body').animate({scrollTop:0}, 'slow'); 
                        $(".message").fadeTo(2000, 500).slideUp(500, function() {
                        $(".message").slideUp(500);
                     });
                  }else{
                     $('.message').append(
                        '<div class="alert alert-danger alert-dismissable">'+
                          '<button type="button" class="close" data-dismiss="alert">'+
                              '<span aria-hidden="true">&times;</span>'+
                              '<span class="sr-only">Close</span>'+
                          '</button>'+
                          result.message+
                        '</div>'
                     );
                     jQuery('html, body').animate({scrollTop:0}, 'slow');
                     setInterval('location.reload()', 1000); 
                     return false;
                  }
               }
            });
         });
         $(".register-switch-input").on('change', function() {
            var user_role = $('#registerintogBtn').attr('role_id');
            var user_id = $('#registerintogBtn').attr('user_id');
            if ($(this).is(':checked')) {
               var switchStatus = $(this).is(':checked'); 
            }
            else {
               var switchStatus = $(this).is(':checked');
            }
            $.ajax({
               type: 'post',
               url: "{{url('admin/registerenabledisable')}}",
               data:{_token: '{{csrf_token()}}', userrole: user_role, switchStatus: switchStatus,user_id: user_id,}, 
               success: function(result){
                  $('.message').html('');
                  if(result.status=='success'){
                     $('.message').append(
                        '<div class="alert alert-success alert-dismissable">'+
                          result.message+
                        '</div>'
                     );
                      jQuery('html, body').animate({scrollTop:0}, 'slow'); 
                     $(".message").fadeTo(2000, 500).slideUp(500, function() {
                        $(".message").slideUp(500);
                     });
                  }else{
                     $('.message').append(
                        '<div class="alert alert-danger alert-dismissable">'+
                          '<button type="button" class="close" data-dismiss="alert">'+
                              '<span aria-hidden="true">&times;</span>'+
                              '<span class="sr-only">Close</span>'+
                          '</button>'+
                          result.message+
                        '</div>'
                     );
                     jQuery('html, body').animate({scrollTop:0}, 'slow');
                     setInterval('location.reload()', 1000);
                     return false;
                  }
               }
            });
         }); 
      </script>
      <script type="text/javascript">
         $(document).ready(function() {
            var table = $('#dataTables').DataTable();
                table.search(this.value).draw();   
         });
      </script>
      <style type="text/css">
         input[aria-controls='dataTables']{
             border: 1px solid #ccc;
             border-radius: 21px;
             margin-left: 9px !important;
             margin-bottom: 12px;
             width: 246px;
             padding: 5px;
             margin-right: 0px;
             margin-top: 0px;
             background: #e0e0e075;
         }
         .reset_password_sec {
            cursor: pointer;
            color: #007bff;
         }
         .card_data_text {
            background: #333333 !important;
         }
         .card-text{
            color: #fff !important;
         }
         .card_data_text .card_datadetails {
            margin-top: 10px;
         }
         .card_image .card_datadetails, .card_video .card_datadetails, .card_youtube .card_datadetails {
            margin-top: 0px;
         }
         .img_hide_icon {
            right: auto;
            top: 25px;
         }
         /************switch accountsection css**********/
         .switch {
           position: relative;
           display: inline-block;
           width: 90px;
           height: 34px;
         }
         .switch input {display:none;}
         .slider {
           position: absolute;
           cursor: pointer;
           top: 0;
           left: 0;
           right: 0;
           bottom: 0;
           background-color: #ca2222;
           -webkit-transition: .4s;
           transition: .4s;
         }
         .slider:before {
           position: absolute;
           content: "";
           height: 26px;
           width: 26px;
           left: 7px;
           bottom: 7px;
           background-color: white;
           -webkit-transition: .4s;
           transition: .4s;
         }
         input:checked + .slider {
           background-color: #2ab934;
         }
         input:focus + .slider {
           box-shadow: 0 0 1px #2196F3;
         }
         input:checked + .slider:before {
           -webkit-transform: translateX(55px);
           -ms-transform: translateX(55px);
           transform: translateX(55px);
         }
         /*------ ADDED CSS ---------*/
         .on
         {
           display: none;
         }
         .on, .off
         {
           color: white;
           position: absolute;
           transform: translate(-50%,-50%);
           top: 50%;
           left: 60%;
           font-size: 12px;
           font-family: Montserrat;
         }
         span.on.enable_on {
    left: 35px;
}
         input:checked+ .slider .on
         {display: block;}
         input:checked + .slider .off
         {display: none;}
         /*--------- END --------*/
         /* Rounded sliders */
         .slider.round {
           border-radius: 34px;
         }
         .slider.round:before {
           border-radius: 50%;}
         }

         /*****

         *******switch accountsection css end**********/

         span.dashboard_loginenable {

            font-size: 16px;
            font-family: Montserrat !important;

         }
         div#dataTables_wrapper {
             margin-bottom: 20px;
         }
         @media(max-width: 768px) and (min-width: 320px){
         .card-notify-year {
          right: 81%;
          top: 28px;
         }
         .img_hide_icon {
          right: auto !important;
          
         }
         }
         .message {
             padding-top: 20px;
         }
        .slick-track {
             float: left;
         }
      </style>
@endsection