<div class="bs-example">
 <nav class="navbar navbar-expand-md navbar-light bg-light">
    <a href="{{url('/home')}}" class="navbar-brand">
    <img src="{{ url('/public') }}/images/LOGO.png" class="img-fluid" alt="Responsive image"></a>
    <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbarCollapse">
    <span class="navbar-toggler-icon"></span>
    </button>
    <?php if(Auth::user()->role=='1'){ ?>
    <div class="collapse navbar-collapse justify-content-between" id="navbarCollapse_admin" style="margin-right: 30px">
       <div class="navbar-nav nav_actoivce">
            <a  href="{{url('admin/dashboard')}}" class="nav-item nav-link ">Admin</a>
          <a  href="{{url('/home')}}" class="nav-item nav-link ">My Homepage</a>
          <a href="{{url('/feed')}}" class="nav-item nav-link  ">The Feed</a>
         <!--  <a class="nav-link pr-2"><i class="fa fa-search"></i></a> -->
          <div class="nav-item dropdown">
             <a href="#" class="nav-link dropdown-toggle" data-target="#navbarDropdown" data-toggle="dropdown">
                <div class="row">
                    <?php $user = Auth::user(); 
                      $path = env('APP_URL');
                      if(empty($user->profilepic)) {
                        $userimg = $path."/public/images/User-dummy-300x300.png";
  
                      } else{
                        $userimg = $path."/public/uploads/profileimages/profilethumbs/".$user->profilepic;
                      }
                    ?>
                   <img src="{{ $userimg }}" class="user-profile"/>
                   <p>{{ Auth::user()->firstname }} {{ Auth::user()->lastname }}</p>
                   <span class="caret">
                </div>
                </span>
             </a>
             <div class="dropdown-menu" id="navbarDropdown">
                <a href="{{url('/user/profile', Auth::id())}}" class="dropdown-item" style="border-bottom: 1px solid #ccc;"><i class="fa fa-user" aria-hidden="true"></i>My Profile</a>
                
                <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
            document.getElementById('logout-form').submit();"><i class="fa fa-sign-out" aria-hidden="true"></i>Logout</a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
            </form>
             </div>
          </div>
       </div>
    </div>
  <?php } else { ?>
    <div class="collapse navbar-collapse justify-content-between" id="navbarCollapse_user">
       <div class="navbar-nav nav_actoivce">
          <a  href="{{url('/home')}}" class="nav-item nav-link ">My Homepage</a>
          <a href="{{url('/feed')}}" class="nav-item nav-link">The Feed</a>
         <!--  <a class="nav-link pr-2"><i class="fa fa-search"></i></a> -->
          <div class="nav-item dropdown">
             <a href="#" class="nav-link dropdown-toggle" data-target="#navbarDropdown1" data-toggle="dropdown">
                <div class="row">
                    <?php $user = Auth::user(); 
                      $path = env('APP_URL');
                      if(empty($user->profilepic)) {
                        $userimg = $path."/public/images/User-dummy-300x300.png";
                      } else{
                        $userimg = $path."/public/uploads/profileimages/profilethumbs/".$user->profilepic;
                      }
                    ?>
                   <img src="{{ $userimg }}" class="user-profile"/>
                   <p>{{ Auth::user()->firstname }} {{ Auth::user()->lastname }}</p>
                   <span class="caret">
                </div>
                </span>
             </a>
             <div class="dropdown-menu" id="navbarDropdown1">
                <a href="{{url('/user/profile', Auth::id())}}" class="dropdown-item" style="border-bottom: 1px solid #ccc;"><i class="fa fa-user" aria-hidden="true"></i>My Profile</a>
                
                <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
            document.getElementById('logout-form').submit();"><i class="fa fa-sign-out" aria-hidden="true"></i>Logout</a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
            </form>
             </div>
          </div>
       </div>
    </div><?php } ?>
 </nav>
</div>
<script type="text/javascript">
  $(function(){

    var url = window.location.pathname, 
        urlRegExp = new RegExp(url.replace(/\/$/,'') + "$"); // create regexp to match current url pathname and remove trailing slash if present as it could collide with the link in navigation in case trailing slash wasn't present there
        // now grab every link from the navigation
        $('.nav_actoivce a.nav-item').each(function(){
            // and test its normalized href against the url pathname regexp
            if(urlRegExp.test(this.href.replace(/\/$/,''))){
                $(this).addClass('active');
            }
        });
});
</script>

<script>
/*$(document).ready(function(){

  jQuery(".texrt").bind('cut copy paste', function (e) {
  e.preventDefault();
  });
  jQuery(document).bind('contextmenu', function (e) {
  e.preventDefault();
  });
  document.onkeydown = function(e) {
  if(e.keyCode == 123) {
  return false;
  }
  if(e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)){
  return false;
  }
  if(e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)){
  return false;
  }
  if(e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)){
  return false;
  }

  }
});*/
</script>
