<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\Posts;
use App\Models\Feeds;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Str;
use App\Classes\ErrorsClass;
use Session;
use Config;
use DB;
use Image;

class ProfileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request, $id)
    {   
        $user_id = Auth::id();
        $user = Auth::user();
        return view('frontend.userprofile',compact('user_id', 'user'));
    }

    public function updateprofile(Request $request)
    {   
      try{
   
            $Input = [];
            $user_id = trim($request->user_id);
            $Input['firstname'] = trim($request->user_firstname);
            $Input['lastname'] = trim($request->user_laststname);
            $Input['updated_by'] = Auth::id();

            if($request->hasfile('user_img')) {
              $image = $request->file('user_img');
              $filename = time() .'_'. $image->getClientOriginalName();
              $image->move(public_path('uploads/profileimages/'), $filename);
              $thumb = Image::make(public_path('uploads/profileimages/'.$filename))->resize(200,200)->save(public_path('uploads/profileimages/profilethumbs/'. $filename),60); 
              $Input['profilepic'] = $filename;
            }       
            $updat_profile = user::where('id', $user_id)->update($Input);

            if($updat_profile) {
              return redirect()->back()->with('success','Profile Update Successfully.');
            } else {
              return redirect()->back()->with('error','Post Created Successfully.');
            }

        }catch(\Illuminate\Database\QueryException $e) {
              $errorClass = new ErrorsClass();
               $errors = $errorClass->saveErrors($e);
            }  catch(\Exception $e) {
              $errorClass = new ErrorsClass();
              $errors = $errorClass->saveErrors($e);
            }       
    }
}
