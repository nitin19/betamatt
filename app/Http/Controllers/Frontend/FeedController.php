<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\Posts;
use App\Models\Feeds;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Str;
use App\Classes\ErrorsClass;
use Session;
use Config;
use DB;
use Image;

class FeedController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {   
        $user_id = Auth::id();
        $user = Auth::user();
        $posts =  Posts::where('post_privacy', '1')->where('is_active', '1')->where('is_deleted', '0')->orderBy('id','DESC')->get();

        $like_feeds_posts_Obj = Feeds::where('user_id', Auth::id())->where('feed_like', '1')->pluck('post_id');
        $like_feed_posts = $like_feeds_posts_Obj->toArray();

        $pray_feeds_posts_Obj = Feeds::where('user_id', Auth::id())->where('feed_pray', '1')->pluck('post_id');
        $pray_feed_posts = $pray_feeds_posts_Obj->toArray();

        $hide_feeds_posts_Obj = Feeds::where('user_id', Auth::id())->where('feed_hide', '1')->pluck('post_id');
        $hide_feed_posts = $hide_feeds_posts_Obj->toArray();
        
        return view('frontend.feed',compact('posts', 'like_feed_posts', 'pray_feed_posts', 'hide_feed_posts'));
    }

    public function addPostFeed(Request $request)
    {
      try{
      $Input =  [];
      $isFeed = Feeds::where('user_id', Auth::id())->where('post_id', trim($request->postidDefault))->where('is_active', '1')->where('is_deleted', '0')->count();
      if( $isFeed > 0 ) {
          $Input['feed_like'] = trim($request->likeDefault);
          $Input['feed_pray'] = trim($request->prayDefault);
          $Input['feed_hide'] = trim($request->hideDefault);
          $Input['updated_by'] = Auth::id();
          $feed = Feeds::where('user_id', Auth::id())->where('post_id', trim($request->postidDefault))->where('is_active', '1')->where('is_deleted', '0')->first();
          $upQry = Feeds::where('id', $feed->id)->update($Input);
          if($upQry) {
            return redirect()->route('feed')
                    ->with('success','Post feed updated successfully'); 
          } else {
            return redirect()->route('feed')
                    ->with('error','Soory fail to update post feed'); 
          }
      } else {
          $Input['user_id'] = Auth::id();
          $Input['post_id'] = trim($request->postidDefault);
          $Input['post_user_id'] = trim($request->postuseridDefault);
          $Input['feed_like'] = trim($request->likeDefault);
          $Input['feed_pray'] = trim($request->prayDefault);
          $Input['feed_hide'] = trim($request->hideDefault);
          $Input['created_by'] = Auth::id();
          $feedId = Feeds::insertGetId($Input);
          if($feedId) {
            return redirect()->route('feed')
                    ->with('success','Post feed added successfully'); 
          } else {
            return redirect()->route('feed')
                    ->with('error','Soory fail to add post feed'); 
          }
        }
    } catch(\Illuminate\Database\QueryException $e) {
        $errorClass = new ErrorsClass();
        $errors = $errorClass->saveErrors($e);
      } catch(\Exception $e) {
          $errorClass = new ErrorsClass();
          $errors = $errorClass->saveErrors($e);
      }
  }
}
