<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\Posts;
use App\Models\Feeds;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Str;
use App\Classes\ErrorsClass;
use Session;
use Config;
use DB;
use Image;

class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function addPost(Request $request)
    {
      try{

      $Input =  [];
      $Input['user_id'] = Auth::id();
      $Input['post_type'] = trim($request->choiceCreate);
      $Input['post_privacy'] = trim($request->privacyCreate);
      $Input['is_like_post'] = trim($request->likeCreate);
      $Input['is_pray_post'] = trim($request->prayCreate);
      $Input['post_views'] = '0';
      $Input['created_by'] = Auth::id();

      if(trim($request->choiceCreate)=='image') {
        $Input['post_media_choice'] = trim($request->imgchoiceCreate);
        if(trim($request->imgchoiceCreate)==1) {
          $Input['post_description'] = trim($request->postpicurlCreate);
        } else {
          $postImgUrl = env('POST_IMG_URL');
          if($request->hasfile('postpicCreate')) {
            $image = $request->file('postpicCreate');
            $filename = time() .'_'. $image->getClientOriginalName();
            $image->move(public_path('uploads/postsimages/'), $filename);
            $thumb = Image::make(public_path('uploads/postsimages/'.$filename))->resize(200,200)->save(public_path('uploads/postsimages/poststhumbs/'. $filename),60); 
            $postImgPath = $postImgUrl.'/'.$filename;
            $Input['post_description'] = $postImgPath;
          } 
        }
      } elseif(trim($request->choiceCreate)=='video') {
        $Input['post_media_choice'] = trim($request->vdochoiceCreate);
        if(trim($request->vdochoiceCreate)==1) {
          $Input['post_description'] = trim($request->postvideourlCreate);
        } else {
          $postVideoUrl = env('POST_VIDEO_URL');
          if($request->hasfile('postvideoCreate')) {
            $video = $request->file('postvideoCreate');
            $filename = time() .'_'. $video->getClientOriginalName();
            $video->move(public_path('uploads/postsvideos/'), $filename);
            $postVdoPath = $postVideoUrl.'/'.$filename;
            $Input['post_description'] = $postVdoPath;
          } 
        }
      } else {

         $Input['post_description'] = strip_tags(trim($request->messageCreate));

      }
      
      $postId = Posts::insertGetId($Input);
      if($postId) {
            return redirect()->route('home')
                    ->with('success','Post added successfully'); 
          } else {
            return redirect()->route('home')
                    ->with('error','Soory fail to add post'); 
          }
    } catch(\Illuminate\Database\QueryException $e) {
        $errorClass = new ErrorsClass();
        $errors = $errorClass->saveErrors($e);
      } catch(\Exception $e) {
          $errorClass = new ErrorsClass();
          $errors = $errorClass->saveErrors($e);
      }
  }

  public function editdefaultpost(Request $request)
    {
      try{
       
      $Input =  [];
      $post_id = trim($request->postidDefault);
     // $Input['user_id'] = Auth::id();
      $Input['post_type'] = trim($request->choiceDefault);
      $Input['post_privacy'] = trim($request->privacyDefault);
      $Input['is_like_post'] = trim($request->likeDefault);
      $Input['is_pray_post'] = trim($request->prayDefault);
      $Input['updated_by'] = Auth::id();

      if(trim($request->choiceDefault)=='image') {
        $Input['post_media_choice'] = trim($request->imgchoiceDefault);
        if(trim($request->imgchoiceDefault)==1) {
          $Input['post_description'] = trim($request->postpicurlDefault);
        } else {
          $postImgUrl = env('POST_IMG_URL');
          if($request->hasfile('postpicDefault')) {
            $image = $request->file('postpicDefault');
            $filename = time() .'_'. $image->getClientOriginalName();
            $image->move(public_path('uploads/postsimages/'), $filename);
            $thumb = Image::make(public_path('uploads/postsimages/'.$filename))->resize(200,200)->save(public_path('uploads/postsimages/poststhumbs/'. $filename),60); 
            $postImgPath = $postImgUrl.'/'.$filename;
            $Input['post_description'] = $postImgPath;
          } 
        }
      } elseif(trim($request->choiceDefault)=='video') {
        $Input['post_media_choice'] = trim($request->vdochoiceDefault);
        if(trim($request->vdochoiceDefault)==1) {
          $Input['post_description'] = trim($request->postvideourlDefault);
        } else {
          $postVideoUrl = env('POST_VIDEO_URL');
          if($request->hasfile('postvideoDefault')) {
            $video = $request->file('postvideoDefault');
            $filename = time() .'_'. $video->getClientOriginalName();
            $video->move(public_path('uploads/postsvideos/'), $filename);
            $postVdoPath = $postVideoUrl.'/'.$filename;
            $Input['post_description'] = $postVdoPath;
          } 
        }
      } else {
         $Input['post_description'] = trim($request->messageDefault);
      }
      
      $upQry = Posts::where('id', $post_id)->update($Input);
      if($upQry) {
            return redirect()->route('home')
                    ->with('success','Post added successfully'); 
          } else {
            return redirect()->route('home')
                    ->with('error','Soory fail to add post'); 
          }
    } catch(\Illuminate\Database\QueryException $e) {
        $errorClass = new ErrorsClass();
        $errors = $errorClass->saveErrors($e);
      } catch(\Exception $e) {
          $errorClass = new ErrorsClass();
          $errors = $errorClass->saveErrors($e);
      }
  }

  public function editlikePost(Request $request)
    {
      try{
       
      $Input =  [];
      $post_id = trim($request->postidLike);
     // $Input['user_id'] = Auth::id();
      $Input['post_type'] = trim($request->choiceLike);
      $Input['post_privacy'] = trim($request->privacyLike);
      $Input['is_like_post'] = trim($request->likeLike);
      $Input['is_pray_post'] = trim($request->prayLike);
      $Input['updated_by'] = Auth::id();

      if(trim($request->choiceLike)=='image') {
        $Input['post_media_choice'] = trim($request->imgchoiceLike);
        if(trim($request->imgchoiceLike)==1) {
          $Input['post_description'] = trim($request->postpicurlLike);
        } else {
          $postImgUrl = env('POST_IMG_URL');
          if($request->hasfile('postpicLike')) {
            $image = $request->file('postpicLike');
            $filename = time() .'_'. $image->getClientOriginalName();
            $image->move(public_path('uploads/postsimages/'), $filename);
            $thumb = Image::make(public_path('uploads/postsimages/'.$filename))->resize(200,200)->save(public_path('uploads/postsimages/poststhumbs/'. $filename),60); 
            $postImgPath = $postImgUrl.'/'.$filename;
            $Input['post_description'] = $postImgPath;
          } 
        }
      } elseif(trim($request->choiceLike)=='video') {
        $Input['post_media_choice'] = trim($request->vdochoiceLike);
        if(trim($request->vdochoiceLike)==1) {
          $Input['post_description'] = trim($request->postvideourlLike);
        } else {
          $postVideoUrl = env('POST_VIDEO_URL');
          if($request->hasfile('postvideoLike')) {
            $video = $request->file('postvideoLike');
            $filename = time() .'_'. $video->getClientOriginalName();
            $video->move(public_path('uploads/postsvideos/'), $filename);
            $postVdoPath = $postVideoUrl.'/'.$filename;
            $Input['post_description'] = $postVdoPath;
          } 
        }
      } else {
         $Input['post_description'] = trim($request->messageLike);
      }
      
      $upQry = Posts::where('id', $post_id)->update($Input);
      if($upQry) {
            return redirect()->route('home')
                    ->with('success','Post added successfully'); 
          } else {
            return redirect()->route('home')
                    ->with('error','Soory fail to add post'); 
          }
    } catch(\Illuminate\Database\QueryException $e) {
        $errorClass = new ErrorsClass();
        $errors = $errorClass->saveErrors($e);
      } catch(\Exception $e) {
          $errorClass = new ErrorsClass();
          $errors = $errorClass->saveErrors($e);
      }
  }

    public function editprayPost(Request $request)
    {
      try{
       
      $Input =  [];
      $post_id = trim($request->postidPray);
     // $Input['user_id'] = Auth::id();
      $Input['post_type'] = trim($request->choicePray);
      $Input['post_privacy'] = trim($request->privacyPray);
      $Input['is_like_post'] = trim($request->likePray);
      $Input['is_pray_post'] = trim($request->prayPray);
      $Input['updated_by'] = Auth::id();

      if(trim($request->choicePray)=='image') {
        $Input['post_media_choice'] = trim($request->imgchoicePray);
        if(trim($request->imgchoicePray)==1) {
          $Input['post_description'] = trim($request->postpicurlPray);
        } else {
          $postImgUrl = env('POST_IMG_URL');
          if($request->hasfile('postpicPray')) {
            $image = $request->file('postpicPray');
            $filename = time() .'_'. $image->getClientOriginalName();
            $image->move(public_path('uploads/postsimages/'), $filename);
            $thumb = Image::make(public_path('uploads/postsimages/'.$filename))->resize(200,200)->save(public_path('uploads/postsimages/poststhumbs/'. $filename),60); 
            $postImgPath = $postImgUrl.'/'.$filename;
            $Input['post_description'] = $postImgPath;
          } 
        }
      } elseif(trim($request->choicePray)=='video') {
        $Input['post_media_choice'] = trim($request->vdochoicePray);
        if(trim($request->vdochoicePray)==1) {
          $Input['post_description'] = trim($request->postvideourlPray);
        } else {
          $postVideoUrl = env('POST_VIDEO_URL');
          if($request->hasfile('postvideoPray')) {
            $video = $request->file('postvideoPray');
            $filename = time() .'_'. $video->getClientOriginalName();
            $video->move(public_path('uploads/postsvideos/'), $filename);
            $postVdoPath = $postVideoUrl.'/'.$filename;
            $Input['post_description'] = $postVdoPath;
          } 
        }
      } else {
         $Input['post_description'] = trim($request->messagePray);
      }
      
      $upQry = Posts::where('id', $post_id)->update($Input);
      if($upQry) {
            return redirect()->route('home')
                    ->with('success','Post added successfully'); 
          } else {
            return redirect()->route('home')
                    ->with('error','Soory fail to add post'); 
          }
    } catch(\Illuminate\Database\QueryException $e) {
        $errorClass = new ErrorsClass();
        $errors = $errorClass->saveErrors($e);
      } catch(\Exception $e) {
          $errorClass = new ErrorsClass();
          $errors = $errorClass->saveErrors($e);
      }
  }


  public function deletepost(Request $request, $id)
    {
      try{
      $Input =  [];
      $Input['is_active'] = '0';
      $Input['is_deleted'] = '1';
      $Input['updated_by'] = Auth::id();
      $upQry = Posts::where('id', $id)->update($Input);
      if($upQry) {
            return redirect()->route('home')
                    ->with('error','Post deleted successfully'); 
          } else {
            return redirect()->route('home')
                    ->with('error','Soory fail to delete post'); 
          }
    } catch(\Illuminate\Database\QueryException $e) {
        $errorClass = new ErrorsClass();
        $errors = $errorClass->saveErrors($e);
      } catch(\Exception $e) {
          $errorClass = new ErrorsClass();
          $errors = $errorClass->saveErrors($e);
      }
  }

}  
