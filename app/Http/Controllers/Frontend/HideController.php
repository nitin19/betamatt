<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\Posts;
use App\Models\Feeds;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Str;
use App\Classes\ErrorsClass;
use Session;
use Config;
use DB;
use Image;

class HideController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function hidelikeFeed(Request $request)
    {
         $Input =  [];
          $post_id = $_POST['postid'];
          $userid = $_POST['userid'];
         $isFeedlike = Feeds::where('user_id',$userid)->where('post_id',$post_id)->where('is_active', '1')->where('is_deleted', '0')->count();
          if($isFeedlike > 0 ) {
           $upQry = Feeds::where('post_id',$post_id)->where('user_id',$userid)  ->update([  'feed_hide' => '1','feed_like' => '0',]);  
           if($upQry){

            echo "update";

           }                   
       
         }
    }
     public function hidePrayFeed(Request $request)
    {
         $Input =  [];
          $post_id = $_POST['postid'];
          $userid = $_POST['userid'];

         $isFeedpray = Feeds::where('user_id',$userid)->where('post_id',$post_id)->where('is_active', '1')->where('is_deleted', '0')->count();

          if($isFeedpray > 0 ) {

           $upQrypray = Feeds::where('post_id',$post_id)->where('user_id',$userid)  ->update([ 'feed_hide' => '1','feed_pray' => '0',]); 

          if($upQrypray) {

            echo "prayupdate";

          }                       
       
        }
    }  

}

