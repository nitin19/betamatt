<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\Posts;
use App\Models\Feeds;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Str;
use App\Classes\ErrorsClass;
use Session;
use Config;
use DB;
use Image;

class PasswordController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request, $id)
    {   
        $user_id = Auth::id();
        $user = Auth::user();

        return view('frontend.password',compact('user_id', 'user'));
    }
     public function updatepassword(Request $request)
    {   
      
        $rules = array (
                'old_password' => 'required',
                'password' => 'required', 
                'password_confirmation' => 'required' 
        );

        $validator = Validator::make ( Input::all (), $rules );

        if ($validator->fails ()) {
            return Redirect::back ()->withErrors ( $validator, 'login' )->withInput ();
        } else {

            $Input = [];
            $new_password  = trim($request->password);
            $confirm_password  = trim($request->password_confirmation);
            $user_id =  trim($request->user_id);
            $password = trim($request->old_password);
            $Input['hd_password'] = $new_password;
            $Input['password'] = Hash::make($confirm_password);

            $user = user::where('id', Auth::id())->where('hd_password', $password)->where('is_active', '1')->where('is_deleted', '0')->first();

            if($user){

                if ($new_password == $confirm_password){

                    $updat_password = user::where('id', $user_id)->update($Input);

                    return redirect()->back()->with('success','Password is updated succesfully .');

                }else {

                    return redirect()->back()->with('error','New Password and Confirm Password does not match .');
                }

            }else {

                return redirect()->back()->with('error','Old Password Is Wrong.');
            }
        }
    }
}