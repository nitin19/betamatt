<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\Posts;
use App\Models\Feeds;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Str;
use App\Classes\ErrorsClass;
use Session;
use Config;
use DB;
use Image;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {   
        $user_id = Auth::id();
        $user = Auth::user();

        $posts =  Posts::where('user_id', Auth::id())->where('is_active', '1')->where('is_deleted', '0')->orderBy('id','DESC')->get();

        $likeposts =  Posts::where('user_id', Auth::id())->where('is_like_post', '1')->where('is_active', '1')->where('is_deleted', '0')->orderBy('id','DESC')->get();

        $prayposts =  Posts::where('user_id', Auth::id())->where('is_pray_post', '1')->where('is_active', '1')->where('is_deleted', '0')->orderBy('id','DESC')->get();

        $like_fetch =  Feeds::where('user_id', Auth::id())->where('feed_like', '1')->get();

        $pray_fetch =  Feeds::where('user_id', Auth::id())->where('feed_pray', '1')->get();

      /*  $users = DB::table('posts')
            ->join('feedposts', 'posts.user_id', '=', 'feedposts.post_user_id')
            ->select('posts.*', 'feedposts.*')
            ->where('posts.is_like_post','=', '1' )
            ->where('feedposts.feed_like','=','1' )
            ->get();


        echo "<pre>";
        print_r($users);
        echo "</pre>";*/

        return view('frontend.home',compact('posts', 'likeposts', 'prayposts','like_fetch','pray_fetch'));
    }

}
