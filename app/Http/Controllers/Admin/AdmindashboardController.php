<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\Posts;
use App\Models\Feeds;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Str;
use App\Classes\ErrorsClass;
use Session;
use Config;
use DB;
use Image;

class AdmindashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
      	try{
      		$posts_data = Posts::where('post_privacy','=','1')
                    ->orderBy('id', 'DESC')
      							->get();
      		$user_data = User::get();

          $admin_user_data = User::where('id', Auth::user()->id)->where('role','=','1')->first();          

      		return view('admin.dashboard', compact('posts_data', 'user_data', 'admin_user_data'));
      	} catch(\Illuminate\Database\QueryException $e) {
        	$errorClass = new ErrorsClass();
        	$errors = $errorClass->saveErrors($e);
      	} catch(\Exception $e) {
          	$errorClass = new ErrorsClass();
          $errors = $errorClass->saveErrors($e);
      }
    }
    public function account_enable_disable(Request $request)
    {
        try{
          $userid = $request->userid;
          $switchStatus = $request->switchStatus;
          if($switchStatus == 'true'){
            $user_update = User::where('id', $userid)
              ->update(['is_active' => '1', 'is_deleted' => '0']);
          } else {
            $user_update = User::where('id', $userid)
              ->update(['is_active' => '0', 'is_deleted' => '1']);
          }
          if($user_update){
            if($switchStatus=='true'){
              return response()->json(['status'=>'success','message'=>'Account activated Successfully!']);
            } else {
              return response()->json(['status'=>'success','message'=>'Account deactivated Successfully!']);
            }
          } else {
            return response()->json(['status'=>'error','message'=>'Account not updated Successfully. Please Try again!']);
          }
        } catch(\Illuminate\Database\QueryException $e) {
          $errorClass = new ErrorsClass();
          $errors = $errorClass->saveErrors($e);
        } catch(\Exception $e) {
            $errorClass = new ErrorsClass();
          $errors = $errorClass->saveErrors($e);
      }
    }
    public function update_user_role(Request $request)
    {
        try{
          $userid = $request->userid;
          $user_role_status = $request->user_role_status;
          $user_update = User::where('id', $userid)
              ->update(['role' => $user_role_status]);
          if($user_update){
            return response()->json(['status'=>'success','message'=>'User role updated Successfully!']);
          } else {
            return response()->json(['status'=>'error','message'=>'User role not updated Successfully. Please Try again!']);
          }
        } catch(\Illuminate\Database\QueryException $e) {
          $errorClass = new ErrorsClass();
          $errors = $errorClass->saveErrors($e);
        } catch(\Exception $e) {
            $errorClass = new ErrorsClass();
          $errors = $errorClass->saveErrors($e);
      }
    }
    public function update_post_hide_status(Request $request)
    {
        try{
          $userid = $request->userid;
          $postid = $request->postid;
          $curntuserid = $request->curntuserid;
          $status = $request->status;
          $feed_post_data = Feeds::where('user_id', $curntuserid)
                                ->where('post_id',$postid)
                                ->where('post_user_id',$userid)  
                                ->get();
          if($status == 1){
            if(count($feed_post_data)>0){
            $feed_hide_update = Feeds::where('post_id',$postid)
                                ->where('post_user_id',$userid)  
                                ->update(['feed_hide' => $status]);
            } else {
              $feed_hide_update = DB::table('feedposts')->insert(['user_id' => $curntuserid, 'post_id' => $postid, 'post_user_id' => $userid, 'feed_hide' => $status]);
            } 
            if($feed_hide_update){
              return response()->json(['status'=>'success','message'=>'Post hide updated Successfully!']);
            } 
          } else {
            if(count($feed_post_data)>0){
            $feed_hide_update = Feeds::where('post_id',$postid)
                                ->where('post_user_id',$userid)  
                                ->update(['feed_hide' => $status]);
            } else {
              $feed_hide_update = DB::table('feedposts')->insert(['user_id' => $curntuserid, 'post_id' => $postid, 'post_user_id' => $userid, 'feed_hide' => $status]);
            } 
            if($feed_hide_update){
              return response()->json(['status'=>'success','message'=>'Post show updated Successfully!']);
            } 
          }                    
        } catch(\Illuminate\Database\QueryException $e) {
          $errorClass = new ErrorsClass();
          $errors = $errorClass->saveErrors($e);
        } catch(\Exception $e) {
            $errorClass = new ErrorsClass();
          $errors = $errorClass->saveErrors($e);
      }
    }
    public function admin_reset_password(Request $request)
    {
        try{
          $userid = $request->userid;
          $user_data = User::where('id',$userid)->first();
          $chrList =  'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789`-=~!@#$%^&*()_+,./<>?;:[]{}\|';
          $chrRepeatMin = 01;
          $chrRepeatMax = 99;
          $chrRandomLength = 12;
          $random_pass = substr(str_shuffle(str_repeat($chrList, mt_rand($chrRepeatMin,$chrRepeatMax))), 1, $chrRandomLength);
          $encrypt_pass =  Hash::make($random_pass);
          $hd_pass = $random_pass;
          $reset_pass_update = User::where('id', $userid)
                                ->update(['password' => $encrypt_pass, 'hd_password' => $hd_pass]);
          if($reset_pass_update){
              $to = $user_data->email;
              $subject = "Admin password reset";
              $message = "<html>
                            <head>
                              <title>Admin password reset</title>
                            </head>
                            <body>
                              <p>Admin password reset details</p>
                              <table>
                                <tr>
                                  <th>Username:</th>
                                  <td>".$user_data->email."</td>
                                </tr>
                                <tr>
                                  <th>Password:</th>
                                  <td>".$hd_pass."</td>
                                </tr>
                              </table>
                            </body>
                          </html>";
              $headers = "MIME-Version: 1.0" . "\r\n";
              $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
              $headers .= 'From: betmatt@gmail.com' . "\r\n";
              //$headers .= 'Cc: myboss@example.com' . "\r\n";
              mail($to,$subject,$message,$headers);
              return response()->json(['status'=>'success','message'=>'Password reset successfully!']);
          } else {
            return response()->json(['status'=>'error','message'=>'Password reset not Successfully. Please Try again!']);
          }
        } catch(\Illuminate\Database\QueryException $e) {
          $errorClass = new ErrorsClass();
          $errors = $errorClass->saveErrors($e);
        } catch(\Exception $e) {
            $errorClass = new ErrorsClass();
          $errors = $errorClass->saveErrors($e);
      }
    }
    public function register_enable_disable(Request $request)
    {
        try{

          $userrole = $request->userrole;
          $userid = $request->user_id;
          $switchStatus = $request->switchStatus;
          if($switchStatus == 'true'){
            $user_update = User::where('role', $userrole)
                                ->update(['register_status' => '1', 'Register_disableby'=>$userid]);
          } else {
            $user_update = User::where('role', $userrole)
                                ->update(['register_status' => '0','Register_disableby'=>$userid]);
          }
          if($user_update){
            if($switchStatus=='true'){
              return response()->json(['status'=>'success','message'=>'Login enable Successfully!']);
            } else {
              return response()->json(['status'=>'success','message'=>'Login disable Successfully!']);
            }
          } else {
            return response()->json(['status'=>'error','message'=>'Sorry something went wrong. Please Try again!']);
          }
        } catch(\Illuminate\Database\QueryException $e) {
          $errorClass = new ErrorsClass();
          $errors = $errorClass->saveErrors($e);
        } catch(\Exception $e) {
            $errorClass = new ErrorsClass();
          $errors = $errorClass->saveErrors($e);
        }
    }
}