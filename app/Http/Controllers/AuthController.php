<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\Posts;
use App\Models\Feeds;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Str;
use App\Classes\ErrorsClass;
use Session;
use Config;
use DB;
use Image;

class AuthController extends Controller {

public function __construct()
{
    $this->middleware('guest')->except('logout');
}

public function login(Request $request) {	
		$username = $request->username;
        $password = $request->password;

        $remember_me = $request->has('remember_me') ? true : false;

        if(empty($username) || empty($password)){
           return redirect()->back()->with('message','Please fill required fields.');
        } 
		
        $login_type = filter_var($request->username, FILTER_VALIDATE_EMAIL ) 
        ? 'email' 
        : 'username';

        $request->merge([ $login_type => $request->username,'is_active'=>'1','is_deleted'=>'0']);
 
        $userdata = $request->only($login_type, 'password','is_active','is_deleted');

        $rem_email = "rem_email";
            $rem_email_val = $request->username;
            $rem_pwd = "rem_pwd";
            $rem_pwd_val = $request->password;
            $rem_chk = "rem_chk";
            $rem_chk_val = "1";

        if($request->remeber_me==1) {
            setcookie($rem_email, $rem_email_val, time() + (86400 * 90), "/"); 
            setcookie($rem_pwd, $rem_pwd_val, time() + (86400 * 90), "/"); 
            setcookie($rem_chk, $rem_chk_val, time() + (86400 * 90), "/");
        } else {
          setcookie($rem_email, "", time() - 3600);
          setcookie($rem_pwd, "", time() - 3600);
          setcookie($rem_chk, "", time() - 3600);
        }

        if (Auth::attempt($userdata, $remember_me)) {

        	$user = Auth::user();

        	if($user['role'] == '1'){
	          return redirect()->intended('/admin/dashboard');
	          }
	        else{
	           return redirect()->intended('home');
	        }
		} else {
	        Session::flash ( 'message', "Invalid Credentials , Please try again." );
	        return redirect()->back()->with('rerror','Registration Failed! Something Went wrong.');
	        //return redirect()->back()->with('lerror','Please Enter Valid Password or Username.');
	    } 
	//}
}
	public function register(Request $request) {
		$rules = array (
			'firstname' => ['required', 'string', 'max:255'],
            'lastname' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6']
		);
		$validator = Validator::make ( Input::all (), $rules );
		if ($validator->fails ()) {
			return Redirect::back ()->withErrors ( $validator, 'register' )->withInput ();
		} else {

			/*$user = new User ();
			$user->name = $request->get ( 'firstname' ).''.$request->get ( 'lastname' );
			$user->username = substr(str_replace(array('@', '.'), '', $request->get ( 'email' )) . '_' . str_random(), 0, 12);
			$user->email = $request->get ( 'email' );
			$user->password = Hash::make ( $request->get ( 'password' ) );
			$user->hd_password = $request->get ( 'password' );
			$user->firstname = $request->get ( 'firstname' );
			$user->lastname = $request->get ( 'lastname' );
			$user->role = '2';
			$user->device = isset($_SERVER['SERVER_SOFTWARE']) ? $_SERVER['SERVER_SOFTWARE'] : null;
			$user->browser = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : null;
			$user->ipaddress = isset($_SERVER['REMOTE_ADDR']) ? ip2long($_SERVER['REMOTE_ADDR']) : null;
			$user->active_code = strtolower(Str::random(30));
			$user->remember_token = $request->get ( '_token' );
			
			$user->save ();

			$auth_user = $user->id;*/

		$Input =  [];
        $Input['name'] = trim($request->firstname).' '.trim($request->lastname);
        $Input['username'] = substr(str_replace(array('@', '.'), '', trim($request->email)) . '_' . str_random(), 0, 12);
        $Input['email'] = trim($request->email);
        $Input['password'] = Hash::make(trim($request->password));
        $Input['hd_password'] = trim($request->password);
        $Input['firstname'] = trim($request->firstname);
        $Input['lastname'] = trim($request->lastname);
        $Input['role'] = '2';
        $Input['device'] = isset($_SERVER['SERVER_SOFTWARE']) ? $_SERVER['SERVER_SOFTWARE'] : null;
        $Input['browser'] = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : null;
        $Input['ipaddress'] = isset($_SERVER['REMOTE_ADDR']) ? ip2long($_SERVER['REMOTE_ADDR']) : null;
        $Input['active_code'] = strtolower(Str::random(30));

        $auth_user = User::create($Input);

        $userdata = array(
          'email' => trim($request->email) ,
          'password' => trim($request->password)
        );
         
        if($auth_user) {
	        if (Auth::attempt($userdata)) {
	          return redirect()->intended('home');
	          } else {
	          return redirect()->back()->with('rerror','Registration Failed! Something Went wrong.');
	          }
	        } else {
	        	return redirect()->back()->with('rerror','Registration Failed! Something Went wrong.');
	        }  
			
		//return Redirect::back ();
		}
	}
	public function logout() {
		Session::flush ();
		Auth::logout ();
		return Redirect::back ();
	}

	public function userprofile() {

		return view('frontend.userpofile');
	}

	/*public function forgetpassword() {
		return view('web.forgetpassword',$data);
	}*/
}
