<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'username', 'email', 'password', 'hd_password', 'firstname', 'lastname', 'gender', 'dob', 'phone', 'address', 'city', 'state', 'country', 'zipcode', 'lat', 'lng', 'profilepic', 'description', 'role', 'device', 'browser', 'ipaddress', 'active_code', 'is_approved', 'is_active', 'is_deleted', 'is_online', 'llhid', 'logins', 'last_login', 'fp_token', 'created_by', 'updated_by', 'created_at', 'updated_at', 
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'hd_password',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
