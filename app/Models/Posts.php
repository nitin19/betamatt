<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Posts extends Model
{
   
    protected $table = 'posts';

    protected $fillable = [
        'id', 'user_id', 'post_type', 'post_description', 'post_media_choice', 'post_privacy', 'is_like_post', 'is_pray_post', 'is_active', 'is_deleted', 'post_views', 'created_by', 'updated_by', 'created_at', 'updated_at',
    ];

}
