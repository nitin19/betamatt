<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Feeds extends Model
{
   
    protected $table = 'feedposts';

    protected $fillable = [
        'id', 'user_id', 'post_id', 'post_user_id', 'feed_like', 'feed_pray', 'feed_hide', 'is_active', 'is_deleted', 'created_by', 'updated_by', 'created_at', 'updated_at',
    ];

}
